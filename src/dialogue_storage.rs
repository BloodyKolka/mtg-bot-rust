use std::marker::PhantomData;
use std::sync::Arc;
use rusqlite::{Connection, OptionalExtension, Row};
use teloxide::dispatching::dialogue::{Storage};
use teloxide::prelude::ChatId;
use tokio::sync::Mutex;
use crate::mtg::types::ErrorCode;

pub struct DialogueStorage<D>
{
    db: Mutex<Connection>,
    marker: PhantomData<D>,
}

impl<D> DialogueStorage<D>
{
    pub fn new() -> Arc<Self> {
        log::info!("Initializing SQLite dialogue storage...");
        log::info!("Establishing database connection...");
        let db = Connection::open("./data/dialogues.db").expect("Failed to open dialogue database connection!");
        log::info!("Database connection established");

        log::info!("Verifying database...");
        let res = db.query_row("SELECT name FROM sqlite_master WHERE type='table' AND name='dialogues'", (), |_| Ok(())).optional().expect("Failed to check players database!");
        if res.is_none() {
            log::info!("Database does not contain required table. Initializing...");
            log::info!("Initializing players table...");
            db.execute(
                "CREATE TABLE dialogues (
                        chat_id INTEGER PRIMARY KEY,
                        state   BLOB NOT NULL
                )",
                (),
            ).expect("Failed to initialize players table!");
            log::info!("Players table initialized");
        }
        Arc::new(DialogueStorage::<D>{
            db: Mutex::new(db),
            marker: Default::default(),
        })
    }
}

impl<D> Storage<D> for DialogueStorage<D>
where D: rusqlite::types::FromSql + rusqlite::types::ToSql + Sync + Send + 'static
{
    type Error = ErrorCode;

    fn remove_dialogue(self: Arc<Self>, chat_id: ChatId) -> futures_core::future::BoxFuture<'static, Result<(), Self::Error>> {
        Box::pin(async move {
            let deleted_rows = self.db.lock().await.execute("DELETE FROM dialogues WHERE chat_id=?1", [chat_id.0]).expect("Failed accessing dialogues database!");
            if deleted_rows == 0 {
                Err(ErrorCode::NotFound)
            } else {
                Ok(())
            }
        })
    }

    fn update_dialogue(self: Arc<Self>, chat_id: ChatId, dialogue: D) -> futures_core::future::BoxFuture<'static, Result<(), Self::Error>> {
        Box::pin(async move {
            let updated_rows = self.db.lock().await.execute("INSERT INTO dialogues (chat_id, state) VALUES(?1, ?2) ON CONFLICT(chat_id) DO UPDATE SET state=excluded.state", (chat_id.0, dialogue)).expect("Failed accessing dialogues database!");
            if updated_rows == 0 {
                Err(ErrorCode::NotFound)
            } else {
                Ok(())
            }
        })
    }

    fn get_dialogue(self: Arc<Self>, chat_id: ChatId) -> futures_core::future::BoxFuture<'static, Result<Option<D>, Self::Error>> {
        Box::pin(async move {
            Ok(self.db.lock().await.query_row("SELECT state FROM dialogues WHERE chat_id=?1", (chat_id.0,), |row: &Row| row.get(0)).optional().expect("Failed accessing dialogues database!"))
        })
    }
}
