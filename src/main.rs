mod mtg;
mod dialogue_storage;

use std::{time::Duration, sync::Arc, cmp::min};
use chrono::{DateTime, NaiveDateTime, FixedOffset, NaiveTime};
use rusqlite::types::{FromSql, ToSql, FromSqlError, FromSqlResult, ToSqlOutput, ValueRef};

use teloxide::{
    dispatching::dialogue::{Dialogue, GetChatId}, errors::AsResponseParameters, payloads::{EditMessageTextSetters, UnpinChatMessageSetters}, prelude::*, types::{InputFile, InputMedia, InputMediaPhoto, KeyboardButton, KeyboardMarkup, KeyboardRemove, Me, MessageId, ParseMode}, utils::{command::BotCommands, html::user_mention_or_link}
};

use tokio::sync::{Mutex, MutexGuard};

type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;
type PrivateDialogue = Dialogue<PrivateDialogueState, dialogue_storage::DialogueStorage<PrivateDialogueState>>;

#[derive(Clone)]
pub enum GameField {
    Place,               // 0
    Description,         // 1
    DateTime,            // 2
    Fee,                 // 3
    RequiredPlayerCount, // 4
    AnonPlayer,          // 5
}

#[derive(Clone)]
pub enum PollField {
    Question,    // 0
    Description, // 1
    Place,       // 2
    GameTime,    // 3
    StartTime,   // 4
    StartDay,    // 5
    EndTime,     // 6
    EndDay,      // 7
}

#[repr(i8)]
#[derive(Clone, Default)]
pub enum PrivateDialogueState {
    #[default]
    CommandMode,
    MenuMode {menu_message_id: MessageId},

    // Registration process
    AskForName,

    // Profile
    AskForNewName {user_id: i64},
    AskForNewMaxGames {user_id: i64},

    // Game creation
    AskForDate {game_format: mtg::types::GameFormat},
    AskForPlace {game_format: mtg::types::GameFormat, date_time: DateTime<FixedOffset>},
    AskForDescription {game_format: mtg::types::GameFormat, date_time: DateTime<FixedOffset>, place: String},
    AskForFee {game_format: mtg::types::GameFormat, date_time: DateTime<FixedOffset>, place: String, desc: String},
    AskIfFeeMandatory {game_format: mtg::types::GameFormat, date_time: DateTime<FixedOffset>, place: String, desc: String, fee: u32},
    AskPlayerCount {game_format: mtg::types::GameFormat, date_time: DateTime<FixedOffset>, place: String, desc: String, fee: u32, is_fee_mandatory: bool},

    EditGame{game_id: i64, user_id: i64, change_field: GameField},

    // Poll creation
    AskForPollQuestion {game_format: mtg::types::GameFormat},
    AskForPollDescription {game_format: mtg::types::GameFormat, question: String},
    AskForPollPlace {game_format: mtg::types::GameFormat, question: String, description: String},
    AskForPollGameTime {game_format: mtg::types::GameFormat, question: String, description: String, place: String},
    AskForPollStartDay {game_format: mtg::types::GameFormat, question: String, description: String, place: String, game_time: NaiveTime},
    AskForPollStartTime {game_format: mtg::types::GameFormat, question: String, description: String, place: String, game_time: NaiveTime, start_day: u8},
    AskForPollEndDay {game_format: mtg::types::GameFormat, question: String, description: String, place: String, game_time: NaiveTime, start_day: u8, start_time: NaiveTime},
    AskForPollEndTime {game_format: mtg::types::GameFormat, question: String, description: String, place: String, game_time: NaiveTime, start_day: u8, start_time: NaiveTime, end_day: u8},

    EditPoll {poll_id: i64, user_id: i64, change_field: PollField},
}

fn game_format_to_u8(val: &mtg::types::GameFormat) -> u8 {
    match val {
         mtg::types::GameFormat::Commander => 0,
         mtg::types::GameFormat::Standard => 1,
         mtg::types::GameFormat::CommanderDraft => 2,
         mtg::types::GameFormat::StandardDraft => 3,
    }
}

fn game_field_to_u8(val: &GameField) -> u8 {
    match val {
        GameField::Place => 0,
        GameField::Description => 1,
        GameField::DateTime => 2,
        GameField::Fee => 3,
        GameField::RequiredPlayerCount => 4,
        GameField::AnonPlayer => 5,
    }
}

fn poll_field_to_u8(val: &PollField) -> u8 {
    match val {
        PollField::Question => 0,
        PollField::Description => 1,
        PollField::Place => 2,
        PollField::GameTime => 3,
        PollField::StartTime => 4,
        PollField::StartDay => 5,
        PollField::EndTime => 6,
        PollField::EndDay => 7,
    }
}

impl ToSql for PrivateDialogueState {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        match self {
            PrivateDialogueState::CommandMode => Ok(ToSqlOutput::from("0")),
            PrivateDialogueState::MenuMode {menu_message_id} => Ok(ToSqlOutput::from(format!("1|{}", menu_message_id.0))),

            PrivateDialogueState::AskForName => Ok(ToSqlOutput::from("2")),

            PrivateDialogueState::AskForNewName {user_id} => Ok(ToSqlOutput::from(format!("3|{}", user_id))),
            PrivateDialogueState::AskForNewMaxGames {user_id} => Ok(ToSqlOutput::from(format!("4|{}", user_id))),

            PrivateDialogueState::AskForDate {game_format} => Ok(ToSqlOutput::from(format!("5|{}", game_format_to_u8(game_format)))),
            PrivateDialogueState::AskForPlace {game_format, date_time} => Ok(ToSqlOutput::from(format!("6|{}|{}", game_format_to_u8(game_format), date_time))),
            PrivateDialogueState::AskForDescription {game_format, date_time, place} => Ok(ToSqlOutput::from(format!("7|{}|{}|{}", game_format_to_u8(game_format), date_time, place))),
            PrivateDialogueState::AskForFee {game_format, date_time, place, desc} => Ok(ToSqlOutput::from(format!("8|{}|{}|{}|{}", game_format_to_u8(game_format), date_time, place, desc))),
            PrivateDialogueState::AskIfFeeMandatory {game_format, date_time, place, desc, fee} => Ok(ToSqlOutput::from(format!("9|{}|{}|{}|{}|{}", game_format_to_u8(game_format), date_time, place, desc, fee))),
            PrivateDialogueState::AskPlayerCount {game_format, date_time, place, desc, fee, is_fee_mandatory} => Ok(ToSqlOutput::from(format!("10|{}|{}|{}|{}|{}|{}", game_format_to_u8(game_format), date_time, place, desc, fee, is_fee_mandatory))),

            PrivateDialogueState::EditGame {game_id, user_id, change_field} => Ok(ToSqlOutput::from(format!("11|{}|{}|{}", game_id, user_id, game_field_to_u8(change_field)))),

            PrivateDialogueState::AskForPollQuestion {game_format} => Ok(ToSqlOutput::from(format!("12|{}", game_format_to_u8(game_format)))),
            PrivateDialogueState::AskForPollDescription {game_format, question} => Ok(ToSqlOutput::from(format!("13|{}|{}", game_format_to_u8(game_format), question))),
            PrivateDialogueState::AskForPollPlace {game_format, question, description} => Ok(ToSqlOutput::from(format!("14|{}|{}|{}", game_format_to_u8(game_format), question, description))),
            PrivateDialogueState::AskForPollGameTime{game_format, question, description, place} => Ok(ToSqlOutput::from(format!("15|{}|{}|{}|{}", game_format_to_u8(game_format), question, description, place))),
            PrivateDialogueState::AskForPollStartDay{game_format, question, description, place, game_time} => Ok(ToSqlOutput::from(format!("16|{}|{}|{}|{}|{}", game_format_to_u8(game_format), question, description, place, game_time))),
            PrivateDialogueState::AskForPollStartTime{game_format, question, description, place, game_time, start_day} => Ok(ToSqlOutput::from(format!("17|{}|{}|{}|{}|{}|{}", game_format_to_u8(game_format), question, description, place, game_time, start_day))),
            PrivateDialogueState::AskForPollEndDay{game_format, question, description, place, game_time, start_day, start_time} => Ok(ToSqlOutput::from(format!("18|{}|{}|{}|{}|{}|{}|{}", game_format_to_u8(game_format), question, description, place, game_time, start_day, start_time))),
            PrivateDialogueState::AskForPollEndTime{game_format, question, description, place, game_time, start_day, start_time, end_day} => Ok(ToSqlOutput::from(format!("19|{}|{}|{}|{}|{}|{}|{}|{}", game_format_to_u8(game_format), question, description, place, game_time, start_day, start_time, end_day))),

            PrivateDialogueState::EditPoll { poll_id, user_id, change_field } => Ok(ToSqlOutput::from(format!("20|{}|{}|{}", poll_id, user_id, poll_field_to_u8(change_field)))),
        }
    }
}

fn u8_to_game_format(val: u8) -> Result<mtg::types::GameFormat, FromSqlError> {
    match val {
        0 => Ok(mtg::types::GameFormat::Commander),
        1 => Ok(mtg::types::GameFormat::Standard),
        2 => Ok(mtg::types::GameFormat::CommanderDraft),
        3 => Ok(mtg::types::GameFormat::StandardDraft),
        _ => Err(FromSqlError::OutOfRange(val as i64)),
    }
}

fn u8_to_game_field(val: u8) -> Result<GameField, FromSqlError> {
    match val {
        0 => Ok(GameField::Place),
        1 => Ok(GameField::Description),
        2 => Ok(GameField::DateTime),
        3 => Ok(GameField::Fee),
        4 => Ok(GameField::RequiredPlayerCount),
        5 => Ok(GameField::AnonPlayer),
        _ => Err(FromSqlError::OutOfRange(val as i64)),
    }
}

fn u8_to_poll_field(val: u8) -> Result<PollField, FromSqlError> {
    match val {
        0 => Ok(PollField::Question),
        1 => Ok(PollField::Description),
        2 => Ok(PollField::Place),
        3 => Ok(PollField::GameTime),
        4 => Ok(PollField::StartTime),
        5 => Ok(PollField::StartDay),
        6 => Ok(PollField::EndTime),
        7 => Ok(PollField::EndDay),
        _ => Err(FromSqlError::OutOfRange(val as i64)),
    }
}

impl FromSql for PrivateDialogueState {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let val = value.as_str()?;
        let parts = val.split("|").collect::<Vec<&str>>();
        let num = parts[0].parse::<u8>().expect("Invalid dialogue state");
        match num {
            0 => Ok(PrivateDialogueState::CommandMode),
            1 => {
                if parts.len() != 2 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let menu_message_id = MessageId(parts[1].parse::<i32>().expect("Invalid dialogue state"));
                    Ok(PrivateDialogueState::MenuMode { menu_message_id })
                }
            },
            2 => Ok(PrivateDialogueState::AskForName),
            3 => {
                if parts.len() != 2 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let user_id = parts[1].parse::<i64>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForNewName { user_id })
                }
            },
            4 => {
                if parts.len() != 2 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let user_id = parts[1].parse::<i64>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForNewMaxGames { user_id })
                }
            },
            5 => {
                if parts.len() != 2 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    Ok(PrivateDialogueState::AskForDate { game_format })
                }
            }
            6 => {
                if parts.len() != 3 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let date_time = parts[2].parse::<DateTime<FixedOffset>>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForPlace { game_format, date_time })
                }
            },
            7 => {
                if parts.len() != 4 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let date_time = parts[2].parse::<DateTime<FixedOffset>>().expect("Invalid dialogue state");
                    let place = parts[3].to_string();
                    Ok(PrivateDialogueState::AskForDescription { game_format, date_time, place })
                }
            },
            8 => {
                if parts.len() != 5 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let date_time = parts[2].parse::<DateTime<FixedOffset>>().expect("Invalid dialogue state");
                    let place = parts[3].to_string();
                    let desc = parts[4].to_string();
                    Ok(PrivateDialogueState::AskForFee { game_format, date_time, place, desc})
                }
            },
            9 => {
                if parts.len() != 6 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let date_time = parts[2].parse::<DateTime<FixedOffset>>().expect("Invalid dialogue state");
                    let place = parts[3].to_string();
                    let desc = parts[4].to_string();
                    let fee = parts[5].parse::<u32>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskIfFeeMandatory { game_format, date_time, place, desc, fee})
                }
            },
            10 => {
                if parts.len() != 7 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let date_time = parts[2].parse::<DateTime<FixedOffset>>().expect("Invalid dialogue state");
                    let place = parts[3].to_string();
                    let desc = parts[4].to_string();
                    let fee = parts[5].parse::<u32>().expect("Invalid dialogue state");
                    let is_fee_mandatory = parts[6].parse::<bool>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskPlayerCount { game_format, date_time, place, desc, fee, is_fee_mandatory})
                }
            },
            11 => {
                if parts.len() != 4 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_id = parts[1].parse::<i64>().expect("Invalid dialogue state");
                    let user_id = parts[2].parse::<i64>().expect("Invalid dialogue state");
                    let change_field = u8_to_game_field(parts[3].parse::<u8>().expect("Invalid dialogue state"))?;
                    Ok(PrivateDialogueState::EditGame { game_id, user_id, change_field })
                }
            },
            12 => {
                if parts.len() != 2 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    Ok(PrivateDialogueState::AskForPollQuestion { game_format })
                }
            },
            13 => {
                if parts.len() != 3 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    Ok(PrivateDialogueState::AskForPollDescription { game_format, question})
                }
            },
            14 => {
                if parts.len() != 4 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    Ok(PrivateDialogueState::AskForPollPlace { game_format, question, description})
                }
            },
            15 => {
                if parts.len() != 5 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    let place = parts[4].to_string();
                    Ok(PrivateDialogueState::AskForPollGameTime { game_format, question, description, place})
                }
            },
            16 => {
                if parts.len() != 6 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    let place = parts[4].to_string();
                    let game_time = parts[5].parse::<NaiveTime>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForPollStartDay { game_format, question, description, place, game_time})
                }
            },
            17 => {
                if parts.len() != 7 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    let place = parts[4].to_string();
                    let game_time = parts[5].parse::<NaiveTime>().expect("Invalid dialogue state");
                    let start_day = parts[6].parse::<u8>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForPollStartTime { game_format, question, description, place, game_time, start_day})
                }
            },
            18 => {
                if parts.len() != 8 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    let place = parts[4].to_string();
                    let game_time = parts[5].parse::<NaiveTime>().expect("Invalid dialogue state");
                    let start_day = parts[6].parse::<u8>().expect("Invalid dialogue state");
                    let start_time = parts[7].parse::<NaiveTime>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForPollEndDay { game_format, question, description, place, game_time, start_day, start_time})
                }
            },
            19 => {
                if parts.len() != 9 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let game_format = u8_to_game_format(parts[1].parse::<u8>().expect("Invalid dialogue state"))?;
                    let question = parts[2].to_string();
                    let description = parts[3].to_string();
                    let place = parts[4].to_string();
                    let game_time = parts[5].parse::<NaiveTime>().expect("Invalid dialogue state");
                    let start_day = parts[6].parse::<u8>().expect("Invalid dialogue state");
                    let start_time = parts[7].parse::<NaiveTime>().expect("Invalid dialogue state");
                    let end_day = parts[8].parse::<u8>().expect("Invalid dialogue state");
                    Ok(PrivateDialogueState::AskForPollEndTime { game_format, question, description, place, game_time, start_day, start_time, end_day})
                }
            },
            20 => {
                if parts.len() != 4 {
                    Err(FromSqlError::InvalidType)
                } else {
                    let poll_id = parts[1].parse::<i64>().expect("Invalid dialogue state");
                    let user_id = parts[2].parse::<i64>().expect("Invalid dialogue state");
                    let change_field = u8_to_poll_field(parts[3].parse::<u8>().expect("Invalid dialogue state"))?;
                    Ok(PrivateDialogueState::EditPoll { poll_id, user_id, change_field })
                }
            }
            _ => Err(FromSqlError::OutOfRange(num as i64)),
        }
    }
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase")]
enum PrivateCommand {
    Help,
    Start{arg: String},
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase")]
enum ChatCommand {
    Help,
    Init,
    DefaultPlace{default_place: String},
    Delete,
    CurrentResults,
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase")]
enum AlwaysOnPrivateCommand {
    Cancel
}

async fn scheduler_function(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    me: Me,
) -> ! {
    let mut chat_id = ChatId(0);

    while chat_id.0 == 0 {
        chat_id = manager.lock().await.get_chat_id().unwrap_or(ChatId(0));
        // Sleep for 1 minute
        tokio::time::sleep(Duration::from_secs(60 * 1)).await;
    }

    loop {
        log::debug!("Scheduler loop start");

        log::debug!("Checking polls to send");
        let polls_to_send = manager.lock().await.get_polls_to_send();
        for poll_id in polls_to_send {
            log::info!("Sending poll with id {}", poll_id);
            let mut manager_guard = manager.lock().await;
            match bot.send_poll(chat_id, manager_guard.get_poll_question(poll_id), manager_guard.get_poll_options(poll_id)).allows_multiple_answers(true).is_anonymous(false).await {
                Ok(msg) => {
                    let tg_poll_id = &msg.poll().unwrap().id;
                    manager_guard.attach_poll_and_msg(poll_id, msg.id, tg_poll_id);
                    match bot.pin_chat_message(chat_id, msg.id).await {
                        Ok(_) => {
                            log::info!("Pinned poll message with id {}", msg.id)
                        },
                        Err(err) => {
                            log::warn!("Failed to pin poll message with id {} with error: {}", msg.id, err)
                        }
                    }
                    log::info!("Poll with id {} has message id {} and telegram id {}", poll_id, msg.id, tg_poll_id);
                }
                Err(err) => {
                    log::error!("Failed sending poll with id {} with error: {}", poll_id, err);
                }
            }
        }

        log::debug!("Checking polls to process");
        let polls_to_process = manager.lock().await.get_polls_to_process();
        for poll_id in polls_to_process {
            log::info!("Processing results for poll with id {}", poll_id);
            let message_id = manager.lock().await.get_poll_message_id(poll_id);
            match bot.stop_poll(chat_id, message_id).await {
                Ok(_) => {
                    log::info!("Stopped poll with id {} and message id {}", poll_id, message_id.0);
                }
                Err(err) => {
                    log::warn!("Failed to stop poll with id {} and message id {} with error: {}", poll_id, message_id.0, err);
                }
            }
            match bot.unpin_chat_message(chat_id).message_id(message_id).await {
                Ok(_) => {
                    log::info!("Unpinned message with id {}", message_id.0);
                }
                Err(err) => {
                    log::warn!("Failed unpin message with id {} with error: {}", message_id.0, err);
                }
            }
            let (games_with_players, winning_days, wanting_players, checking_players) = manager.lock().await.process_poll_results(poll_id, false);
            if games_with_players.is_empty() {
                log::info!("Poll with id {} completed with not enough players", poll_id);
                match bot.send_message(chat_id, "Нет ни одного дня с более чем 3-мя голосами. Если всё равно хотите сыграть - пожалуйста, создайте игры самостоятельно").await {
                    Ok(_) => {
                        log::info!("Sent sad message to chat for poll with id {}", poll_id);
                    }
                    Err(err) => {
                        log::error!("Failed to send sad message to chat for poll with id {} with error: {}", poll_id, err);
                    }
                }
            } else {
                for (game, users) in games_with_players {
                    let mut new_game = game;
                    let message = match bot.send_message(chat_id, "[новая игра]").await {
                        Ok(msg) => msg,
                        Err(err) => {
                            log::error!("Failed sending empty message for game creation with error: {}", err);
                            continue;
                        }
                    };
                    new_game.message_id = message.id;
                    new_game = manager.lock().await.add_game(new_game).unwrap();
                    log::info!("New game with id {} was created", new_game.id);
                    for user in users {
                        if !manager.lock().await.join_game(new_game.id, user) {
                            log::warn!("Failed auto-joining user {} to game {}", user, new_game.id);
                        }
                    }
                    new_game = manager.lock().await.get_game(new_game.id).unwrap();
                    fill_missing_player_info(&bot, chat_id, &mut new_game.registrations).await.unwrap_or_default();
                    let (text, markup) = mtg::menus::create_game_menu(&new_game, me.tme_url().as_str());
                    match bot.edit_message_text(chat_id, message.id, text).parse_mode(ParseMode::Html).await {
                        Ok(_) => {},
                        Err(err) => {
                            log::error!("Failed editing game message text with error: {}", err);
                        }
                    }
                    match bot.edit_message_reply_markup(chat_id, message.id).reply_markup(markup).await {
                        Ok(_) => {},
                        Err(err) => {
                            log::error!("Failed editing game message reply markup with error: {}", err);
                        }
                    }
                    match bot.pin_chat_message(chat_id, message.id).await {
                        Ok(_) => {},
                        Err(err) => {
                            log::warn!("Failed to pin message for game with id {} with error: {}", new_game.id, err);
                        }
                    }
                }
                let mention = if !wanting_players.is_empty() {
                    let mut player_tags = String::new();
                    for player in wanting_players {
                        match bot.get_chat_member(chat_id, player).await {
                            Ok(chat_member) => player_tags += format!("{} ", user_mention_or_link(&chat_member.user)).as_str(),
                            Err(_) => continue
                        }
                    }
                    format!("\n{}, к сожалению, ваши дни не победили, но вы можете присоединиться к созданным играм или создать игру самостоятельно", player_tags)
                } else {
                    "".to_string()
                };

                match bot.send_message(chat_id, format!("Опрос завершён, победили дни: {}{}", winning_days, mention)).reply_to_message_id(message_id).await {
                    Ok(_) => {
                        log::info!("Sent results for poll with id {}", poll_id);
                    }
                    Err(err) => {
                        log::error!("Failed to send results for poll with id {} with error: {}", poll_id, err);
                    }
                }
            }
            if !checking_players.is_empty() {
                let mut checkers_tags = String::new();
                for player in checking_players {
                    match bot.get_chat_member(chat_id, player).await {
                        Ok(chat_member) => checkers_tags += format!("{} ", user_mention_or_link(&chat_member.user)).as_str(),
                        Err(_) => continue
                    }
                }
                match bot.send_message(chat_id, format!("{}, докладываю обстановку: победили дни: {}", checkers_tags, winning_days)).reply_to_message_id(message_id).await {
                    Ok(_) => {
                        log::info!("Tagged checking players for poll with id {}", poll_id);
                    }
                    Err(err) => {
                        log::error!("Failed to tag checking players for poll with id {} with error: {}", poll_id, err);
                    }
                }
            }
        }

        log::debug!("Checking for outdated games");
        let outdated_games = manager.lock().await.get_outdated_games();
        for game_id in outdated_games {
            log::info!("Deleting outdated game with id {}", game_id);
            let (game_deleted, msg_id) = manager.lock().await.delete_game(game_id);
            if game_deleted {
                log::info!("Deleted game with id: {}", game_id);
                match bot.edit_message_reply_markup(chat_id, msg_id).await {
                    Ok(_) => {
                        log::info!("Removed buttons from message with id {}", msg_id);
                    }
                    Err(err) => {
                        log::warn!("Failed removing buttons from message with id {} with error: {}", msg_id, err);
                    }
                }
                match bot.unpin_chat_message(chat_id).message_id(msg_id).await {
                    Ok(_) => {
                        log::info!("Unpinned message with id {}", msg_id);
                    }
                    Err(err) => {
                        log::warn!("Failed unpinning message with id {} with error: {}", msg_id, err);
                    }
                }
            } else {
                log::warn!("Failed deleting outdated game with id {}", game_id);
                continue;
            }
        }

        log::debug!("Checking notifications to send");
        let notifications = manager.lock().await.get_pending_notifications();
        for notification in notifications {
            log::info!("Sending notification \"{}\" to \"{}\"", notification.text, notification.telegram_id);
            match bot.send_message(notification.telegram_id, &notification.text).await {
                Ok(_) => {
                    log::info!("Notification sent to user {}", notification.telegram_id);
                }
                Err(err) => {
                    log::warn!("Failed to send notification to user {} with error: {}", notification.telegram_id, err);
                }
            }
        }

        // Sleep for 30 minutes
        log::debug!("Scheduler going to sleep");
        tokio::time::sleep(Duration::from_secs(60 * 30)).await;
    }
}

async fn verify_chat_membership_message(
    manager: &Arc<Mutex<mtg::Manager>>,
    bot: &Bot,
    msg: &Message,
) ->  Result<bool, Box<dyn std::error::Error + Send + Sync>> {
    let user_id = match msg.from() {
        Some(user) => { user.id }
        _ => { return Ok(false); }
    };

    // Try to verify via user database
    match manager.lock().await.get_player_id(user_id) {
        Ok(_) => { return Ok(true); }
        Err(_) => { /* Not registered yet */ }
    };

    let chat_id = match manager.lock().await.get_chat_id() {
        Ok(chat_id) => { chat_id },
        Err(_) => { return Ok(false); }
    };

    let chat_member = bot.get_chat_member(chat_id, user_id).await?;
    Ok(chat_member.is_present())
}

async fn verify_chat_membership_callback(
    manager: &Arc<Mutex<mtg::Manager>>,
    bot: &Bot,
    q: &CallbackQuery,
) ->  Result<bool, Box<dyn std::error::Error + Send + Sync>> {
    let chat_id = manager.lock().await.get_chat_id()?;
    let chat_member = bot.get_chat_member(chat_id, q.from.id).await?;
    Ok(chat_member.is_present())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    pretty_env_logger::init();
    log::info!("Starting MTG bot...");

    if !std::path::Path::new("./tmp").exists() {
        std::fs::create_dir("./tmp").expect("Failed to create tmp directory!");
    }

    let handler = dptree::entry()
        // Messages in private chat
        .branch(Update::filter_message()
            .filter(|msg: Message| msg.chat.is_private())
            .enter_dialogue::<Message, dialogue_storage::DialogueStorage<PrivateDialogueState>, PrivateDialogueState>()
            .branch(dptree::case![PrivateDialogueState::CommandMode]
                .filter_command::<PrivateCommand>()
                .endpoint(private_command_handler)
            )
            .branch(dptree::case![PrivateDialogueState::MenuMode {menu_message_id}]
                .filter_command::<PrivateCommand>()
                .endpoint(close_menu_handler)
            )
            .branch(dptree::case![PrivateDialogueState::AskForName]
                .endpoint(receive_display_name)
            )
            .branch(dptree::case![PrivateDialogueState::AskForNewName {user_id}]
                .endpoint(receive_new_display_name)
            )
            .branch(dptree::case![PrivateDialogueState::AskForNewMaxGames {user_id}]
                .endpoint(receive_new_max_games_in_week)
            )
            .branch(dptree::case![PrivateDialogueState::AskForDate {game_format}]
                .endpoint(receive_game_date)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPlace {game_format, date_time}]
                .endpoint(receive_game_place)
            )
            .branch(dptree::case![PrivateDialogueState::AskForDescription {game_format, date_time, place}]
                .endpoint(receive_game_desc)
            )
            .branch(dptree::case![PrivateDialogueState::AskForFee {game_format, date_time, place, desc}]
                .endpoint(receive_game_fee)
            )
            .branch(dptree::case![PrivateDialogueState::AskIfFeeMandatory {game_format, date_time, place, desc, fee}]
                .endpoint(receive_game_is_mandatory_fee)
            )
            .branch(dptree::case![PrivateDialogueState::AskPlayerCount {game_format, date_time, place, desc, fee, is_fee_mandatory}]
                .endpoint(receive_req_player_count)
            )
            .branch(dptree::case![PrivateDialogueState::EditGame {game_id, user_id, change_field}]
                .endpoint(receive_game_edit)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollQuestion {game_format}]
                .endpoint(receive_poll_question)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollDescription {game_format, question}]
                .endpoint(receive_poll_description)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollPlace {game_format, question, description}]
                .endpoint(receive_poll_place)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollGameTime {game_format, question, description, place}]
                .endpoint(receive_poll_game_time)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollStartDay {game_format, question, description, place, game_time}]
                .endpoint(receive_poll_start_day)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollStartTime {game_format, question, description, place, game_time, start_day}]
                .endpoint(receive_poll_start_time)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollEndDay {game_format, question, description, place, game_time, start_day, start_time}]
                .endpoint(receive_poll_end_day)
            )
            .branch(dptree::case![PrivateDialogueState::AskForPollEndTime {game_format, question, description, place, game_time, start_day, start_time, end_day}]
                .endpoint(receive_poll_end_time)
            )
            .branch(dptree::case![PrivateDialogueState::EditPoll {poll_id, user_id, change_field}]
                .endpoint(receive_poll_edit)
            )
        )
        // Button presses in private chat
        .branch(Update::filter_callback_query()
            // In private chat user id matches chat id
            .filter(|q: CallbackQuery| q.from.id.0 == q.chat_id().unwrap_or(ChatId(0_i64)).0 as u64)
            .enter_dialogue::<CallbackQuery, dialogue_storage::DialogueStorage<PrivateDialogueState>, PrivateDialogueState>()
            .branch(dptree::case![PrivateDialogueState::MenuMode {menu_message_id}]
                .endpoint(private_callback_handler)
            )
            .branch(dptree::case![PrivateDialogueState::CommandMode]
                .endpoint(stale_callback_handler)
            )
        )
        // Commands in group chat
        .branch(Update::filter_message()
            .filter(|msg: Message| msg.chat.is_group() || msg.chat.is_supergroup())
            .filter_command::<ChatCommand>()
            .endpoint(chat_command_handler)
        )
        // Messages in group chat
        .branch(Update::filter_message()
            .filter(|msg: Message| msg.chat.is_group() || msg.chat.is_supergroup())
            .endpoint(chat_message_handler)
        )
        // Button presses in chat
        .branch(Update::filter_callback_query()
            // In group chat user id does not match chat id
            .filter(|q: CallbackQuery| q.from.id.0 != q.chat_id().unwrap_or(ChatId(0_i64)).0 as u64)
            .endpoint(chat_callback_handler)
        )
        // Poll answers
        .branch(Update::filter_poll_answer()
            .endpoint(chat_poll_handler)
        );

    let mtg_manager = Arc::new(Mutex::new(mtg::Manager::new()));

    log::info!("Creating bot...");
    let bot = Bot::from_env();

    log::info!("Preparing scheduler...");
    let bot_clone = bot.clone();
    let mtg_manager_clone = mtg_manager.clone();
    let mut me_res = bot.get_me().await;
    while matches!(me_res, Err(_)) {
        tokio::time::sleep(Duration::from_secs(30 * 1)).await;
        me_res = bot.get_me().await;
    }
    let me = me_res.unwrap();

    log::info!("Starting scheduler...");
    tokio::spawn(async move {
        scheduler_function(mtg_manager_clone, bot_clone, me).await;
    });

    let mut headers = reqwest::header::HeaderMap::new();
    headers.insert(reqwest::header::USER_AGENT, reqwest::header::HeaderValue::from_static("mtg-bot-rust/1.0.1"));
    headers.insert("Accept", reqwest::header::HeaderValue::from_static("application/json"));
    let client = Arc::new(Mutex::new(reqwest::Client::builder()
        .default_headers(headers)
        .build()?
    ));

    log::info!("MTG bot started!");
    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![client, mtg_manager, dialogue_storage::DialogueStorage::<PrivateDialogueState>::new()])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
    Ok(())
}

async fn fill_missing_player_info(bot: &Bot, chat_id: ChatId, registrations: &mut Vec<mtg::types::GameRegistration>) -> HandlerResult {
    for reg in registrations {
        match reg.telegram_id {
            Some(telegram_id) => {
                let user = bot.get_chat_member(chat_id, telegram_id).await?.user;
                match reg.display_name {
                    Some(_) => {
                        // Nothing to do!
                    }
                    None => {
                        reg.display_name = Some(user.full_name());
                    }
                }
                reg.telegram_tag = Some(user_mention_or_link(&user));
            }
            None => {
                // Nothing to do!
            }
        }
    }
    Ok(())
}

async fn update_game_msg(manager_guard: &mut MutexGuard<'_, mtg::Manager>, bot: &Bot, me: &Me, game_id: i64) -> HandlerResult {
    let chat_id = manager_guard.get_chat_id()?;
    let mut game = manager_guard.get_game(game_id)?;
    fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
    let (text, markup) = mtg::menus::create_game_menu(&game, me.tme_url().as_str());
    bot.edit_message_text(chat_id, game.message_id, text).parse_mode(ParseMode::Html).await?;
    bot.edit_message_reply_markup(chat_id, game.message_id).reply_markup(markup).await?;

    Ok(())
}

async fn download_file(
    url: String,
    filename: String,
    client: &Arc<Mutex<reqwest::Client>>,
) -> HandlerResult {
    log::debug!("Downloading \"{}\"", url);
    let res = client.lock().await.get(url).send().await;
    match res {
        Ok(response) => {
            let mut file = std::fs::File::create(filename)?;
            let mut content = std::io::Cursor::new(response.bytes().await?);
            std::io::copy(&mut content, &mut file)?;
        }
        Err(err) => {
            log::error!("Failed downloading file with error: {}", err);
        }
    }

    Ok(())
}

async fn chat_message_handler(
    client: Arc<Mutex<reqwest::Client>>,
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    msg: Message,
) -> HandlerResult {
    let chat_id = match manager.lock().await.get_chat_id() {
        Ok(chat_id) => chat_id,
        Err(_) => { return Ok(()); }
    };
    if chat_id != msg.chat.id {
        return Ok(());
    }

    let mut opt_text : Option<&str> = None;
    match msg.text() {
        Some(text) => {
            opt_text = Some(text);
        },
        None => {
            match msg.caption() {
                Some(caption) => {
                    opt_text = Some(caption)
                },
                None => {
                    // Nothing else we could try
                },
            }
        },
    }

    match opt_text {
        Some(text) => {
            let card_info_re = regex::Regex::new(r#"\[\[([A-Za-zА-Яа-я0-9!@#$%^&*()_+\-={}:;\"'<>,.?\/ ]+)(\|( )*[A-Za-z0-9]{3}( )*)?\]\]"#).unwrap();
            let mut card_infos: Vec<(String, Option<String>)> = vec![];

            for card_info in card_info_re.find_iter(text).map(|m| { m.as_str().replace("[[", "").replace("]]", "") }) {
                if card_info.contains('|') {
                    let card_info_split = card_info.split('|').collect::<Vec<&str>>();
                    card_infos.push((card_info_split[0].to_string(), Some(card_info_split[1].replace(" ", ""))))
                } else {
                    card_infos.push((card_info, None));
                }
            }

            if !card_infos.is_empty() {
                let mut found_all_cards = true;
                let mut found_all_images = true;
                let mut not_found_list = String::new();
                let mut no_image_list = String::new();

                std::fs::create_dir(format!("./tmp/{}", msg.id.0 as u32))?;
                for current_block in 0..=(card_infos.len() / 5) {
                    let first_card = current_block * 5;
                    let last_card = min(current_block * 5 + 4, card_infos.len() - 1);

                    let mut files : Vec<InputMedia> = vec![];
                    for card_num in first_card..=last_card {
                        let card_info = &card_infos[card_num];

                        // Scryfall asks for delay between API requests
                        tokio::time::sleep(Duration::from_millis(100)).await;

                        let reply = match &card_info.1 {
                            Some(set) => client.lock().await.get(format!("https://api.scryfall.com/cards/named?fuzzy={}&set={}", urlencoding::encode(card_info.0.as_str()), set)).send(),
                            None => client.lock().await.get(format!("https://api.scryfall.com/cards/named?fuzzy={}", urlencoding::encode(card_info.0.as_str()))).send(),
                        }.await?.text().await?;

                        let res = json::parse(&*reply)?;
                        log::debug!("Processing card {}", card_info.0);
                        if res["status"].is_null() {
                            if !res["image_uris"].is_null() {
                                let image_url = res["image_uris"]["png"].to_string();

                                // Scryfall asks for delay between API requests
                                tokio::time::sleep(Duration::from_millis(100)).await;
                                download_file(image_url, format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2), &client).await?;

                                files.push(InputMedia::Photo(InputMediaPhoto::new(InputFile::file(format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2)))));
                            } else if !res["card_faces"].is_null() && !res["card_faces"][0]["image_uris"].is_null() {
                                let image_url_face = res["card_faces"][0]["image_uris"]["png"].to_string();
                                let image_url_back = res["card_faces"][1]["image_uris"]["png"].to_string();

                                // Scryfall asks for delay between API requests
                                tokio::time::sleep(Duration::from_millis(100)).await;
                                download_file(image_url_face, format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2), &client).await?;

                                files.push(InputMedia::Photo(InputMediaPhoto::new(InputFile::file(format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2)))));

                                // Scryfall asks for delay between API requests
                                tokio::time::sleep(Duration::from_millis(100)).await;
                                download_file(image_url_back, format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2 + 1), &client).await?;

                                files.push(InputMedia::Photo(InputMediaPhoto::new(InputFile::file(format!("./tmp/{}/image_{}.png", msg.id.0, card_num * 2 + 1)))));
                            } else {
                                log::debug!("Could not extract image for the card, received: {}", res.dump());
                                found_all_images = false;
                                no_image_list += format!("- {}{}\n", card_info.0, match &card_info.1 {
                                    Some(set) => format!("(from set \"{}\")", set).to_string(),
                                    None => "".to_string(),
                                }).as_str();
                            }
                        } else {
                            log::debug!("Could not find the card, received: {}", res.dump());
                            found_all_cards = false;
                            not_found_list += format!("- {}{}\n", card_info.0, match &card_info.1 {
                                Some(set) => format!("(from set \"{}\")", set).to_string(),
                                None => "".to_string(),
                            }).as_str();
                        }
                    }

                    // This may be very big. Telegram might refuse to accept all the images.
                    // Need to wait if we receive RetryAfter
                    let mut should_repeat = true;
                    while should_repeat {
                        match bot.send_media_group(msg.chat.id, files.clone()).reply_to_message_id(msg.id).await {
                            Ok(_) => { should_repeat = false; }
                            Err(error) => {
                                if error.retry_after().is_none() {
                                    should_repeat = false;
                                } else {
                                    let user_info = match msg.from() {
                                        Some(user) => format!("{} (id: {})", user.full_name(), user.id),
                                        None => "ERR:USER".to_string(),
                                    };
                                    log::warn!("Huge card request from user {}", user_info);
                                    let timeout = error.retry_after().unwrap();
                                    log::warn!("Retrying after {} seconds", timeout.as_secs());
                                    tokio::time::sleep(timeout).await;
                                }
                            }
                        }
                    }
                }
                if !found_all_cards {
                    bot.send_message(msg.chat.id, format!("Не смог найти карты:\n{}", not_found_list).to_string()).reply_to_message_id(msg.id).await?;
                }
                if !found_all_images {
                    bot.send_message(msg.chat.id, format!("Не смог найти изображения карт:\n{}", no_image_list).to_string()).reply_to_message_id(msg.id).await?;
                }

                std::fs::remove_dir_all(format!("./tmp/{}", msg.id.0 as u32))?;
            }
        }
        None => {
            // Nothing to do
        }
    }

    Ok(())
}

async fn handle_always_on_cmd(
    manager: &Arc<Mutex<mtg::Manager>>,
    bot: &Bot,
    dialogue: &PrivateDialogue,
    msg: &Message,
) -> Result<bool, Box<dyn std::error::Error + Send + Sync>> {
    match msg.text() {
        Some(text) => {
            match AlwaysOnPrivateCommand::parse(text, bot.get_me().await?.username()) {
                Ok(cmd) => {
                    match cmd {
                        AlwaysOnPrivateCommand::Cancel => {
                            let manager_guard = manager.lock().await;

                            let player_id = manager_guard.get_player_id(msg.from().expect("Error getting user id from message!").id)?;
                            let allow_polls = manager_guard.is_admin(player_id);

                            bot.send_message(msg.chat.id, "Действие отменено!").reply_markup(KeyboardRemove::new()).await?;

                            let (text, markup) = mtg::menus::create_main_menu(player_id, allow_polls);
                            let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                            dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;

                            Ok(true)
                        }
                    }
                }
                Err(_) => {
                    Ok(false)
                }
            }
        }
        None => {
            Ok(false)
        }
    }
}

async fn receive_new_display_name(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    user_id: i64,
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(display_name) => {
            if display_name.chars().count() > 20 {
                bot.send_message(msg.chat.id, "Длина имени не должна превышать 20 символов").await?;
                return Ok(());
            }

            let name_changed = manager.lock().await.edit_player_name(user_id, display_name.to_string());
            if name_changed {
                bot.send_message(msg.chat.id, "Обновил твоё имя в профиле!").await?;
            } else {
                bot.send_message(msg.chat.id, "Ошибка при изменении имени!").await?;
                log::error!("Failed updating display name of user {}, new name \"{}\"", user_id, display_name)
            }
            let player = manager.lock().await.get_player(user_id, true, true)?;
            let (text, markup) = mtg::menus::create_profile_menu(player);
            let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
            dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
        }
        _ => {
            bot.send_message(msg.chat.id, "Пожалуйста, отправь мне простое сообщение!").await?;
        }
    }

    Ok(())
}

async fn receive_new_max_games_in_week(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    user_id: i64,
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| text.parse::<u8>() ) {
        Some(Ok(new_max_games_in_week)) => {
            if (1_u8..=5_u8).contains(&new_max_games_in_week) {
                let max_games_in_week_changed = manager.lock().await.edit_player_max_games_in_week(user_id, new_max_games_in_week);
                if max_games_in_week_changed {
                    bot.send_message(msg.chat.id, "Обновил предпочтение в профиле!").await?;
                } else {
                    bot.send_message(msg.chat.id, "Ошибка при изменении предпочтений!").await?;
                    log::error!("Failed updating max_games_in_week of user {}, new value \"{}\"", user_id, new_max_games_in_week)
                }
                let player = manager.lock().await.get_player(user_id, true, true)?;
                let (text, markup) = mtg::menus::create_profile_menu(player);
                let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                dialogue.update(PrivateDialogueState::MenuMode { menu_message_id: menu_message.id }).await?;
            } else {
                bot.send_message(msg.chat.id, "Пожалуйста, отправь число от 1 до 5").await?;
            }
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты что-то кроме числа. Попробуй ещё раз").await?;
        },
        _ => {
            bot.send_message(msg.chat.id, "Пожалуйста, отправь мне простое сообщение!").await?;
        }
    }

    Ok(())
}

async fn receive_display_name(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(display_name) => {
            if display_name.chars().count() > 20 {
                bot.send_message(msg.chat.id, "Длина имени не должна превышать 20 символов").await?;
                return Ok(());
            }

            let telegram_id = match msg.from() {
                Some(user) => { user.id },
                _ => {
                    bot.send_message(msg.chat.id, "Пожалуйста, отправь простое сообщение").await?;
                    return Ok(())
                }
            };

            let manager_guard = manager.lock().await;
            match manager_guard.add_player(telegram_id, display_name) {
                Ok(id) => {
                    log::info!("New user registered with id = {}, telegram id = {}", id, telegram_id);
                    bot.send_message(msg.chat.id, format!("Регистрация успешна! Ваш идентификатор: {}", id)).await?;
                    let (text, markup) = mtg::menus::create_main_menu(id, false);
                    let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                    dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
                },
                Err(err) => {
                    log::error!("Failed to register user with id {} with error {}", telegram_id, err);
                    bot.send_message(msg.chat.id, format!("Ошибка регистрации! {}", err)).await?;
                    dialogue.update(PrivateDialogueState::CommandMode).await?;
                }
            }
        }
        _ => {
            bot.send_message(msg.chat.id, "Пожалуйста, отправь мне простое сообщение!").await?;
        }
    }

    Ok(())
}

async fn receive_game_date(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    game_format: mtg::types::GameFormat,
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| NaiveDateTime::parse_from_str(text, "%d.%m.%Y %H:%M")) {
        Some(Ok(naive_date_time)) => {
            let offset = manager.lock().await.get_offset();
            let date_time: DateTime<FixedOffset> = DateTime::from_naive_utc_and_offset(naive_date_time.checked_sub_offset(offset).unwrap(), offset);
            if date_time > chrono::Utc::now().with_timezone(&offset) {
                dialogue.update(PrivateDialogueState::AskForPlace {game_format, date_time}).await?;
                match manager.lock().await.get_default_place() {
                    Ok(default_place) => {
                        bot.send_message(msg.chat.id, "Отлично, теперь напиши где будет проходить игра:").reply_markup(KeyboardMarkup::new(vec![vec![KeyboardButton::new(default_place)]])).await?;
                    },
                    Err(_) => {
                        bot.send_message(msg.chat.id, "Отлично, теперь напиши где будет проходить игра:").reply_markup(KeyboardRemove::new()).await?;
                    }
                };
            } else {
                bot.send_message(msg.chat.id, "Кажется, игра происходит в прошлом, она будет автоматически удалена в течение 30 минут. Попробуй ещё раз").await?;
            }
        }
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал дату и время в неверном формате. Попробуй ещё раз").await?;
        }
        None => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }
    Ok(())
}

async fn receive_game_place(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, date_time): (mtg::types::GameFormat, DateTime<FixedOffset>),
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(place) => {
            dialogue.update(PrivateDialogueState::AskForDescription {game_format, date_time, place: place.to_string()}).await?;
            bot.send_message(msg.chat.id, "Отлично, теперь введи описание игры:").reply_markup(KeyboardMarkup::new(vec![vec![KeyboardButton::new("Оставить пустым")]])).await?;
        }
        _ => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }

    Ok(())
}

async fn receive_game_desc(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, date_time, place): (mtg::types::GameFormat, DateTime<FixedOffset>, String),
    msg: Message,
    me: Me,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(received_desc) => {
            let desc = if received_desc == "Оставить пустым" {
                String::new()
            } else {
                received_desc.to_string().replace('|', "")
            };

            match game_format {
                mtg::types::GameFormat::Commander | mtg::types::GameFormat::Standard => {
                    dialogue.update(PrivateDialogueState::CommandMode).await?;
                    let chat_id = manager.lock().await.get_chat_id()?;
                    let creator_id = manager.lock().await.get_player_id(msg.from().expect("Error getting user id from message!").id)?;
                    let message = bot.send_message(chat_id, "[новая игра]").await?;
                    let game = manager.lock().await.add_game(mtg::types::Game{
                        id: 0,
                        message_id: message.id,
                        creator_id,
                        place,
                        description: desc,
                        date_time,
                        required_player_count: 0,
                        format: game_format,
                        participation_fee: 0,
                        mandatory_fee: false,
                        registrations: vec![]
                    })?;
                    let (text, markup) = mtg::menus::create_game_menu(&game, me.tme_url().as_str());
                    bot.edit_message_text(chat_id, message.id, text).parse_mode(ParseMode::Html).await?;
                    bot.edit_message_reply_markup(chat_id, message.id).reply_markup(markup).await?;
                    match bot.pin_chat_message(chat_id, message.id).await {
                        Ok(_) => {},
                        Err(err) => {
                            log::warn!("Failed to pin message for game with id {} with error: {}", game.id, err);
                        }
                    }
                    bot.send_message(msg.chat.id, "Игра успешно создана и отправлена в чат!").reply_markup(KeyboardRemove::new()).await?;
                    log::info!("New game with id {} was created", game.id);
                }
                mtg::types::GameFormat::CommanderDraft | mtg::types::GameFormat::StandardDraft => {
                    dialogue.update(PrivateDialogueState::AskForFee {game_format, date_time, place, desc}).await?;
                    bot.send_message(msg.chat.id, "Отлично, теперь введи взнос за игру:").reply_markup(KeyboardRemove::new()).await?;
                }
            }

        }
        _ => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }

    Ok(())
}

async fn receive_game_fee(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, date_time, place, desc): (mtg::types::GameFormat, DateTime<FixedOffset>, String, String),
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| text.parse::<u32>()) {
        Some(Ok(fee)) => {
            dialogue.update(PrivateDialogueState::AskIfFeeMandatory {game_format, date_time, place, desc, fee}).await?;
            bot.send_message(msg.chat.id, "Отлично, теперь напиши обязателен ли взнос [Да / Нет]:").reply_markup(KeyboardMarkup::new(vec![vec![KeyboardButton::new("Да")], vec![KeyboardButton::new("Нет")]])).await?;
        }
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты что-то кроме числа. Попробуй ещё раз").await?;
        }
        _ => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }
    Ok(())
}

async fn receive_game_is_mandatory_fee(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, date_time, place, desc, fee): (mtg::types::GameFormat, DateTime<FixedOffset>, String, String, u32),
    msg: Message,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(text) => {
            let is_fee_mandatory: bool;
            if text.to_lowercase().starts_with("да") {
                is_fee_mandatory = true;
            } else if text.to_lowercase().starts_with("нет") {
                is_fee_mandatory = false;
            } else {
                bot.send_message(msg.chat.id, "Кажется, ты что-то кроме 'Да' или 'Нет'. Попробуй ещё раз").await?;
                return Ok(())
            }
            dialogue.update(PrivateDialogueState::AskPlayerCount {game_format, date_time, place, desc, fee, is_fee_mandatory}).await?;
            bot.send_message(msg.chat.id, "Отлично, теперь введи, сколько требуется игроков для игры:").reply_markup(KeyboardRemove::new()).await?;
        }
        _ => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }
    Ok(())
}

async fn receive_req_player_count(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, date_time, place, desc, fee, is_fee_mandatory): (mtg::types::GameFormat, DateTime<FixedOffset>, String, String, u32, bool),
    msg: Message,
    me: Me,
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| text.parse::<u32>()) {
        Some(Ok(required_player_count)) => {
            dialogue.update(PrivateDialogueState::CommandMode).await?;
            let chat_id = manager.lock().await.get_chat_id()?;
            let creator_id = manager.lock().await.get_player_id(msg.from().expect("Error getting user id from message!").id)?;
            let message = bot.send_message(chat_id, "[новая игра]").await?;
            let game = manager.lock().await.add_game(mtg::types::Game{
                id: 0,
                message_id: message.id,
                creator_id,
                place,
                description: desc,
                date_time,
                required_player_count,
                format: game_format,
                participation_fee: fee,
                mandatory_fee: is_fee_mandatory,
                registrations: vec![]
            })?;
            let (text, markup) = mtg::menus::create_game_menu(&game, me.tme_url().as_str());
            bot.edit_message_text(chat_id, message.id, text).parse_mode(ParseMode::Html).await?;
            bot.edit_message_reply_markup(chat_id, message.id).reply_markup(markup).await?;
            match bot.pin_chat_message(chat_id, message.id).await {
                Ok(_) => {},
                Err(err) => {
                    log::warn!("Failed to pin message for game with id {} with error: {}", game.id, err);
                }
            }
            bot.send_message(msg.chat.id, "Игра успешно создана и отправлена в чат!").reply_markup(KeyboardRemove::new()).await?;
            log::info!("New game with id {} was created", game.id);
        }
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты что-то кроме числа. Попробуй ещё раз").await?;
        }
        _ => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }
    Ok(())
}

async fn receive_game_edit(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_id, user_id, game_field): (i64, i64, GameField),
    msg: Message,
    me: Me,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(text) => {
            let mut should_update_game = false;
            let mut should_return_to_menu = false;
            match game_field {
                GameField::Place => {
                    let updated = manager.lock().await.edit_game_place(game_id, text);
                    if updated {
                        should_update_game = true;
                        bot.send_message(msg.chat.id, "Место игры обновлено!").await?;
                    } else {
                        bot.send_message(msg.chat.id, "Ошибка обновления места игры!").await?;
                        log::error!("Error updating game place! game id: {}, player id: {}, new place: {}", game_id, user_id, text);
                    }
                    should_return_to_menu = true;
                },
                GameField::Description => {
                    let updated = manager.lock().await.edit_game_desc(game_id, text);
                    if updated {
                        should_update_game = true;
                        bot.send_message(msg.chat.id, "Описание игры обновлено!").await?;
                    } else {
                        bot.send_message(msg.chat.id, "Ошибка обновления описания игры").await?;
                        log::error!("Error updating game description! game id: {}, player id: {}, new desc: {}", game_id, user_id, text);
                    }
                    should_return_to_menu = true;
                },
                GameField::DateTime => {
                    match NaiveDateTime::parse_from_str(text, "%d.%m.%Y %H:%M") {
                        Ok(naive_date_time) => {
                            let offset = manager.lock().await.get_offset();
                            let date_time: DateTime<FixedOffset> = DateTime::from_naive_utc_and_offset(naive_date_time.checked_sub_offset(offset).unwrap(), offset);
                            let updated = manager.lock().await.edit_game_date_time(game_id, date_time);
                            if updated {
                                should_update_game = true;
                                bot.send_message(msg.chat.id, "Дата и время игры обновлено!").await?;
                            } else {
                                bot.send_message(msg.chat.id, "Ошибка обновления даты и времени!").await?;
                                log::error!("Error updating game date amd time! game id: {}, player id: {}, new date and time: {}", game_id, user_id, date_time);
                            }
                            should_return_to_menu = true;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты написал дату и время в неверном формате. Попробуй ещё раз").await?;
                        }
                    }
                },
                GameField::Fee => {
                    match text.parse::<u32>() {
                        Ok(fee) => {
                            let updated = manager.lock().await.edit_game_participation_fee(game_id, fee);
                            if updated {
                                should_update_game = true;
                                bot.send_message(msg.chat.id, "Взнос за игру обновлён!").await?;
                            } else {
                                bot.send_message(msg.chat.id, "Ошибка обновления взноса за игру!").await?;
                                log::error!("Error updating game fee! game id: {}, player id: {}, new fee: {}", game_id, user_id, fee);
                            }
                            should_return_to_menu = true;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты что-то кроме числа. Попробуй ещё раз").await?;
                        }
                    }
                },
                GameField::RequiredPlayerCount => {
                    match text.parse::<u32>() {
                        Ok(required_player_count) => {
                            let updated = manager.lock().await.edit_game_required_player_count(game_id, required_player_count);
                            if updated {
                                should_update_game = true;
                                bot.send_message(msg.chat.id, "Требуемое количество игроков обновлено!").await?;
                            } else {
                                bot.send_message(msg.chat.id, "Ошибка обновления требуемого количества игроков!").await?;
                                log::error!("Error updating game player count! game id: {}, player id: {}, new player count: {}", game_id, user_id, required_player_count);
                            }
                            should_return_to_menu = true;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты что-то кроме числа. Попробуй ещё раз").await?;
                        }
                    }
                },
                GameField::AnonPlayer => {
                    let updated = manager.lock().await.add_super_anon_player(game_id, text);
                    if updated {
                        should_update_game = true;
                        bot.send_message(msg.chat.id, "Игрок добавлен!").await?;
                    } else {
                        bot.send_message(msg.chat.id, "Ошибка добавления игрока!").await?;
                        log::error!("Error adding player! game id: {}, player id: {}, new player: {}", game_id, user_id, text);
                    }
                    should_return_to_menu = true;
                }
            }
            if should_return_to_menu {
                if should_update_game {
                    let mut manager_guard = manager.lock().await;
                    update_game_msg(&mut manager_guard, &bot, &me, game_id).await?;
                }
                let (text, markup) = mtg::menus::create_game_management_menu(user_id);
                let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
            }
        }
        None => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }

    Ok(())
}

async fn receive_poll_question(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    game_format: mtg::types::GameFormat,
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(received_question) => {
            if received_question.len() >= 256 {
                bot.send_message(msg.chat.id, "Длина вопроса не должна превышать 255 символов").await?;
            } else {
                let question: String = received_question.replace("|", "");
                bot.send_message(msg.chat.id, "Введи описание для игр, которые будут созданы по результатам опроса:").reply_markup(KeyboardMarkup::new(vec![vec![KeyboardButton::new("Оставить пустым")]])).await?;
                dialogue.update(PrivateDialogueState::AskForPollDescription { game_format, question }).await?;
            }
        }
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое текстовое сообщение с вопросом для опроса").await?;
        }
    }

    Ok(())
}

async fn receive_poll_description(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question): (mtg::types::GameFormat, String),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(received_description) => {
            if received_description.len() >= 256 {
                bot.send_message(msg.chat.id, "Длина описания не должна превышать 255 символов").await?;
            } else {
                let description: String = if received_description == "Оставить пустым" {
                    "".to_string()
                } else {
                    received_description.replace("|", "")
                };
                match manager.lock().await.get_default_place() {
                    Ok(default_place) => {
                        bot.send_message(msg.chat.id, "Введи место для игр, которые будут созданы по результатам опроса:").reply_markup(KeyboardMarkup::new(vec![vec![KeyboardButton::new(default_place)]])).await?;
                    },
                    Err(_) => {
                        bot.send_message(msg.chat.id, "Введи место для игр, которые будут созданы по результатам опроса:").reply_markup(KeyboardRemove::new()).await?;
                    }
                };
                dialogue.update(PrivateDialogueState::AskForPollPlace { game_format, question, description }).await?;
            }
        }
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое текстовое сообщение с вопросом для опроса").await?;
        }
    }

    Ok(())
}

async fn receive_poll_place(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description): (mtg::types::GameFormat, String, String),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(received_place) => {
            if received_place.len() >= 256 {
                bot.send_message(msg.chat.id, "Длина места не должна превышать 255 символов").await?;
            } else {
                let place: String = received_place.replace("|", "");
                bot.send_message(msg.chat.id, "Введи время для игр, которые будут созданы по результатам опроса в формате \"HH:MM\":").reply_markup(KeyboardRemove::new()).await?;
                dialogue.update(PrivateDialogueState::AskForPollGameTime { game_format, question, description, place }).await?;
            }
        }
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое текстовое сообщение с вопросом для опроса").await?;
        }
    }

    Ok(())
}

async fn receive_poll_game_time(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description, place): (mtg::types::GameFormat, String, String, String),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| NaiveTime::parse_from_str(text, "%H:%M")) {
        Some(Ok(game_time)) => {
            bot.send_message(msg.chat.id, "Введи день недели (номер от 1 до 7), в который опрос должен начинаться:").await?;
            dialogue.update(PrivateDialogueState::AskForPollStartDay { game_format, question, description, place, game_time }).await?;
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
        },
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое сообщение с временем для создаваемых игр").await?;
        },
    }

    Ok(())
}

async fn receive_poll_start_day(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description, place, game_time): (mtg::types::GameFormat, String, String, String, NaiveTime),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| text.parse::<u8>()) {
        Some(Ok(start_day)) => {
            if 1 <= start_day && start_day <= 7 {
                bot.send_message(msg.chat.id, "Введи время, в которое опрос должен начинаться в формате \"HH:MM\":").await?;
                dialogue.update(PrivateDialogueState::AskForPollStartTime { game_format, question, description, place, game_time, start_day }).await?;
            } else {
                bot.send_message(msg.chat.id, "Число должно быть от 1 до 7. Попробуй ещё раз").await?;
            }
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал что-то кроме числа. Попробуй ещё раз").await?;
        },
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое сообщение с днём недели начала опроса").await?;
        },
    }

    Ok(())
}

async fn receive_poll_start_time(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description, place, game_time, start_day): (mtg::types::GameFormat, String, String, String, NaiveTime, u8),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| NaiveTime::parse_from_str(text, "%H:%M")) {
        Some(Ok(start_time)) => {
            bot.send_message(msg.chat.id, "Введи день недели (номер от 1 до 7), в который опрос должен завершаться:").await?;
            dialogue.update(PrivateDialogueState::AskForPollEndDay { game_format, question, description, place, game_time, start_day, start_time }).await?;
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
        },
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое сообщение с временем временем начала опроса").await?;
        },
    }

    Ok(())
}

async fn receive_poll_end_day(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description, place, game_time, start_day, start_time): (mtg::types::GameFormat, String, String, String, NaiveTime, u8, NaiveTime),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| text.parse::<u8>()) {
        Some(Ok(end_day)) => {
            if 1 <= end_day && end_day <= 7 {
                bot.send_message(msg.chat.id, "Введи время, в которое опрос должен завершаться в формате \"HH:MM\":").await?;
                dialogue.update(PrivateDialogueState::AskForPollEndTime { game_format, question, description, place, game_time, start_day, start_time, end_day }).await?;
            } else {
                bot.send_message(msg.chat.id, "Число должно быть от 1 до 7. Попробуй ещё раз").await?;
            }
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал что-то кроме числа. Попробуй ещё раз").await?;
        },
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое сообщение с днём недели завершения опроса").await?;
        },
    }

    Ok(())
}

async fn receive_poll_end_time(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (game_format, question, description, place, game_time, start_day, start_time, end_day): (mtg::types::GameFormat, String, String, String, NaiveTime, u8, NaiveTime, u8),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text().map(|text| NaiveTime::parse_from_str(text, "%H:%M")) {
        Some(Ok(end_time)) => {
            let manager_guard = manager.lock().await;
            let user_id = manager_guard.get_player_id(msg.from().unwrap().id)?;
            manager_guard.schedule_poll(user_id, question, description, place, game_format, game_time, start_time, start_day, end_time, end_day);
            bot.send_message(msg.chat.id, "Опрос успешно запланирован!").await?;
            let (text, markup) = mtg::menus::create_poll_management_menu(user_id);
            let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
            dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
        },
        Some(Err(_)) => {
            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
        },
        None => {
            bot.send_message(msg.chat.id, "Отправь мне простое сообщение с временем завершения опроса").await?;
        },
    }

    Ok(())
}

async fn receive_poll_edit(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    (poll_id, user_id, poll_field): (i64, i64, PollField),
    msg: Message,
) -> HandlerResult {
    if handle_always_on_cmd(&manager, &bot, &dialogue, &msg).await? {
        return Ok(());
    }

    match msg.text() {
        Some(text) => {
            let mut should_return_to_menu = false;
            match poll_field {
                PollField::Question => {
                    manager.lock().await.edit_poll_question(poll_id, text.to_string());
                    bot.send_message(msg.chat.id, "Вопрос для опроса успешно обновлён").await?;
                    should_return_to_menu = true;
                },
                PollField::Description => {
                    manager.lock().await.edit_poll_description(poll_id, text.to_string());
                    bot.send_message(msg.chat.id, "Описание для создаваемых игр успешно обновлён").await?;
                    should_return_to_menu = true;
                },
                PollField::Place => {
                    manager.lock().await.edit_poll_place(poll_id, text.to_string());
                    bot.send_message(msg.chat.id, "Место для создаваемых игр успешно обновлёно").await?;
                    should_return_to_menu = true;
                },
                PollField::GameTime => {
                    match NaiveTime::parse_from_str(text, "%H:%M") {
                        Ok(game_time) => {
                            should_return_to_menu = true;
                            manager.lock().await.edit_poll_game_time(poll_id, game_time);
                            bot.send_message(msg.chat.id, "Время создаваемых игр успешно обновлено").await?;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
                        },
                    }
                },
                PollField::StartTime => {
                    match NaiveTime::parse_from_str(text, "%H:%M") {
                        Ok(start_time) => {
                            should_return_to_menu = true;
                            manager.lock().await.edit_poll_start_time(poll_id, start_time);
                            bot.send_message(msg.chat.id, "Время начала опроса успешно обновлено").await?;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
                        },
                    }
                },
                PollField::StartDay => {
                    match text.parse::<u8>() {
                        Ok(start_day) => {
                            if start_day >= 1 && start_day <= 7 {
                                should_return_to_menu = true;
                                manager.lock().await.edit_poll_start_day(poll_id, start_day);
                                bot.send_message(msg.chat.id, "День начала опроса успешно обновлён").await?;
                            } else {
                                bot.send_message(msg.chat.id, "Кажется ты ввёл число не в диапозоне 1-7. Попробуй ещё раз").await?;
                            }
                        }
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется ты написал что-то кроме числа. Попробуй ещё раз").await?;
                        }
                    }
                },
                PollField::EndTime => {
                    match NaiveTime::parse_from_str(text, "%H:%M") {
                        Ok(end_time) => {
                            should_return_to_menu = true;
                            manager.lock().await.edit_poll_end_time(poll_id, end_time);
                            bot.send_message(msg.chat.id, "Время конца опроса успешно обновлено").await?;
                        },
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется, ты написал время в неверном формате. Попробуй ещё раз").await?;
                        },
                    }
                },
                PollField::EndDay => {
                    match text.parse::<u8>() {
                        Ok(end_day) => {
                            if end_day >= 1 && end_day <= 7 {
                                should_return_to_menu = true;
                                manager.lock().await.edit_poll_end_day(poll_id, end_day);
                                bot.send_message(msg.chat.id, "День конца опроса успешно обновлён").await?;
                            } else {
                                bot.send_message(msg.chat.id, "Кажется ты ввёл число не в диапозоне 1-7. Попробуй ещё раз").await?;
                            }
                        }
                        Err(_) => {
                            bot.send_message(msg.chat.id, "Кажется ты написал что-то кроме числа. Попробуй ещё раз").await?;
                        }
                    }
                },
            }
            if should_return_to_menu {
                let poll = manager.lock().await.get_poll(poll_id);
                let (text, markup) = mtg::menus::create_edit_poll_menu(user_id, poll);
                let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
            }
        }
        None => {
            bot.send_message(msg.chat.id, "Кажется, ты отправил мне что-то, кроме простого текстового сообщения. Попробуй ещё раз").await?;
        }
    }

    Ok(())
}

async fn close_menu_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    menu_message_id: MessageId,
    msg: Message,
    cmd: PrivateCommand
) -> HandlerResult {
    match bot.delete_message(msg.chat.id, menu_message_id).await {
        Ok(_) => {
            // Good, nothing to do
        },
        Err(_) => {
            bot.edit_message_text(msg.chat.id, menu_message_id, "[меню закрыто]").await?;
        }
    }
    dialogue.update(PrivateDialogueState::CommandMode).await?;
    private_command_handler(manager, bot, dialogue, msg, cmd).await
}

async fn private_command_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    msg: Message,
    cmd: PrivateCommand
) -> HandlerResult {
    if !verify_chat_membership_message(&manager, &bot, &msg).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    match cmd {
        PrivateCommand::Help => {
            // Just send the description of all commands.
            bot.send_message(msg.chat.id, PrivateCommand::descriptions().to_string()).await?;
        }
        PrivateCommand::Start{arg} => {
            let mut manager_guard = manager.lock().await;
            match manager_guard.get_player_id(msg.from().expect("Error getting user id from message!").id) {
                Ok(player_id) => {
                    let mut game_callback_ok = false;
                    if arg.len() > 0 {
                        match mtg::menus::UniversalCallback::from_base64(&arg) {
                            Ok(callback) => {
                                match mtg::menus::GameCallback::from_universal(callback) {
                                    Ok(game_callback) => {
                                        if manager_guard.game_exists(game_callback.game_id) {
                                            let chat_id = manager_guard.get_chat_id()?;
                                            let mut game = manager_guard.get_game(game_callback.game_id)?;
                                            let player = manager_guard.get_player(player_id, false, false)?;
                                            fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                                            let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                                            let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                                            dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
                                            game_callback_ok = true;
                                        } else {
                                            // Should not really happen, unless someone tries to imitate callback base64 encoding
                                            log::warn!("Received non-existent game id {} from user {}", game_callback.game_id, msg.from().expect("Error getting user id from message!").id);
                                        }
                                    },
                                    Err(_) => {
                                        // Should not really happen, unless someone tries to imitate callback base64 encoding
                                        log::warn!("Failed to parse game callback from user {}", msg.from().expect("Error getting user id from message!").id);
                                    }
                                }
                            }
                            Err(_) => {
                                // Maybe the user just typed something. Ignore it
                            }
                        }
                    }

                    if !game_callback_ok {
                        let allow_polls = manager_guard.is_admin(player_id);
                        let (text, markup) = mtg::menus::create_main_menu(player_id, allow_polls);
                        let menu_message = bot.send_message(msg.chat.id, text).reply_markup(markup).await?;
                        dialogue.update(PrivateDialogueState::MenuMode {menu_message_id: menu_message.id}).await?;
                    }
                }
                Err(_) => {
                    bot.send_message(msg.chat.id, "Привет, похоже, ты ещё не зарегистрировался в боте. Давай начнём! Как мне тебя называть?").await?;
                    dialogue.update(PrivateDialogueState::AskForName).await?;
                }
            }
        }
    }
    Ok(())
}

async fn chat_command_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    msg: Message,
    cmd: ChatCommand,
) -> HandlerResult {
    let admin_id = manager.lock().await.get_admin_id()?;
    let user_id = match msg.from() {
        Some(user) => { user.id }
        _ => { return Ok(()); }
    };
    if user_id != admin_id {
        return Ok(());
    }

    match cmd {
        ChatCommand::Help => {
            bot.send_message(msg.chat.id, ChatCommand::descriptions().to_string()).await?;
        }
        ChatCommand::Init => {
            manager.lock().await.set_chat_id(msg.chat.id);
            bot.send_message(msg.chat.id, "Зарегистрировал этот чат!").await?;
        }
        ChatCommand::DefaultPlace {default_place} => {
            manager.lock().await.set_default_place(default_place);
            bot.send_message(msg.chat.id, "Обновил стандартное место для игр").await?;
        }
        ChatCommand::Delete => {
            match msg.reply_to_message() {
                Some(message) => {
                    match bot.delete_message(msg.chat.id, message.id).await {
                        Ok(_) => {
                            // Good, nothing to do
                        },
                        Err(_) => {
                            bot.edit_message_text(msg.chat.id, message.id, "[удалено]").await?;
                        }
                    }
                }
                None => {
                    // Nothing to do!
                }
            }
        }
        ChatCommand::CurrentResults => {
            match msg.reply_to_message() {
                Some(message) => {
                    let mut manager_lock = manager.lock().await;
                    match manager_lock.get_poll_id(message.id) {
                        Some(poll_id) => {
                            let (_, winning_days, _, _) = manager_lock.process_poll_results(poll_id, true);
                            if !winning_days.is_empty() {
                                bot.send_message(msg.chat.id, format!("Пока что побеждают дни: {}", winning_days)).reply_to_message_id(msg.id).await?;
                            } else {
                                bot.send_message(msg.chat.id, "Пока что игроков недостаточно ни в один из дней").reply_to_message_id(msg.id).await?;
                            }
                        }
                        None => {
                            bot.send_message(msg.chat.id, "Чтобы получить промежуточные результаты опроса, отправьте эту команду в ответ на сообщение с опросом").reply_to_message_id(msg.id).await?;
                        }
                    }
                }
                None => {
                    bot.send_message(msg.chat.id, "Чтобы получить промежуточные результаты опроса, отправьте эту команду в ответ на сообщение с опросом").reply_to_message_id(msg.id).await?;
                }
            }
        }
    }
    Ok(())
}

async fn private_callback_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    dialogue: PrivateDialogue,
    q: CallbackQuery,
    me: Me,
) -> HandlerResult {
    if !verify_chat_membership_callback(&manager, &bot, &q).await? {
        dialogue.exit().await?;
        return Ok(());
    }

    if let Some(data) = q.data {
        let callback = mtg::menus::UniversalCallback::from_base64(&data).expect("Failed to parse callback data!");
        match callback.get_menu().expect("Invalid callback data!") {
            mtg::menus::Menu::Main => {
                let main_menu_callback = mtg::menus::MainCallback::from_universal(callback)?;
                match main_menu_callback.button {
                    mtg::menus::MainButton::OpenProfile => {
                        let player = manager.lock().await.get_player(main_menu_callback.user_id, true, true)?;
                        let (text, markup) = mtg::menus::create_profile_menu(player);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::MainButton::ManageGames => {
                        let (text, markup) = mtg::menus::create_game_management_menu(main_menu_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::MainButton::ManagePolls => {
                        let (text, markup) = mtg::menus::create_poll_management_menu(main_menu_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::MainButton::Links => {
                        let link_infos = manager.lock().await.get_links();
                        let (text, markup) = mtg::menus::create_links_menu(main_menu_callback.user_id, link_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                    mtg::menus::MainButton::ExitMenu => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Пока👋 Если ещё понадоблюсь, пиши /start").await?;
                        }
                        dialogue.exit().await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::Profile => {
                let profile_menu_callback = mtg::menus::ProfileCallback::from_universal(callback)?;
                match profile_menu_callback.button {
                    mtg::menus::ProfileButton::EditDisplayName => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Введи новое имя:").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForNewName {user_id: profile_menu_callback.user_id}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ProfileButton::EditPreferences => {
                        let (text, markup) = mtg::menus::create_edit_preferences_menu(profile_menu_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ProfileButton::EditSubscriptions => {
                        let player = manager.lock().await.get_player(profile_menu_callback.user_id, false, true)?;
                        let (text, markup) = mtg::menus::create_manage_subscriptions_menu(player);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                    mtg::menus::ProfileButton::ViewAchievements => {
                        let achievement_infos = manager.lock().await.get_player_achievement_infos(profile_menu_callback.user_id, 0);
                        let (text, markup) = mtg::menus::create_achievements_menu(profile_menu_callback.user_id, 0, achievement_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ProfileButton::ViewAllAchievements => {
                        let achievement_infos = manager.lock().await.get_achievement_infos(0);
                        let (text, markup) = mtg::menus::create_all_achievements_menu(profile_menu_callback.user_id, 0, achievement_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                    mtg::menus::ProfileButton::DeleteProfile => {
                        let context = profile_menu_callback.user_id as u64;
                        let msg = "Вы собираетесь удалить профиль. При этом будет удалён Ваш профиль, а также будут удалены все заявки на достижения, уже полученные достижения и созданные игры. Регистрации на все игры также будут удалены.".to_string();
                        let (text, markup) = mtg::menus::create_confirmation_menu(context, mtg::menus::ConfirmationAction::ProfileDeletion, msg);
                        if let Some(Message { id, chat, ..}) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ProfileButton::BackToMainMenu => {
                        let allow_polls = manager.lock().await.is_admin(profile_menu_callback.user_id);
                        let (text, markup) = mtg::menus::create_main_menu(profile_menu_callback.user_id, allow_polls);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                }
            },
            mtg::menus::Menu::ManageSubscriptions => {
                let manage_subscription_callback = mtg::menus::ManageSubscriptionsCallback::from_universal(callback)?;
                let mut manager_guard = manager.lock().await;
                let mut player = manager_guard.get_player(manage_subscription_callback.user_id, false, true)?;
                let mut is_quiting = false;
                match manage_subscription_callback.button {
                    mtg::menus::ManageSubscriptionsButton::ToggleNewStdSubscription => {
                        player.subscription.new_std = !player.subscription.new_std;
                    },
                    mtg::menus::ManageSubscriptionsButton::ToggleNewEdhSubscription => {
                        player.subscription.new_edh = !player.subscription.new_edh;
                    },
                    mtg::menus::ManageSubscriptionsButton::ToggleNewStdDraftSubscription => {
                        player.subscription.new_std_draft = !player.subscription.new_std_draft;
                    },
                    mtg::menus::ManageSubscriptionsButton::ToggleNewEdhDraftSubscription => {
                        player.subscription.new_edh_draft = !player.subscription.new_edh_draft;
                    },
                    mtg::menus::ManageSubscriptionsButton::BackToProfile => {
                        is_quiting = true;
                    },
                }
                if !is_quiting {
                    manager_guard.update_user_subscriptions(player.id, &player.subscription);
                    let (text, markup) = mtg::menus::create_manage_subscriptions_menu(player);
                    if let Some(Message { id, chat, .. }) = q.message {
                        bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                    }
                } else {
                    let (text, markup) = mtg::menus::create_profile_menu(player);
                    if let Some(Message { id, chat, .. }) = q.message {
                        bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                    }
                }
                bot.answer_callback_query(q.id).await?;
            }
            mtg::menus::Menu::EditPreferences => {
                let edit_preferences_callback = mtg::menus::EditPreferencesCallback::from_universal(callback)?;
                match edit_preferences_callback.button {
                    mtg::menus::EditPreferencesButton::EditMaxGamesInWeek => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Введи новое максимальное число игр в неделю:").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForNewMaxGames {user_id: edit_preferences_callback.user_id}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPreferencesButton::ResetBadge => {
                        manager.lock().await.set_player_badge(edit_preferences_callback.user_id, "".to_string());
                        bot.answer_callback_query(q.id).text("Бэдж сброшен!").show_alert(true).await?;
                    },
                    mtg::menus::EditPreferencesButton::BackToProfile => {
                        let player = manager.lock().await.get_player(edit_preferences_callback.user_id, true, true)?;
                        let (text, markup) = mtg::menus::create_profile_menu(player);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::Achievements => {
                let achievements_callback = mtg::menus::AchievementsCallback::from_universal(callback)?;
                match achievements_callback.button {
                    mtg::menus::AchievementsButton::AchievementInfo => {
                        let achievement = manager.lock().await.get_achievement(achievements_callback.achievement_id.unwrap(), achievements_callback.user_id);
                        let (text, markup) = mtg::menus::create_achievement_info_menu(achievements_callback.user_id, achievement, false);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::AchievementsButton::PreviousPage => {
                        let current_page = achievements_callback.page.unwrap();
                        if current_page == 0 {
                            bot.answer_callback_query(q.id).text("Эта страница - первая!").show_alert(true).await?;
                        } else {
                            let achievement_infos = manager.lock().await.get_player_achievement_infos(achievements_callback.user_id, current_page - 1);
                            if !achievement_infos.is_empty() {
                                let (text, markup) = mtg::menus::create_achievements_menu(achievements_callback.user_id, current_page - 1, achievement_infos);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                                bot.answer_callback_query(q.id).await?;
                            } else {
                                bot.answer_callback_query(q.id).text("Внутренняя ошибка!").show_alert(true).await?;
                            }
                        }
                    },
                    mtg::menus::AchievementsButton::NextPage => {
                        let current_page = achievements_callback.page.unwrap();
                        let achievement_infos = manager.lock().await.get_player_achievement_infos(achievements_callback.user_id, current_page + 1);
                        if !achievement_infos.is_empty() {
                            let (text, markup) = mtg::menus::create_achievements_menu(achievements_callback.user_id, current_page + 1, achievement_infos);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Эта страница - последняя!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::AchievementsButton::BackToProfile => {
                        let player = manager.lock().await.get_player(achievements_callback.user_id, true, true)?;
                        let (text, markup) = mtg::menus::create_profile_menu(player);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::AllAchievements => {
                let all_achievements_callback = mtg::menus::AllAchievementsCallback::from_universal(callback)?;
                match all_achievements_callback.button {
                    mtg::menus::AllAchievementsButton::AchievementInfo => {
                        let achievement = manager.lock().await.get_achievement(all_achievements_callback.achievement_id.unwrap(), all_achievements_callback.user_id);
                        let (text, markup) = mtg::menus::create_achievement_info_menu(all_achievements_callback.user_id, achievement, true);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::AllAchievementsButton::PreviousPage => {
                        let current_page = all_achievements_callback.page.unwrap();
                        if current_page == 0 {
                            bot.answer_callback_query(q.id).text("Эта страница - первая!").show_alert(true).await?;
                        } else {
                            let achievement_infos = manager.lock().await.get_achievement_infos(current_page - 1);
                            if !achievement_infos.is_empty() {
                                let (text, markup) = mtg::menus::create_all_achievements_menu(all_achievements_callback.user_id, current_page - 1, achievement_infos);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                                bot.answer_callback_query(q.id).await?;
                            } else {
                                bot.answer_callback_query(q.id).text("Внутренняя ошибка!").show_alert(true).await?;
                            }
                        }
                    },
                    mtg::menus::AllAchievementsButton::NextPage => {
                        let current_page = all_achievements_callback.page.unwrap();
                        let achievement_infos = manager.lock().await.get_achievement_infos(current_page + 1);
                        if !achievement_infos.is_empty() {
                            let (text, markup) = mtg::menus::create_all_achievements_menu(all_achievements_callback.user_id, current_page + 1, achievement_infos);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Эта страница - последняя!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::AllAchievementsButton::BackToProfile => {
                        let player = manager.lock().await.get_player(all_achievements_callback.user_id, true, true)?;
                        let (text, markup) = mtg::menus::create_profile_menu(player);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::AchievementInfo => {
                let achievement_info_callback = mtg::menus::AchievementInfoCallback::from_universal(callback)?;
                match achievement_info_callback.button {
                    mtg::menus::AchievementInfoButton::Claim => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.does_achievement_claim_exist(achievement_info_callback.user_id, achievement_info_callback.achievement_id) {
                            bot.answer_callback_query(q.id).text("Запрос данного достижения уже был отправлен в чат! Дождитесь результатов").show_alert(true).await?;
                        } else {
                            let chat_id = manager_guard.get_chat_id()?;
                            let achievement = manager_guard.get_achievement(achievement_info_callback.achievement_id, achievement_info_callback.user_id);
                            let player = manager_guard.get_player(achievement_info_callback.user_id, false, false)?;
                            let message = bot.send_message(chat_id, "[запрос достижения]").await?;
                            let claim_id = manager_guard.add_achievement_claim(achievement_info_callback.user_id, achievement_info_callback.achievement_id, message.id);
                            let (text, markup) = mtg::menus::create_achievement_claim_menu(achievement_info_callback.user_id, claim_id, format!("{} ({})", player.display_name, user_mention_or_link(&q.from)), achievement);
                            bot.edit_message_text(chat_id, message.id, text).reply_markup(markup).await?;
                            bot.answer_callback_query(q.id).text("Отправил запрос в чат!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::AchievementInfoButton::UseBadge => {
                        let badge = manager.lock().await.get_achievement_badge(achievement_info_callback.achievement_id);
                        manager.lock().await.set_player_badge(achievement_info_callback.user_id, badge);
                        bot.answer_callback_query(q.id).text("Бэдж обновлён!").show_alert(true).await?;
                    },
                    mtg::menus::AchievementInfoButton::BackToAchievements => {
                        let achievement_infos = manager.lock().await.get_player_achievement_infos(achievement_info_callback.user_id, 0);
                        let (text, markup) = mtg::menus::create_achievements_menu(achievement_info_callback.user_id, 0, achievement_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::AchievementInfoButton::BackToAllAchievements => {
                        let achievement_infos = manager.lock().await.get_achievement_infos(0);
                        let (text, markup) = mtg::menus::create_all_achievements_menu(achievement_info_callback.user_id, 0, achievement_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            }
            mtg::menus::Menu::Confirmation => {
                let confirmation_callback = mtg::menus::ConfirmationCallback::from_universal(callback)?;
                match confirmation_callback.button {
                    mtg::menus::ConfirmationButton::Confirm => {
                        match confirmation_callback.action {
                            mtg::menus::ConfirmationAction::ProfileDeletion => {
                                let user_id = confirmation_callback.context as i64;
                                let player_deleted = manager.lock().await.delete_player(user_id);
                                if player_deleted {
                                    if let Some(Message { id, chat, .. }) = q.message {
                                        bot.edit_message_text(chat.id, id, "Введи /start чтобы зарегистрироваться снова").await?;
                                    }
                                    bot.answer_callback_query(q.id).text("Профиль успешно удалён!").show_alert(true).await?;
                                    dialogue.exit().await?;
                                } else {
                                    bot.answer_callback_query(q.id).text("Ошибка удаления профиля!").show_alert(true).await?;
                                    log::error!("Error deleting profile! player id: {}", user_id);
                                }
                            },
                            mtg::menus::ConfirmationAction::GameDeletion => {
                                let game_id = confirmation_callback.context as i64;
                                let (game_deleted, msg_id) = manager.lock().await.delete_game(game_id);
                                if game_deleted {
                                    let chat_id = manager.lock().await.get_chat_id()?;
                                    match bot.delete_message(chat_id, msg_id).await {
                                        Ok(_) => { /* Nothing to do*/ }
                                        Err(_) => {
                                            bot.edit_message_text(chat_id, msg_id, "[игра удалена]").await?;
                                        }
                                    }
                                    match bot.unpin_chat_message(chat_id).message_id(msg_id).await {
                                        Ok(_) => { /* Nothing to do */ }
                                        Err(err) => {
                                            log::warn!("Failed unpinning message with id {} for deleted game {} with error: {}", msg_id, game_id, err);
                                        }
                                    }
                                    bot.answer_callback_query(q.id).text("Игра удалена!").show_alert(true).await?;
                                } else {
                                    bot.answer_callback_query(q.id).text("Ошибка при удалении игры!").show_alert(true).await?;
                                    log::error!("Error deleting game! game id: {}", game_id);
                                }
                                let telegram_id = q.from.id;
                                let user_id = manager.lock().await.get_player_id(telegram_id)?;
                                let game_infos = manager.lock().await.get_games_infos(user_id);
                                let (text, markup) = mtg::menus::create_games_list_menu(user_id, game_infos);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                            },
                            mtg::menus::ConfirmationAction::DuplicatePollCreation => {
                                // Cannot happen
                            },
                        }
                    },
                    mtg::menus::ConfirmationButton::Cancel => {
                        bot.answer_callback_query(q.id).text("Действие отменено!").show_alert(true).await?;
                        match confirmation_callback.action {
                            mtg::menus::ConfirmationAction::ProfileDeletion => {
                                let user_id = confirmation_callback.context as i64;
                                let allow_polls = manager.lock().await.is_admin(user_id);
                                let (text, markup) = mtg::menus::create_main_menu(user_id, allow_polls);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                            },
                            mtg::menus::ConfirmationAction::GameDeletion => {
                                let telegram_id = q.from.id;
                                let user_id = manager.lock().await.get_player_id(telegram_id)?;
                                let games_infos = manager.lock().await.get_games_infos(user_id);
                                let (text, markup) = mtg::menus::create_games_list_menu(user_id, games_infos);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                            },
                            mtg::menus::ConfirmationAction::DuplicatePollCreation => {
                                // Cannot happen
                            },
                        }
                    },
                }
            }
            mtg::menus::Menu::GameManagement => {
                let game_management_callback = mtg::menus::GameManagementCallback::from_universal(callback)?;
                match game_management_callback.button {
                    mtg::menus::GameManagementButton::AddGame => {
                        let (text, markup) = mtg::menus::create_game_format_menu(game_management_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::GameManagementButton::MyGames => {
                        let game_infos = manager.lock().await.get_games_infos(game_management_callback.user_id);
                        if game_infos.is_empty() {
                            bot.answer_callback_query(q.id).text("У тебя нет созданных игр!").show_alert(true).await?;
                        } else {
                            let (text, markup) = mtg::menus::create_games_list_menu(game_management_callback.user_id, game_infos);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        }
                    },
                    mtg::menus::GameManagementButton::AllGames => {
                        let game_infos = manager.lock().await.get_games_infos(0);
                        if game_infos.is_empty() {
                            bot.answer_callback_query(q.id).text("Сейчас нет созданных игр!").show_alert(true).await?;
                        } else {
                            let (text, markup) = mtg::menus::create_games_list_menu(game_management_callback.user_id, game_infos);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        }
                    },
                    mtg::menus::GameManagementButton::BackToMainMenu => {
                        let allow_polls = manager.lock().await.is_admin(game_management_callback.user_id);
                        let (text, markup) = mtg::menus::create_main_menu(game_management_callback.user_id, allow_polls);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            }
            mtg::menus::Menu::GameType => {
                let game_format_callback = mtg::menus::GameFormatCallback::from_universal(callback)?;
                let game_format = match game_format_callback.button {
                    mtg::menus::GameFormatButton::Commander => { mtg::types::GameFormat::Commander }
                    mtg::menus::GameFormatButton::Standard => { mtg::types::GameFormat::Standard }
                    mtg::menus::GameFormatButton::CommanderDraft => { mtg::types::GameFormat::CommanderDraft }
                    mtg::menus::GameFormatButton::StandardDraft => { mtg::types::GameFormat::StandardDraft }
                    mtg::menus::GameFormatButton::Cancel => {
                        let allow_polls = manager.lock().await.is_admin(game_format_callback.user_id);
                        let (text, markup) = mtg::menus::create_main_menu(game_format_callback.user_id, allow_polls);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        return Ok(());
                    }
                };
                if let Some(Message { id, chat, .. }) = q.message {
                    bot.edit_message_text(chat.id, id, "Напиши дату и время игры в формате \"DD.MM.YYYY HH:MM\"").await?;
                }
                dialogue.update(PrivateDialogueState::AskForDate {game_format}).await?;
                bot.answer_callback_query(q.id).await?;
            }
            mtg::menus::Menu::GamesList => {
                let games_list_callback = mtg::menus::GamesListCallback::from_universal(callback)?;
                match games_list_callback.button {
                    mtg::menus::GamesListButton::BackToGameManagement => {
                        let (text, markup) = mtg::menus::create_game_management_menu(games_list_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::GamesListButton::SelectGame => {
                        match games_list_callback.game_id {
                            Some(game_id) => {
                                let mut manager_guard = manager.lock().await;
                                let chat_id = manager_guard.get_chat_id()?;
                                let mut game = manager_guard.get_game(game_id)?;
                                let player = manager_guard.get_player(games_list_callback.user_id, false, false)?;
                                fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                                let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                                if let Some(Message { id, chat, .. }) = q.message {
                                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                                }
                                bot.answer_callback_query(q.id).await?;
                            },
                            None => {
                                bot.answer_callback_query(q.id).text("Внутренняя ошибка!").show_alert(true).await?;
                                log::error!("Invalid GamesListCallback from user {}!", games_list_callback.user_id);
                            }
                        }
                    }
                }
            },
            mtg::menus::Menu::ManageGame => {
                let manage_game_callback = mtg::menus::ManageGameCallback::from_universal(callback)?;
                match manage_game_callback.button {
                    mtg::menus::ManageGameButton::SignUp => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.join_game(manage_game_callback.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, manage_game_callback.game_id).await?;
                            let chat_id = manager_guard.get_chat_id()?;
                            let mut game = manager_guard.get_game(manage_game_callback.game_id)?;
                            let player = manager_guard.get_player(manage_game_callback.user_id, false, false)?;
                            fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                            let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты уже записан!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::ManageGameButton::ToggleBuyout => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.toggle_buyout(manage_game_callback.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, manage_game_callback.game_id).await?;
                            let chat_id = manager_guard.get_chat_id()?;
                            let mut game = manager_guard.get_game(manage_game_callback.game_id)?;
                            let player = manager_guard.get_player(manage_game_callback.user_id, false, false)?;
                            fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                            let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты не записан на эту игру!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::ManageGameButton::Quit => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.quit_game(manage_game_callback.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, manage_game_callback.game_id).await?;
                            let chat_id = manager_guard.get_chat_id()?;
                            let mut game = manager_guard.get_game(manage_game_callback.game_id)?;
                            let player = manager_guard.get_player(manage_game_callback.user_id, false, false)?;
                            fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                            let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                            if let Some(Message { id, chat, .. }) = q.message {
                                bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                            }
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты и так не записан!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::ManageGameButton::GenerateSittingsGroups => {
                        let chat_id = manager.lock().await.get_chat_id()?;
                        let mut game_registrations = manager.lock().await.get_game_registrations(manage_game_callback.game_id)?;
                        fill_missing_player_info(&bot, chat_id, &mut game_registrations).await?;

                        match mtg::create_sittings_groups(game_registrations, manage_game_callback.game_id) {
                            Ok(sittings) => {
                                bot.send_message(chat_id, sittings).await?;
                                bot.answer_callback_query(q.id).text("Рассадка сгенерирована и отправлена в чат!").show_alert(true).await?;
                            },
                            Err(error_msg) => {
                                bot.answer_callback_query(q.id).text(error_msg).show_alert(true).await?;
                            }
                        }
                    },
                    mtg::menus::ManageGameButton::GenerateSittingsPairs => {
                        let chat_id = manager.lock().await.get_chat_id()?;
                        let mut game_registrations = manager.lock().await.get_game_registrations(manage_game_callback.game_id)?;
                        fill_missing_player_info(&bot, chat_id, &mut game_registrations).await?;

                        match mtg::create_sittings_pairs(game_registrations, manage_game_callback.game_id) {
                            Ok(sittings) => {
                                bot.send_message(chat_id, sittings).await?;
                                bot.answer_callback_query(q.id).text("Рассадка сгенерирована и отправлена в чат!").show_alert(true).await?;
                            },
                            Err(error_msg) => {
                                bot.answer_callback_query(q.id).text(error_msg).show_alert(true).await?;
                            }
                        }
                    },
                    mtg::menus::ManageGameButton::EditPlace => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое место для игры:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::Place}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::EditDescription => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Введи новое описание для игры:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::Description}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::EditDateTime => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новые дату и время для игры в формате \"DD.MM.YYYY HH:MM\":").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::DateTime}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::EditFee => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новую сумму взноса для игры:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::Fee}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::EditMandatory => {
                        let mut manager_guard = manager.lock().await;
                        let mut game = manager_guard.get_game(manage_game_callback.game_id)?;
                        game.mandatory_fee = !game.mandatory_fee;
                        manager_guard.edit_game_mandatory_fee(manage_game_callback.game_id, game.mandatory_fee);
                        let chat_id = manager_guard.get_chat_id()?;
                        let player = manager_guard.get_player(manage_game_callback.user_id, false, false)?;
                        fill_missing_player_info(&bot, chat_id, &mut game.registrations).await?;
                        let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).text("Обязательность взноса изменена").show_alert(true).await?;
                    },
                    mtg::menus::ManageGameButton::EditRequiredPlayerCount => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое требуемое число игроков:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::RequiredPlayerCount}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::AddPlayer => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши имя добавляемого игрока:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditGame {game_id: manage_game_callback.game_id, user_id: manage_game_callback.user_id, change_field: GameField::AnonPlayer}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::RemovePlayer => {
                        let chat_id = manager.lock().await.get_chat_id()?;
                        let mut game_registrations = manager.lock().await.get_game_registrations(manage_game_callback.game_id)?;
                        fill_missing_player_info(&bot, chat_id, &mut game_registrations).await?;

                        let (text, markup) = mtg::menus::create_remove_player_menu(manage_game_callback.game_id, game_registrations);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::DeleteGame => {
                        let context = manage_game_callback.game_id as u64;
                        let msg = "Вы собираетесь удалить игру. При этом регистрация всех игроков будет отменена, и сообщение об игре удалено из чата. Восстановить игру не получится".to_string();
                        let (text, markup) = mtg::menus::create_confirmation_menu(context, mtg::menus::ConfirmationAction::GameDeletion, msg);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::ManageGameButton::BackToGamesList => {
                        let game_infos = manager.lock().await.get_games_infos(manage_game_callback.user_id);
                        let (text, markup) = mtg::menus::create_games_list_menu(manage_game_callback.user_id, game_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                }
            }
            mtg::menus::Menu::RemovePlayer => {
                let remove_player_callback = mtg::menus::RemovePlayerCallback::from_universal(callback)?;
                let action = match remove_player_callback.button {
                    mtg::menus::RemovePlayerButton::RemovePlayerNormal => {
                        Some(mtg::types::GameRegistration::simple_form(remove_player_callback.reg_id, mtg::types::RegistrationType::Normal))
                    },
                    mtg::menus::RemovePlayerButton::RemovePlayerTelegramId => {
                        Some(mtg::types::GameRegistration::simple_form(remove_player_callback.reg_id, mtg::types::RegistrationType::TelegramId))
                    },
                    mtg::menus::RemovePlayerButton::RemovePlayerNameOnly => {
                        Some(mtg::types::GameRegistration::simple_form(remove_player_callback.reg_id, mtg::types::RegistrationType::NameOnly))
                    },
                    mtg::menus::RemovePlayerButton::BackToManageGame => {
                        None
                    }
                };
                match action {
                    Some(game_registration) => {
                        let player_removed = manager.lock().await.remove_player(remove_player_callback.game_id, game_registration);
                        if player_removed {
                            bot.answer_callback_query(q.id).text("Игрок удалён!").show_alert(true).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка при удалении игрока!").show_alert(true).await?;
                            log::error!("Error deleting player! game_id: {}, reg_id: {}", remove_player_callback.game_id, remove_player_callback.reg_id);
                        }
                        let mut manager_guard = manager.lock().await;
                        update_game_msg(&mut manager_guard, &bot, &me, remove_player_callback.game_id).await?;
                    }
                    None => {
                        bot.answer_callback_query(q.id).await?;
                    }
                }
                let game = manager.lock().await.get_game(remove_player_callback.game_id)?;
                let user_id = manager.lock().await.get_player_id(q.from.id)?;
                let player = manager.lock().await.get_player(user_id, false, false)?;
                let (text, markup) = mtg::menus::create_manage_game_menu(player, game);
                if let Some(Message { id, chat, .. }) = q.message {
                    bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                }
            },
            mtg::menus::Menu::PollManagement => {
                let poll_management_callback = mtg::menus::PollManagementCallback::from_universal(callback)?;
                match poll_management_callback.button {
                    mtg::menus::PollManagementButton::SchedulePoll => {
                        let (text, markup) = mtg::menus::create_poll_format_menu(poll_management_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::PollManagementButton::ManagePolls => {
                        let poll_infos = manager.lock().await.get_poll_infos();
                        let (text, markup) = mtg::menus::create_polls_list_menu(poll_management_callback.user_id, poll_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::PollManagementButton::BackToMainMenu => {
                        let (text, markup) = mtg::menus::create_main_menu(poll_management_callback.user_id, true);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::PollFormat => {
                let poll_format_callback = mtg::menus::PollFormatCallback::from_universal(callback)?;
                match poll_format_callback.button {
                    mtg::menus::PollFormatButton::Cancel => {
                        let (text, markup) = mtg::menus::create_poll_management_menu(poll_format_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::PollFormatButton::Commander => {
                        bot.answer_callback_query(q.id).await?;
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши, какой вопрос должен быть у опроса").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForPollQuestion { game_format: mtg::types::GameFormat::Commander }).await?;
                    },
                    mtg::menus::PollFormatButton::Standard => {
                        bot.answer_callback_query(q.id).await?;
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши, какой вопрос должен быть у опроса").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForPollQuestion { game_format: mtg::types::GameFormat::Standard }).await?;
                    },
                    mtg::menus::PollFormatButton::CommanderDraft => {
                        bot.answer_callback_query(q.id).await?;
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши, какой вопрос должен быть у опроса").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForPollQuestion { game_format: mtg::types::GameFormat::CommanderDraft }).await?;
                    },
                    mtg::menus::PollFormatButton::StandardDraft => {
                        bot.answer_callback_query(q.id).await?;
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши, какой вопрос должен быть у опроса").await?;
                        }
                        dialogue.update(PrivateDialogueState::AskForPollQuestion { game_format: mtg::types::GameFormat::StandardDraft }).await?;
                    },
                }
            },
            mtg::menus::Menu::PollsList => {
                let polls_list_callback = mtg::menus::PollsListCallback::from_universal(callback)?;
                match polls_list_callback.button {
                    mtg::menus::PollsListButton::SelectPoll => {
                        let poll = manager.lock().await.get_poll(polls_list_callback.poll_id);
                        let (text, markup) = mtg::menus::create_edit_poll_menu(polls_list_callback.user_id, poll);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                    mtg::menus::PollsListButton::BackToPollsManagement => {
                        let (text, markup) = mtg::menus::create_poll_management_menu(polls_list_callback.user_id);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                }
            },
            mtg::menus::Menu::EditPoll => {
                let edit_poll_callback = mtg::menus::EditPollCallback::from_universal(callback)?;
                match edit_poll_callback.button {
                    mtg::menus::EditPollButton::EditQuestion => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новый вопрос для опроса:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::Question}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditDescription => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое описание для создаваемых игр:").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::Description}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditGameTime => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое время игры в формате \"HH:MM\":").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::GameTime}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditStartTime => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое время начала опроса в формате \"HH:MM\":").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::StartTime}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditStartDay => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новый день начала опроса (1 - 7):").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::StartDay}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditEndTime => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новое время конца опроса в формате \"HH:MM\":").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::EndTime}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::EditEndDay => {
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, "Напиши новый день конца опроса (1 - 7):").await?;
                        }
                        dialogue.update(PrivateDialogueState::EditPoll {poll_id: edit_poll_callback.poll_id, user_id: edit_poll_callback.user_id, change_field: PollField::EndDay}).await?;
                        bot.answer_callback_query(q.id).await?;
                    },
                    mtg::menus::EditPollButton::DeletePoll => {
                        manager.lock().await.remove_poll(edit_poll_callback.poll_id);
                        let poll_infos = manager.lock().await.get_poll_infos();
                        let (text, markup) = mtg::menus::create_polls_list_menu(edit_poll_callback.user_id, poll_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).text("Опрос удалён!").show_alert(true).await?;
                    },
                    mtg::menus::EditPollButton::BackToPollsList => {
                        let poll_infos = manager.lock().await.get_poll_infos();
                        let (text, markup) = mtg::menus::create_polls_list_menu(edit_poll_callback.user_id, poll_infos);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    },
                }
            },
            mtg::menus::Menu::LinksMenu => {
                let links_callback = mtg::menus::LinksCallback::from_universal(callback)?;
                match links_callback.button {
                    mtg::menus::LinksButton::BackToMainMenu => {
                        let (text, markup) = mtg::menus::create_main_menu(links_callback.user_id, true);
                        if let Some(Message { id, chat, .. }) = q.message {
                            bot.edit_message_text(chat.id, id, text).reply_markup(markup).await?;
                        }
                        bot.answer_callback_query(q.id).await?;
                    }
                }
            },
            _ => {
                bot.answer_callback_query(q.id).text("Функционал пока недоступен!").show_alert(true).await?;
            },
        }
    }

    Ok(())
}

async fn chat_callback_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    bot: Bot,
    q: CallbackQuery,
    me: Me,
) -> HandlerResult {
    if let Some(data) = q.data {
        let callback = mtg::menus::UniversalCallback::from_base64(&data).expect("Failed to parse callback data!");
        match callback.get_menu().expect("Invalid callback data!") {
            mtg::menus::Menu::Game => {
                let game_callback_query = mtg::menus::GameCallback::from_universal(callback)?;
                match game_callback_query.button {
                    mtg::menus::GameButton::SignUp => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.join_game(game_callback_query.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, game_callback_query.game_id).await?;
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты уже записан!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::GameButton::ToggleBuyout => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.toggle_buyout(game_callback_query.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, game_callback_query.game_id).await?;
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты не записан на эту игру!").show_alert(true).await?;
                        }
                    },
                    mtg::menus::GameButton::Manage => {
                        // Manage is handled differently, if we received it - something is wrong
                        bot.answer_callback_query(q.id).text("Внутренняя ошибка!").show_alert(true).await?;
                        log::error!("Received GameButton::Manage from callback! game_id: {}", game_callback_query.game_id);
                    }
                    mtg::menus::GameButton::Quit => {
                        let mut manager_guard = manager.lock().await;
                        if manager_guard.quit_game(game_callback_query.game_id, q.from.id) {
                            update_game_msg(&mut manager_guard, &bot, &me, game_callback_query.game_id).await?;
                            bot.answer_callback_query(q.id).await?;
                        } else {
                            bot.answer_callback_query(q.id).text("Ошибка, ты и так не записан!").show_alert(true).await?;
                        }
                    }
                }
            }
            mtg::menus::Menu::AchievementClaim => {
                let achievement_claim_callback = mtg::menus::AchievementClaimCallback::from_universal(callback)?;
                let vote: u8 = match achievement_claim_callback.button {
                    mtg::menus::AchievementClaimButton::Approve => {
                        1
                    },
                    mtg::menus::AchievementClaimButton::Reject => {
                        0
                    },
                };
                let mut manager_guard = manager.lock().await;
                match manager_guard.add_achievement_claim_vote(q.from.id, achievement_claim_callback.claim_id, vote) {
                    Some((verdict, message_id, achievement_id)) => {
                        let chat_id = manager_guard.get_chat_id()?;
                        bot.edit_message_reply_markup(chat_id, message_id).await?;
                        if verdict {
                            manager_guard.grant_player_achievement(achievement_claim_callback.user_id, achievement_id);
                            bot.send_message(chat_id, "Достижение выдано!").reply_to_message_id(message_id).await?;
                        } else {
                            bot.send_message(chat_id, "В достижении отказано!").reply_to_message_id(message_id).await?;
                        }
                    }
                    None => {
                        // Nothing to do!
                    },
                }
                bot.answer_callback_query(q.id).text("Ваш голос учтён!").show_alert(true).await?;
            }
            _ => {
                bot.answer_callback_query(q.id).text("Внутренняя ошибка!").show_alert(true).await?;
                log::error!("Unexpected callback in game chat!");
            }
        }
    }

    Ok(())
}

async fn chat_poll_handler(
    manager: Arc<Mutex<mtg::Manager>>,
    poll_answer: PollAnswer,
) -> HandlerResult {
    manager.lock().await.add_poll_result(poll_answer);
    Ok(())
}

async fn stale_callback_handler(
    bot: Bot,
    _dialogue: PrivateDialogue,
    q: CallbackQuery
) -> HandlerResult {
    log::warn!("Received callback data in command mode from user {} with username/id {}...", q.from.full_name(), q.from.username.unwrap_or(q.id.clone()));
    bot.answer_callback_query(q.id).show_alert(true).text("Ой! Что-то пошло не так. Открой меню заново командой /start").await?;
    Ok(())
}
