use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};
use chrono::Datelike;
use crate::mtg::types;

pub enum Menu {
    Main,                // 0

    Profile,             // 1
    GameManagement,      // 2
    PollManagement,      // 3

    EditPreferences,     // 4
    Achievements,        // 5
    AllAchievements,     // 6

    Confirmation,        // 7

    AchievementInfo,     // 8

    GameType,            // 9

    GamesList,           // 10
    ManageGame,          // 11
    RemovePlayer,        // 12

    Game,                // 13
    AchievementClaim,    // 14

    LinksMenu,           // 15

    PollFormat,          // 16

    PollsList,           // 17
    EditPoll,            // 18

    ManageSubscriptions, // 19
}

impl Menu {
    pub fn to_u8(&self) -> u8 {
        match self {
            Self::Main                => 0,
            Self::Profile             => 1,
            Self::GameManagement      => 2,
            Self::PollManagement      => 3,
            Self::EditPreferences     => 4,
            Self::Achievements        => 5,
            Self::AllAchievements     => 6,
            Self::Confirmation        => 7,
            Self::AchievementInfo     => 8,
            Self::GameType            => 9,
            Self::GamesList           => 10,
            Self::ManageGame          => 11,
            Self::RemovePlayer        => 12,
            Self::Game                => 13,
            Self::AchievementClaim    => 14,
            Self::LinksMenu           => 15,
            Self::PollFormat            => 16,
            Self::PollsList           => 17,
            Self::EditPoll          => 18,
            Self::ManageSubscriptions => 19,
        }
    }

    pub fn from_u8(val: u8) -> Result<Menu, types::ErrorCode> {
        match val {
            00 => Ok(Self::Main),
            01 => Ok(Self::Profile),
            02 => Ok(Self::GameManagement),
            03 => Ok(Self::PollManagement),
            04 => Ok(Self::EditPreferences),
            05 => Ok(Self::Achievements),
            06 => Ok(Self::AllAchievements),
            07 => Ok(Self::Confirmation),
            08 => Ok(Self::AchievementInfo),
            09 => Ok(Self::GameType),
            10 => Ok(Self::GamesList),
            11 => Ok(Self::ManageGame),
            12 => Ok(Self::RemovePlayer),
            13 => Ok(Self::Game),
            14 => Ok(Self::AchievementClaim),
            15 => Ok(Self::LinksMenu),
            16 => Ok(Self::PollFormat),
            17 => Ok(Self::PollsList),
            18 => Ok(Self::EditPoll),
            19 => Ok(Self::ManageSubscriptions),
             _ => Err(types::ErrorCode::InternalError)
        }
    }
}

pub enum MainButton {
    OpenProfile, // 0
    ManageGames, // 1
    ManagePolls, // 2
    Links,       // 3
    ExitMenu,    // 4
}

pub enum ProfileButton {
    EditDisplayName,     // 0
    EditPreferences,     // 1
    EditSubscriptions,   // 2
    ViewAchievements,    // 3
    ViewAllAchievements, // 4
    DeleteProfile,       // 5
    BackToMainMenu,      // 6
}

pub enum GameManagementButton {
    AddGame,        // 0
    MyGames,        // 1
    AllGames,       // 2
    BackToMainMenu, // 3
}

pub enum GamesListButton {
    BackToGameManagement, // 0
    SelectGame,           // 1
}

pub enum ManageGameButton {
    SignUp,                  // 0
    ToggleBuyout,            // 1
    Quit,                    // 2
    GenerateSittingsGroups,  // 3
    GenerateSittingsPairs,   // 4
    EditPlace,               // 5
    EditDescription,         // 6
    EditDateTime,            // 7
    EditFee,                 // 8
    EditMandatory,           // 9
    EditRequiredPlayerCount, // 10
    AddPlayer,               // 11
    RemovePlayer,            // 12
    DeleteGame,              // 13
    BackToGamesList,         // 14
}

pub enum RemovePlayerButton {
    BackToManageGame,       // 0
    RemovePlayerNormal,     // 1
    RemovePlayerTelegramId, // 2
    RemovePlayerNameOnly,   // 3
}

pub enum PollManagementButton {
    SchedulePoll,   // 0
    ManagePolls,    // 1
    BackToMainMenu, // 2
}

pub enum EditPreferencesButton {
    EditMaxGamesInWeek, // 0
    ResetBadge,         // 1
    BackToProfile,      // 2
}

pub enum GameFormatButton {
    Commander,      // 0
    Standard,       // 1
    CommanderDraft, // 2
    StandardDraft,  // 3
    Cancel,         // 4
}

pub enum AchievementsButton {
    AchievementInfo, // 0
    NextPage,        // 1
    PreviousPage,    // 2
    BackToProfile,   // 3
}

pub enum AllAchievementsButton {
    AchievementInfo, // 0
    NextPage,        // 1
    PreviousPage,    // 2
    BackToProfile,   // 3
}

pub enum AchievementInfoButton {
    Claim,                 // 0
    UseBadge,              // 1
    BackToAchievements,    // 2
    BackToAllAchievements, // 3
}

pub enum ConfirmationButton {
    Confirm, // 0
    Cancel,  // 1
}

#[derive(Clone)]
pub enum ConfirmationAction {
    ProfileDeletion,       // 0
    GameDeletion,          // 1
    DuplicatePollCreation, // 2
}

pub enum GameButton {
    SignUp,       // 0
    ToggleBuyout, // 1
    Quit,         // 2
    Manage,       // 4
}

pub enum AchievementClaimButton {
    Approve,            // 0
    Reject,             // 1
}

pub enum LinksButton {
    BackToMainMenu, // 0
}

pub enum PollFormatButton {
    Cancel,         // 0
    Commander,      // 1
    Standard,       // 2
    CommanderDraft, // 3
    StandardDraft,  // 4
}

pub enum PollsListButton {
    BackToPollsManagement, // 0
    SelectPoll,            // 1
}

pub enum EditPollButton {
    EditQuestion,     // 0
    EditDescription,  // 1
    EditGameTime,     // 2
    EditStartTime,    // 3
    EditStartDay,     // 4
    EditEndTime,      // 5
    EditEndDay,       // 6
    DeletePoll,       // 7
    BackToPollsList,  // 8
}

pub enum ManageSubscriptionsButton {
    ToggleNewStdSubscription,      // 0
    ToggleNewEdhSubscription,      // 1
    ToggleNewStdDraftSubscription, // 2
    ToggleNewEdhDraftSubscription, // 3
    BackToProfile,                 // 4
}

pub fn create_main_menu(user_id: i64, allow_polls: bool) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Открыть профиль", MainCallback{ user_id, button: MainButton::OpenProfile }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Управление играми", MainCallback{ user_id, button: MainButton::ManageGames }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    if allow_polls {
        keyboard.push(vec![InlineKeyboardButton::callback("Управление опросами", MainCallback { user_id, button: MainButton::ManagePolls }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }
    keyboard.push(vec![InlineKeyboardButton::callback("Ссылки", MainCallback{user_id, button: MainButton::Links }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Выйти", MainCallback{ user_id, button: MainButton::ExitMenu }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Главное Меню >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_profile_menu(player: types::Player) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить имя", ProfileCallback{ user_id: player.id, button: ProfileButton::EditDisplayName }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить предпочтения", ProfileCallback{ user_id: player.id, button: ProfileButton::EditPreferences }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить подписки", ProfileCallback{ user_id: player.id, button: ProfileButton::EditSubscriptions }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Полученные достижения", ProfileCallback{ user_id: player.id, button: ProfileButton::ViewAchievements }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Все достижения", ProfileCallback{ user_id: player.id, button: ProfileButton::ViewAllAchievements }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Удалить профиль", ProfileCallback{ user_id: player.id, button: ProfileButton::DeleteProfile }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Выйти в меню", ProfileCallback{ user_id: player.id, button: ProfileButton::BackToMainMenu }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    let mut subs = String::new();
    if player.subscription.new_std {
        subs += "- Новые игры стандарт\n";
    }
    if player.subscription.new_edh {
        subs += "- Новые игры EDH\n";
    }
    if player.subscription.new_std_draft {
        subs += "- Новые драфты\n";
    }
    if player.subscription.new_edh_draft {
        subs += "- Новые драфты EDH";
    }
    if subs.is_empty() {
        subs += "- Нет"
    }

    let text = format!("==< Ваш профиль >==\nИмя: {}\nИдентификатор: {}\nПрава администратора: {}\nПолучено достижений: {}\nПредпочтения:\n- Кол-во игр в неделю: {}\n- Используемый бэдж: {}\nПодписки:\n{}",
        player.display_name,
        player.id,
        if player.is_admin { "да" } else { "нет" },
        player.achievements.len(),
        player.preferences.max_games_in_week,
        if player.preferences.used_badge.is_empty() { "нет" } else { &player.preferences.used_badge },
        subs,
    );

    (text, InlineKeyboardMarkup::new(keyboard))
}

pub fn create_edit_preferences_menu(user_id: i64) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];

    keyboard.push(vec![InlineKeyboardButton::callback("Изменить максимум игр в неделю", EditPreferencesCallback{user_id, button: EditPreferencesButton::EditMaxGamesInWeek}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Сбросить бэдж", EditPreferencesCallback{user_id, button: EditPreferencesButton::ResetBadge}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в профиль", EditPreferencesCallback{user_id, button: EditPreferencesButton::BackToProfile}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    ("==< Изменить предпочтения >==\nПримечение: Чтобы установить бэдж - перейдите в меню полученных достижений и выберите достижение, бэдж которого хотите поставить".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_manage_subscriptions_menu(player: types::Player) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];

    keyboard.push(vec![InlineKeyboardButton::callback(format!("Новые игры стандарт: {}", if player.subscription.new_std { "✅" } else { "❌" }), ManageSubscriptionsCallback{user_id: player.id, button: ManageSubscriptionsButton::ToggleNewStdSubscription}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback(format!("Новые игры EDH: {}", if player.subscription.new_edh { "✅" } else { "❌" }), ManageSubscriptionsCallback{user_id: player.id, button: ManageSubscriptionsButton::ToggleNewEdhSubscription}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback(format!("Новые драфты: {}", if player.subscription.new_std_draft { "✅" } else { "❌" }), ManageSubscriptionsCallback{user_id: player.id, button: ManageSubscriptionsButton::ToggleNewStdDraftSubscription}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback(format!("Новые драфты EDH: {}", if player.subscription.new_edh_draft { "✅" } else { "❌" }), ManageSubscriptionsCallback{user_id: player.id, button: ManageSubscriptionsButton::ToggleNewEdhDraftSubscription}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в профиль", ManageSubscriptionsCallback{user_id: player.id, button: ManageSubscriptionsButton::BackToProfile}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    ("==< Управление подписками >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_achievements_menu(user_id: i64, page: u64, achievement_infos: Vec<(i64, String)>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    for (id, text) in achievement_infos {
        keyboard.push(vec![InlineKeyboardButton::callback(text, AchievementsCallback{ user_id, button: AchievementsButton::AchievementInfo, page: None, achievement_id: Some(id)}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }
    keyboard.push(vec![
        InlineKeyboardButton::callback("<--", AchievementsCallback{ user_id, button: AchievementsButton::PreviousPage, page: Some(page), achievement_id: None}.to_universal().to_base64().expect("Failed encoding callback to base64!")),
        InlineKeyboardButton::callback("-->", AchievementsCallback{ user_id, button: AchievementsButton::NextPage, page: Some(page), achievement_id: None}.to_universal().to_base64().expect("Failed encoding callback to base64!"))
    ]);
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в профиль", AchievementsCallback{ user_id, button: AchievementsButton::BackToProfile, page: None, achievement_id: None}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Полученные достижения >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_all_achievements_menu(user_id: i64, page: u64, achievement_infos: Vec<(i64, String)>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    for (id, text) in achievement_infos {
        keyboard.push(vec![InlineKeyboardButton::callback(text, AllAchievementsCallback{ user_id, button: AllAchievementsButton::AchievementInfo, page: None, achievement_id: Some(id)}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }
    keyboard.push(vec![
        InlineKeyboardButton::callback("<--", AllAchievementsCallback{ user_id, button: AllAchievementsButton::PreviousPage, page: Some(page), achievement_id: None}.to_universal().to_base64().expect("Failed encoding callback to base64!")),
        InlineKeyboardButton::callback("-->", AllAchievementsCallback{ user_id, button: AllAchievementsButton::NextPage, page: Some(page), achievement_id: None }.to_universal().to_base64().expect("Failed encoding callback to base64!"))
    ]);
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в профиль", AllAchievementsCallback{ user_id, button: AllAchievementsButton::BackToProfile, page: None, achievement_id: None}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Доступные достижения >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_achievement_info_menu(user_id: i64, achievement: types::Achievement, from_all: bool) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    if from_all {
        if achievement.achievable {
            keyboard.push(vec![InlineKeyboardButton::callback("Получить", AchievementInfoCallback { user_id, achievement_id: achievement.id, button: AchievementInfoButton::Claim }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        }
        keyboard.push(vec![InlineKeyboardButton::callback("Назад", AchievementInfoCallback{ user_id, achievement_id: achievement.id, button: AchievementInfoButton::BackToAllAchievements }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    } else {
        keyboard.push(vec![InlineKeyboardButton::callback("Использовать бэдж", AchievementInfoCallback{ user_id, achievement_id: achievement.id, button: AchievementInfoButton::UseBadge }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        keyboard.push(vec![InlineKeyboardButton::callback("Назад", AchievementInfoCallback{ user_id, achievement_id: achievement.id, button: AchievementInfoButton::BackToAchievements }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    let text = format!("==< Информация о достижении >==\nИдентификатор: {}\nНазвание: {}\nОписание: {}\nБэдж: {}\nВозможно получить: {}",
        achievement.id,
        achievement.name,
        achievement.description,
        achievement.badge,
        if achievement.achievable { "да" } else { "нет" },
    );

    (text, InlineKeyboardMarkup::new(keyboard))
}

pub fn create_confirmation_menu(context: u64, action: ConfirmationAction, msg: String) -> (String, InlineKeyboardMarkup)
{
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Отменить", ConfirmationCallback{ context, button: ConfirmationButton::Cancel, action: action.clone()}.to_universal().to_base64().expect("Failed encoding callback to base64"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Подтвердить", ConfirmationCallback{ context, button: ConfirmationButton::Confirm, action}.to_universal().to_base64().expect("Failed encoding callback to base64"))]);
    (format!("==< !Подветрждение! >==\n{}", msg), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_game_management_menu(user_id: i64) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Создать игру", GameManagementCallback{ user_id, button: GameManagementButton::AddGame }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Мои игры", GameManagementCallback{ user_id, button: GameManagementButton::MyGames }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Все игры", GameManagementCallback{ user_id, button: GameManagementButton::AllGames }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Выйти в меню", GameManagementCallback{ user_id, button: GameManagementButton::BackToMainMenu }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Управление Играми >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_game_format_menu(user_id: i64) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Commander", GameFormatCallback { user_id, button: GameFormatButton::Commander }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Standard", GameFormatCallback { user_id, button: GameFormatButton::Standard }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Commander Draft", GameFormatCallback { user_id, button: GameFormatButton::CommanderDraft }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Standard Draft", GameFormatCallback { user_id, button: GameFormatButton::StandardDraft }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Отменить", GameFormatCallback { user_id, button: GameFormatButton::Cancel }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Создание Игры >==\nВыберите формат игры:".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_poll_format_menu(user_id: i64) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Commander", PollFormatCallback { user_id, button: PollFormatButton::Commander }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Standard", PollFormatCallback { user_id, button: PollFormatButton::Standard }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Commander Draft", PollFormatCallback { user_id, button: PollFormatButton::CommanderDraft }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Standard Draft", PollFormatCallback { user_id, button: PollFormatButton::StandardDraft }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Отменить", PollFormatCallback { user_id, button: PollFormatButton::Cancel }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Создание Опроса >==\nВыберите формат игры:".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_polls_list_menu(user_id: i64, polls_infos: Vec<(i64, String)>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    for poll_info in polls_infos {
        keyboard.push(vec![InlineKeyboardButton::callback(poll_info.1, PollsListCallback { user_id, poll_id: poll_info.0, button: PollsListButton::SelectPoll }.to_universal().to_base64().expect("Failed encoding callback to base64!"))])
    }
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в управление опросами", PollsListCallback { user_id, poll_id: 0, button: PollsListButton::BackToPollsManagement }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Список Опросов >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_edit_poll_menu(user_id: i64, poll: types::Poll) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];

    keyboard.push(vec![InlineKeyboardButton::callback("Изменить вопрос", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditQuestion}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить описание игры", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditDescription}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить время игры", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditGameTime}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить время начала опроса", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditStartTime}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить день начала опроса", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditStartDay}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить время конца опроса", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditEndTime}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Изменить день конца опроса", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::EditEndDay}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Удалить опрос", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::DeletePoll}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в список опросов", EditPollCallback {user_id, poll_id: poll.id, button: EditPollButton::BackToPollsList}.to_universal().to_base64().expect("Failed encoding callback to base64") )]);

    let day_to_str = |day: u8| {
        match day {
            1 => "Пн",
            2 => "Вт",
            3 => "Ср",
            4 => "Чт",
            5 => "Пт",
            6 => "Сб",
            7 => "Вс",
            _ => "Err"
        }
    };

    let text = format!("==< Редактирование опроса № {} >==\nСоздатель опроса: {}\nВопрос: {}\nОписание для игр: {}\nМесто игр: {}\nФормат игр: {}\nВремя игр: {}\nВремя начала опроса: {}\nДень начала опроса: {}\nВремя конца опроса: {}\nДень конца опроса: {}",
        poll.id,
        poll.creator_id,
        poll.question,
        poll.description,
        poll.place,
        match poll.game_format {
            types::GameFormat::Commander => "EDH",
            types::GameFormat::Standard => "стандарт",
            types::GameFormat::CommanderDraft => "EDH Драфт",
            types::GameFormat::StandardDraft => "Драфт"
        },
        poll.game_time.format("%H:%M"),
        poll.start_time.format("%H:%M"),
        day_to_str(poll.start_day),
        poll.end_time.format("%H:%M"),
        day_to_str(poll.end_day),
    );

    (text, InlineKeyboardMarkup::new(keyboard))
}

pub fn create_games_list_menu(user_id: i64, games_infos: Vec<(i64, String)>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    for game_info in games_infos {
        keyboard.push(vec![InlineKeyboardButton::callback(game_info.1, GamesListCallback{user_id, game_id: Some(game_info.0), button: GamesListButton::SelectGame}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в управление играми", GamesListCallback{user_id, game_id: None, button: GamesListButton::BackToGameManagement}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Управление Игрой >==\nВыберите игру:".to_string(), InlineKeyboardMarkup::new(keyboard))
}

fn create_game_info(game: &types::Game, escape_html: bool) -> String {
    let mut registrations_info = String::new();

    if game.registrations.is_empty() {
        registrations_info = "\nПока никто не записался".to_string();
    } else {
        for registration in &game.registrations {
            let buy_info = if game.participation_fee != 0 && !game.mandatory_fee {
                if registration.is_buying {
                    "💰 ".to_string()
                } else {
                    "❌ ".to_string()
                }
            } else {
                "".to_string()
            };

            registrations_info += format!("\n- {}{}{}",
                buy_info,
                match &registration.display_name {
                    Some(display_name) => if escape_html { teloxide::utils::html::escape(display_name)} else { display_name.clone() },
                    None => "ERR:NAME".to_string()
                },
                match &registration.telegram_tag {
                    Some(tag) => format!(" ({})", tag),
                    None => "".to_string()
                }
            ).as_str();
        }
    }

    let player_counter = if game.required_player_count != 0 {
        format!(" [{} / {}]", game.registrations.len(), game.required_player_count)
    } else {
        "".to_string()
    };

    let day_str = match game.date_time.weekday().number_from_monday() {
        1 => " (Пн)".to_string(),
        2 => " (Вт)".to_string(),
        3 => " (Ср)".to_string(),
        4 => " (Чт)".to_string(),
        5 => " (Пт)".to_string(),
        6 => " (Сб)".to_string(),
        7 => " (Вс)".to_string(),
        _ => "".to_string(),
    };

    format!("[{}] Игра {} от {}{} в {}\n{}\n=== Список записавшихся{} ==={}",
        game.id,
        match game.format {
            types::GameFormat::Commander => "EDH",
            types::GameFormat::Standard => "стандарт",
            types::GameFormat::CommanderDraft => "EDH Драфт",
            types::GameFormat::StandardDraft => "Драфт"
        },
        game.date_time.format("%d.%m.%Y, %H:%M"),
        day_str,
        game.place,
        game.description,
        player_counter,
        registrations_info,
    )
}

pub fn create_manage_game_menu(player: types::Player, game: types::Game) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    let user_id = player.id;

    if (game.required_player_count == 0) || (game.required_player_count != 0 && game.registrations.len() < game.required_player_count as usize) {
        let signup_text = if game.mandatory_fee {
            format!("Записаться ({} ₽)", game.participation_fee)
        } else {
            "Записаться".to_string()
        };
        keyboard.push(vec![InlineKeyboardButton::callback(signup_text, ManageGameCallback { user_id, game_id: game.id, button: ManageGameButton::SignUp }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    if !game.mandatory_fee && game.participation_fee != 0 {
        keyboard.push(vec![InlineKeyboardButton::callback(format!("Выкупить/Не выкупать ({} ₽)", game.participation_fee), ManageGameCallback { user_id, game_id: game.id, button: ManageGameButton::ToggleBuyout }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    keyboard.push(vec![InlineKeyboardButton::callback("Отменить запись", ManageGameCallback{ user_id, game_id: game.id, button: ManageGameButton::Quit }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);


    if player.is_admin || player.id == game.creator_id {
        match game.format {
        types::GameFormat::Commander => {
                keyboard.push(vec![InlineKeyboardButton::callback("Сгенерировать столы", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::GenerateSittingsGroups}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
            },
            types::GameFormat::Standard => {
                keyboard.push(vec![InlineKeyboardButton::callback("Сгенерировать столы", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::GenerateSittingsPairs}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
            },
            types::GameFormat::CommanderDraft | types::GameFormat::StandardDraft => {
                // Drafts should be handles by tournaments
            }
        };

        keyboard.push(vec![InlineKeyboardButton::callback("Изменить место", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::EditPlace}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        keyboard.push(vec![InlineKeyboardButton::callback("Изменить описание", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::EditDescription}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        keyboard.push(vec![InlineKeyboardButton::callback("Изменить дату и время", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::EditDateTime}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

        match game.format {
            types::GameFormat::CommanderDraft | types::GameFormat::StandardDraft => {
                keyboard.push(vec![InlineKeyboardButton::callback("Изменить взнос", ManageGameCallback { user_id, game_id: game.id, button: ManageGameButton::EditFee}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
                keyboard.push(vec![InlineKeyboardButton::callback("Изменить обязательность взноса", ManageGameCallback { user_id, game_id: game.id, button: ManageGameButton::EditMandatory}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
                keyboard.push(vec![InlineKeyboardButton::callback("Изменить число игроков", ManageGameCallback { user_id, game_id: game.id, button: ManageGameButton::EditRequiredPlayerCount}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
            },
            types::GameFormat::Commander | types::GameFormat::Standard => {
                // No additional buttons for these
            }
        }

        keyboard.push(vec![InlineKeyboardButton::callback("Добавить игрока", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::AddPlayer}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        keyboard.push(vec![InlineKeyboardButton::callback("Удалить игрока", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::RemovePlayer}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
        keyboard.push(vec![InlineKeyboardButton::callback("Удалить игру", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::DeleteGame}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }
    keyboard.push(vec![InlineKeyboardButton::callback("Назад в список игр", ManageGameCallback {user_id, game_id: game.id, button: ManageGameButton::BackToGamesList}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    (create_game_info(&game, false), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_remove_player_menu(game_id: i64, registrations: Vec<types::GameRegistration>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];

    for registration in registrations {
        let button_type = match registration.reg_type {
            types::RegistrationType::Normal => {
                RemovePlayerButton::RemovePlayerNormal
            },
            types::RegistrationType::TelegramId => {
                RemovePlayerButton::RemovePlayerTelegramId
            },
            types::RegistrationType::NameOnly => {
                RemovePlayerButton::RemovePlayerNameOnly
            }
        };

        let player_str = match &registration.display_name {
            Some(display_name) => display_name,
            None => "ERR:NAME"
        };

        keyboard.push(vec![InlineKeyboardButton::callback(player_str, RemovePlayerCallback{reg_id: registration.reg_id, game_id, button: button_type}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    keyboard.push(vec![InlineKeyboardButton::callback("Назад в редактирование игры", RemovePlayerCallback{reg_id: 0, game_id, button: RemovePlayerButton::BackToManageGame }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("Выберите игрока, которого хотите удалить".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_poll_management_menu(user_id: i64) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Запланировать опрос", PollManagementCallback{ user_id, button: PollManagementButton::SchedulePoll }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Редактировать опрос", PollManagementCallback{ user_id, button: PollManagementButton::ManagePolls }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Выйти в меню", PollManagementCallback{ user_id, button: PollManagementButton::BackToMainMenu }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    ("==< Управление Опросами >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_game_menu(game: &types::Game, bot_url: &str) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];

    if (game.required_player_count == 0) || (game.required_player_count != 0 && game.registrations.len() < game.required_player_count as usize) {
        let signup_text = if game.mandatory_fee {
            format!("Записаться ({} ₽)", game.participation_fee)
        } else {
            "Записаться".to_string()
        };
        keyboard.push(vec![InlineKeyboardButton::callback(signup_text, GameCallback { game_id: game.id, button: GameButton::SignUp }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    if !game.mandatory_fee && game.participation_fee != 0 {
        keyboard.push(vec![InlineKeyboardButton::callback(format!("Выкупить/Не выкупать ({} ₽)", game.participation_fee), GameCallback { game_id: game.id, button: GameButton::ToggleBuyout }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    }

    keyboard.push(vec![InlineKeyboardButton::callback("Отменить запись", GameCallback{ game_id: game.id, button: GameButton::Quit }.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::url("Управление", format!("{}?start={}", bot_url, GameCallback{ game_id: game.id, button: GameButton::Manage }.to_universal().to_base64().expect("Failed encoding callback to base64!")).parse().expect("Failed creating URL for game management"))]);

    (create_game_info(&game, true), InlineKeyboardMarkup::new(keyboard))
}

pub fn create_achievement_claim_menu(user_id: i64, claim_id: i64, player_str: String, achievement: types::Achievement) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    keyboard.push(vec![InlineKeyboardButton::callback("Одобрить ✅", AchievementClaimCallback{user_id, claim_id, button: AchievementClaimButton::Approve}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);
    keyboard.push(vec![InlineKeyboardButton::callback("Отказать ❌", AchievementClaimCallback{user_id, claim_id, button: AchievementClaimButton::Reject}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    let text = format!("Игрок {} запрашивает достижение \"{}\": {}",
        player_str,
        achievement.name,
        achievement.description,
    );

    (text, InlineKeyboardMarkup::new(keyboard))
}

pub fn create_links_menu(user_id: i64, link_infos: Vec<(String, String)>) -> (String, InlineKeyboardMarkup) {
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    for (text, link) in link_infos {
        match link.parse() {
            Ok(parsed_url) => {
                keyboard.push(vec![InlineKeyboardButton::url(text, parsed_url)]);
            }
            Err(err) => {
                log::error!("Failed parsing \"{}\" for \"{}\" with error: {}", link, text, err);
            }
        }
    }
    keyboard.push(vec![InlineKeyboardButton::callback("Назад", LinksCallback{user_id, button: LinksButton::BackToMainMenu}.to_universal().to_base64().expect("Failed encoding callback to base64!"))]);

    ("==< Ссылки >==".to_string(), InlineKeyboardMarkup::new(keyboard))
}

pub struct MainCallback {
    pub user_id: i64,
    pub button: MainButton,
}

pub struct ProfileCallback {
    pub user_id: i64,
    pub button: ProfileButton,
}

pub struct GameManagementCallback {
    pub user_id: i64,
    pub button: GameManagementButton,
}

pub struct GamesListCallback {
    pub user_id: i64,
    pub game_id: Option<i64>,
    pub button: GamesListButton,
}

pub struct ManageGameCallback {
    pub user_id: i64,
    pub game_id: i64,
    pub button: ManageGameButton,
}

pub struct RemovePlayerCallback {
    pub reg_id: i64,
    pub game_id: i64,
    pub button: RemovePlayerButton,
}

pub struct PollManagementCallback {
    pub user_id: i64,
    pub button: PollManagementButton,
}

pub struct EditPreferencesCallback {
    pub user_id: i64,
    pub button: EditPreferencesButton,
}

pub struct AchievementsCallback {
    pub user_id: i64,
    pub button: AchievementsButton,
    pub page: Option<u64>,
    pub achievement_id: Option<i64>,
}

pub struct AllAchievementsCallback {
    pub user_id: i64,
    pub button: AllAchievementsButton,
    pub page: Option<u64>,
    pub achievement_id: Option<i64>,
}

pub struct AchievementInfoCallback {
    pub user_id: i64,
    pub button: AchievementInfoButton,
    pub achievement_id: i64,
}

pub struct ConfirmationCallback {
    pub context: u64,
    pub button: ConfirmationButton,
    pub action: ConfirmationAction,
}

pub struct GameFormatCallback {
    pub user_id: i64,
    pub button: GameFormatButton,
}

pub struct GameCallback {
    pub game_id: i64,
    pub button: GameButton,
}

pub struct AchievementClaimCallback {
    pub user_id: i64,
    pub claim_id: i64,
    pub button: AchievementClaimButton,
}

pub struct LinksCallback {
    pub user_id: i64,
    pub button: LinksButton,
}

pub struct PollFormatCallback {
    pub user_id: i64,
    pub button: PollFormatButton,
}

pub struct PollsListCallback {
    pub user_id: i64,
    pub poll_id: i64,
    pub button: PollsListButton,
}

pub struct EditPollCallback {
    pub user_id: i64,
    pub poll_id: i64,
    pub button: EditPollButton,
}

pub struct ManageSubscriptionsCallback {
    pub user_id: i64,
    pub button: ManageSubscriptionsButton,
}

pub struct UniversalCallback {
    var1: u64,
    var2: u8,
    var3: u8,
    var4: u64,
}

impl MainCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::Main),
            var3: match self.button {
                MainButton::OpenProfile => 0,
                MainButton::ManageGames => 1,
                MainButton::ManagePolls => 2,
                MainButton::Links => 3,
                MainButton::ExitMenu => 4,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<MainCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::Main) {
            Ok(MainCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => MainButton::OpenProfile,
                    1 => MainButton::ManageGames,
                    2 => MainButton::ManagePolls,
                    3 => MainButton::Links,
                    4 => MainButton::ExitMenu,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl ProfileCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::Profile),
            var3: match self.button {
                ProfileButton::EditDisplayName => 0,
                ProfileButton::EditPreferences => 1,
                ProfileButton::EditSubscriptions => 2,
                ProfileButton::ViewAchievements => 3,
                ProfileButton::ViewAllAchievements => 4,
                ProfileButton::DeleteProfile => 5,
                ProfileButton::BackToMainMenu => 6,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<ProfileCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::Profile) {
            Ok(ProfileCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => ProfileButton::EditDisplayName,
                    1 => ProfileButton::EditPreferences,
                    2 => ProfileButton::EditSubscriptions,
                    3 => ProfileButton::ViewAchievements,
                    4 => ProfileButton::ViewAllAchievements,
                    5 => ProfileButton::DeleteProfile,
                    6 => ProfileButton::BackToMainMenu,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl GameManagementCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::GameManagement),
            var3: match self.button {
                GameManagementButton::AddGame => 0,
                GameManagementButton::MyGames => 1,
                GameManagementButton::AllGames => 2,
                GameManagementButton::BackToMainMenu => 3,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<GameManagementCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::GameManagement) {
            Ok(GameManagementCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => GameManagementButton::AddGame,
                    1 => GameManagementButton::MyGames,
                    2 => GameManagementButton::AllGames,
                    3 => GameManagementButton::BackToMainMenu,
                    _ => { return Err(types::ErrorCode::InternalError) }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl GamesListCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::GamesList),
            var3: match self.button {
                GamesListButton::BackToGameManagement => 0,
                GamesListButton::SelectGame => 1,
            },
            var4: self.game_id.unwrap_or(0) as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<GamesListCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::GamesList) {
            Ok(GamesListCallback{
                user_id: callback.var1 as i64,
                game_id: match callback.var4 {
                    0 => None,
                    _ => Some(callback.var4 as i64)
                },
                button: match callback.var3 {
                    0 => GamesListButton::BackToGameManagement,
                    1 => GamesListButton::SelectGame,
                    _ => { return Err(types::ErrorCode::InternalError) }
                },
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl ManageGameCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::ManageGame),
            var3: match self.button {
                ManageGameButton::SignUp => 0,
                ManageGameButton::ToggleBuyout => 1,
                ManageGameButton::Quit => 2,
                ManageGameButton::GenerateSittingsGroups => 3,
                ManageGameButton::GenerateSittingsPairs => 4,
                ManageGameButton::EditPlace => 5,
                ManageGameButton::EditDescription => 6,
                ManageGameButton::EditDateTime => 7,
                ManageGameButton::EditFee => 8,
                ManageGameButton::EditMandatory => 9,
                ManageGameButton::EditRequiredPlayerCount => 10,
                ManageGameButton::AddPlayer => 11,
                ManageGameButton::RemovePlayer => 12,
                ManageGameButton::DeleteGame => 13,
                ManageGameButton::BackToGamesList => 14,
            },
            var4: self.game_id as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<ManageGameCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::ManageGame) {
            Ok(ManageGameCallback {
                user_id: callback.var1 as i64,
                game_id: callback.var4 as i64,
                button: match callback.var3 {
                    00 => ManageGameButton::SignUp,
                    01 => ManageGameButton::ToggleBuyout,
                    02 => ManageGameButton::Quit,
                    03 => ManageGameButton::GenerateSittingsGroups,
                    04 => ManageGameButton::GenerateSittingsPairs,
                    05 => ManageGameButton::EditPlace,
                    06 => ManageGameButton::EditDescription,
                    07 => ManageGameButton::EditDateTime,
                    08 => ManageGameButton::EditFee,
                    09 => ManageGameButton::EditMandatory,
                    10 => ManageGameButton::EditRequiredPlayerCount,
                    11 => ManageGameButton::AddPlayer,
                    12 => ManageGameButton::RemovePlayer,
                    13 => ManageGameButton::DeleteGame,
                    14 => ManageGameButton::BackToGamesList,
                     _ => { return Err(types::ErrorCode::InternalError) }
                },
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl RemovePlayerCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.reg_id as u64,
            var2: Menu::to_u8(&Menu::RemovePlayer),
            var3: match self.button {
                RemovePlayerButton::BackToManageGame => 0,
                RemovePlayerButton::RemovePlayerNormal => 1,
                RemovePlayerButton::RemovePlayerTelegramId => 2,
                RemovePlayerButton::RemovePlayerNameOnly => 3,
            },
            var4: self.game_id as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<RemovePlayerCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::RemovePlayer) {
            Ok(RemovePlayerCallback{
                reg_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => RemovePlayerButton::BackToManageGame,
                    1 => RemovePlayerButton::RemovePlayerNormal,
                    2 => RemovePlayerButton::RemovePlayerTelegramId,
                    3 => RemovePlayerButton::RemovePlayerNameOnly,
                    _ => { return Err(types::ErrorCode::InternalError) }
                },
                game_id: callback.var4 as i64
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl PollManagementCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback {
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::PollManagement),
            var3: match self.button {
                PollManagementButton::SchedulePoll => 0,
                PollManagementButton::ManagePolls => 1,
                PollManagementButton::BackToMainMenu => 2,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<PollManagementCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::PollManagement) {
            Ok(PollManagementCallback {
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => PollManagementButton::SchedulePoll,
                    1 => PollManagementButton::ManagePolls,
                    2 => PollManagementButton::BackToMainMenu,
                    _ => { return Err(types::ErrorCode::InternalError) }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl EditPreferencesCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::EditPreferences),
            var3: match self.button {
                EditPreferencesButton::EditMaxGamesInWeek => 0,
                EditPreferencesButton::ResetBadge => 1,
                EditPreferencesButton::BackToProfile => 2,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<EditPreferencesCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::EditPreferences) {
            Ok(EditPreferencesCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => EditPreferencesButton::EditMaxGamesInWeek,
                    1 => EditPreferencesButton::ResetBadge,
                    2 => EditPreferencesButton::BackToProfile,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl AchievementsCallback {
    pub fn to_universal(self) -> UniversalCallback {
        let var4 = match self.button {
            AchievementsButton::AchievementInfo => self.achievement_id.unwrap() as u64,
            AchievementsButton::PreviousPage | AchievementsButton::NextPage => self.page.unwrap(),
            AchievementsButton::BackToProfile => 0,
        };

        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::Achievements),
            var3: match self.button {
                AchievementsButton::AchievementInfo => 0,
                AchievementsButton::PreviousPage => 1,
                AchievementsButton::NextPage => 2,
                AchievementsButton::BackToProfile => 3,
            },
            var4
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<AchievementsCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::Achievements) {
            let button = match callback.var3 {
                0 => AchievementsButton::AchievementInfo,
                1 => AchievementsButton::PreviousPage,
                2 => AchievementsButton::NextPage,
                3 => AchievementsButton::BackToProfile,
                _ => { return Err(types::ErrorCode::InternalError); }
            };

            let page = match button {
                AchievementsButton::AchievementInfo => None,
                AchievementsButton::PreviousPage | AchievementsButton::NextPage => Some(callback.var4),
                AchievementsButton::BackToProfile => None,
            };

            let achievement_id = match button {
                AchievementsButton::AchievementInfo => Some(callback.var4 as i64),
                AchievementsButton::PreviousPage | AchievementsButton::NextPage => None,
                AchievementsButton::BackToProfile => None,
            };

            Ok(AchievementsCallback{
                user_id: callback.var1 as i64,
                button,
                page,
                achievement_id
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl AllAchievementsCallback {
    pub fn to_universal(self) -> UniversalCallback {
        let var4 = match self.button {
            AllAchievementsButton::AchievementInfo => self.achievement_id.unwrap() as u64,
            AllAchievementsButton::PreviousPage | AllAchievementsButton::NextPage => self.page.unwrap(),
            AllAchievementsButton::BackToProfile => 0,
        };

        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::AllAchievements),
            var3: match self.button {
                AllAchievementsButton::AchievementInfo => 0,
                AllAchievementsButton::PreviousPage => 1,
                AllAchievementsButton::NextPage => 2,
                AllAchievementsButton::BackToProfile => 3,
            },
            var4
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<AllAchievementsCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::AllAchievements) {
            let button = match callback.var3 {
                0 => AllAchievementsButton::AchievementInfo,
                1 => AllAchievementsButton::PreviousPage,
                2 => AllAchievementsButton::NextPage,
                3 => AllAchievementsButton::BackToProfile,
                _ => { return Err(types::ErrorCode::InternalError); }
            };

            let page = match button {
                AllAchievementsButton::AchievementInfo => None,
                AllAchievementsButton::PreviousPage | AllAchievementsButton::NextPage => Some(callback.var4),
                AllAchievementsButton::BackToProfile => None,
            };

            let achievement_id = match button {
                AllAchievementsButton::AchievementInfo => Some(callback.var4 as i64),
                AllAchievementsButton::PreviousPage | AllAchievementsButton::NextPage => None,
                AllAchievementsButton::BackToProfile => None,
            };

            Ok(AllAchievementsCallback{
                user_id: callback.var1 as i64,
                button,
                page,
                achievement_id
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl AchievementInfoCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::AchievementInfo),
            var3: match self.button {
                AchievementInfoButton::Claim => 0,
                AchievementInfoButton::UseBadge => 1,
                AchievementInfoButton::BackToAchievements => 2,
                AchievementInfoButton::BackToAllAchievements => 3,
            },
            var4: self.achievement_id as u64
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<AchievementInfoCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::AchievementInfo) {
            Ok(AchievementInfoCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => AchievementInfoButton::Claim,
                    1 => AchievementInfoButton::UseBadge,
                    2 => AchievementInfoButton::BackToAchievements,
                    3 => AchievementInfoButton::BackToAllAchievements,
                    _ => { return Err(types::ErrorCode::InternalError); }
                },
                achievement_id: callback.var4 as i64
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl ConfirmationCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.context,
            var2: Menu::to_u8(&Menu::Confirmation),
            var3: match self.button {
                ConfirmationButton::Confirm => 0,
                ConfirmationButton::Cancel => 1,
            },
            var4: match self.action {
                ConfirmationAction::ProfileDeletion => 0,
                ConfirmationAction::GameDeletion => 1,
                ConfirmationAction::DuplicatePollCreation => 2,
            },
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<ConfirmationCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::Confirmation) {
            Ok(ConfirmationCallback {
                context: callback.var1,
                button: match callback.var3 {
                    0 => ConfirmationButton::Confirm,
                    1 => ConfirmationButton::Cancel,
                    _ => { return Err(types::ErrorCode::InternalError); }
                },
                action: match callback.var4 {
                    0 => ConfirmationAction::ProfileDeletion,
                    1 => ConfirmationAction::GameDeletion,
                    2 => ConfirmationAction::DuplicatePollCreation,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl GameFormatCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::GameType),
            var3: match self.button {
                GameFormatButton::Commander => 0,
                GameFormatButton::Standard => 1,
                GameFormatButton::CommanderDraft => 2,
                GameFormatButton::StandardDraft => 3,
                GameFormatButton::Cancel => 4,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<GameFormatCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::GameType) {
            Ok(GameFormatCallback {
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => GameFormatButton::Commander,
                    1 => GameFormatButton::Standard,
                    2 => GameFormatButton::CommanderDraft,
                    3 => GameFormatButton::StandardDraft,
                    4 => GameFormatButton::Cancel,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl GameCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.game_id as u64,
            var2: Menu::to_u8(&Menu::Game),
            var3: match self.button {
                GameButton::SignUp => 0,
                GameButton::ToggleBuyout => 1,
                GameButton::Quit => 2,
                GameButton::Manage => 3,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<GameCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::Game) {
            Ok(GameCallback{
                game_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => GameButton::SignUp,
                    1 => GameButton::ToggleBuyout,
                    2 => GameButton::Quit,
                    3 => GameButton::Manage,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl AchievementClaimCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::AchievementClaim),
            var3: match self.button {
                AchievementClaimButton::Approve => 0,
                AchievementClaimButton::Reject => 1,
            },
            var4: self.claim_id as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<AchievementClaimCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::AchievementClaim) {
            Ok(AchievementClaimCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => AchievementClaimButton::Approve,
                    1 => AchievementClaimButton::Reject,
                    _ => { return Err(types::ErrorCode::InternalError); }
                },
                claim_id: callback.var4 as i64,
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl LinksCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::LinksMenu),
            var3: match self.button {
                LinksButton::BackToMainMenu => 0,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<LinksCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::LinksMenu) {
            Ok(LinksCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => LinksButton::BackToMainMenu,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl PollFormatCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::PollFormat),
            var3: match self.button {
                PollFormatButton::Commander => 0,
                PollFormatButton::Standard => 1,
                PollFormatButton::CommanderDraft => 2,
                PollFormatButton::StandardDraft => 3,
                PollFormatButton::Cancel => 4,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<PollFormatCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::PollFormat) {
            Ok(PollFormatCallback {
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => PollFormatButton::Commander,
                    1 => PollFormatButton::Standard,
                    2 => PollFormatButton::CommanderDraft,
                    3 => PollFormatButton::StandardDraft,
                    4 => PollFormatButton::Cancel,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl PollsListCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::PollsList),
            var3: match self.button {
                PollsListButton::BackToPollsManagement => 0,
                PollsListButton::SelectPoll => 1,
            },
            var4: self.poll_id as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<PollsListCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::PollsList) {
            Ok(PollsListCallback{
                user_id: callback.var1 as i64,
                poll_id: callback.var4 as i64,
                button: match callback.var3 {
                    0 => PollsListButton::BackToPollsManagement,
                    1 => PollsListButton::SelectPoll,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl EditPollCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::EditPoll),
            var3: match self.button {
                EditPollButton::EditQuestion => 0,
                EditPollButton::EditDescription => 1,
                EditPollButton::EditGameTime => 2,
                EditPollButton::EditStartTime => 3,
                EditPollButton::EditStartDay => 4,
                EditPollButton::EditEndTime => 5,
                EditPollButton::EditEndDay => 6,
                EditPollButton::DeletePoll => 7,
                EditPollButton::BackToPollsList => 8,
            },
            var4: self.poll_id as u64,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<EditPollCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::EditPoll) {
            Ok(EditPollCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => EditPollButton::EditQuestion,
                    1 => EditPollButton::EditDescription,
                    2 => EditPollButton::EditGameTime,
                    3 => EditPollButton::EditStartTime,
                    4 => EditPollButton::EditStartDay,
                    5 => EditPollButton::EditEndTime,
                    6 => EditPollButton::EditEndDay,
                    7 => EditPollButton::DeletePoll,
                    8 => EditPollButton::BackToPollsList,
                    _ => { return Err(types::ErrorCode::InternalError); }
                },
                poll_id: callback.var4 as i64,
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl ManageSubscriptionsCallback {
    pub fn to_universal(self) -> UniversalCallback {
        UniversalCallback{
            var1: self.user_id as u64,
            var2: Menu::to_u8(&Menu::ManageSubscriptions),
            var3: match self.button {
                ManageSubscriptionsButton::ToggleNewStdSubscription => 0,
                ManageSubscriptionsButton::ToggleNewEdhSubscription => 1,
                ManageSubscriptionsButton::ToggleNewStdDraftSubscription => 2,
                ManageSubscriptionsButton::ToggleNewEdhDraftSubscription => 3,
                ManageSubscriptionsButton::BackToProfile => 4,
            },
            var4: 0,
        }
    }

    pub fn from_universal(callback: UniversalCallback) -> Result<ManageSubscriptionsCallback, types::ErrorCode> {
        if matches!(callback.get_menu()?, Menu::ManageSubscriptions) {
            Ok(ManageSubscriptionsCallback{
                user_id: callback.var1 as i64,
                button: match callback.var3 {
                    0 => ManageSubscriptionsButton::ToggleNewStdSubscription,
                    1 => ManageSubscriptionsButton::ToggleNewEdhSubscription,
                    2 => ManageSubscriptionsButton::ToggleNewStdDraftSubscription,
                    3 => ManageSubscriptionsButton::ToggleNewEdhDraftSubscription,
                    4 => ManageSubscriptionsButton::BackToProfile,
                    _ => { return Err(types::ErrorCode::InternalError); }
                }
            })
        } else {
            Err(types::ErrorCode::InternalError)
        }
    }
}

impl UniversalCallback {
    pub fn get_menu(&self) -> Result<Menu, types::ErrorCode> {
        Menu::from_u8(self.var2)
    }

    pub fn from_base64(data: &String) -> Result<UniversalCallback, types::ErrorCode> {
        let length = data.len();
        return match length {
            14 => {
                let decoded_data = Self::decode_base64(data)?;
                Ok(UniversalCallback{
                    var1: Self::join_u8(decoded_data[00], decoded_data[01], decoded_data[02], decoded_data[03], decoded_data[04], decoded_data[05], decoded_data[06], decoded_data[07]),
                    var2: decoded_data[8],
                    var3: decoded_data[9],
                    var4: 0,
                })
            }
            24 => {
                let decoded_data = Self::decode_base64(data)?;
                Ok(UniversalCallback{
                    var1: Self::join_u8(decoded_data[00], decoded_data[01], decoded_data[02], decoded_data[03], decoded_data[04], decoded_data[05], decoded_data[06], decoded_data[07]),
                    var2: decoded_data[8],
                    var3: decoded_data[9],
                    var4: Self::join_u8(decoded_data[10], decoded_data[11], decoded_data[12], decoded_data[13], decoded_data[14], decoded_data[15], decoded_data[16], decoded_data[17]),
                })
            }
            _  => Err(types::ErrorCode::InternalError)
        }
    }

    pub fn to_base64(&self) -> Result<String, types::ErrorCode> {
        let data: Vec<u8> = if self.var4 == 0 {
            vec![(self.var1 >> 56) as u8, ((self.var1 >> 48) & 0xFF) as u8, ((self.var1 >> 40) & 0xFF) as u8, ((self.var1 >> 32) & 0xFF) as u8, ((self.var1 >> 24) & 0xFF) as u8, ((self.var1 >> 16) & 0xFF) as u8, ((self.var1 >> 8) & 0xFF) as u8, (self.var1 & 0xFF) as u8,
                 self.var2, self.var3
            ]
        } else {
            vec![(self.var1 >> 56) as u8, ((self.var1 >> 48) & 0xFF) as u8, ((self.var1 >> 40) & 0xFF) as u8, ((self.var1 >> 32) & 0xFF) as u8, ((self.var1 >> 24) & 0xFF) as u8, ((self.var1 >> 16) & 0xFF) as u8, ((self.var1 >> 8) & 0xFF) as u8, (self.var1 & 0xFF) as u8,
                 self.var2, self.var3,
                 (self.var4 >> 56) as u8, ((self.var4 >> 48) & 0xFF) as u8, ((self.var4 >> 40) & 0xFF) as u8, ((self.var4 >> 32) & 0xFF) as u8, ((self.var4 >> 24) & 0xFF) as u8, ((self.var4 >> 16) & 0xFF) as u8, ((self.var4 >> 8) & 0xFF) as u8, (self.var4 & 0xFF) as u8
            ]
        };
        Self::encode_base64(&data)
    }

    fn base64_to_u8(c: char) -> Result<u8, types::ErrorCode> {
        return match c {
            'A' => Ok(0x00_u8),
            'B' => Ok(0x01_u8),
            'C' => Ok(0x02_u8),
            'D' => Ok(0x03_u8),
            'E' => Ok(0x04_u8),
            'F' => Ok(0x05_u8),
            'G' => Ok(0x06_u8),
            'H' => Ok(0x07_u8),
            'I' => Ok(0x08_u8),
            'J' => Ok(0x09_u8),
            'K' => Ok(0x0A_u8),
            'L' => Ok(0x0B_u8),
            'M' => Ok(0x0C_u8),
            'N' => Ok(0x0D_u8),
            'O' => Ok(0x0E_u8),
            'P' => Ok(0x0F_u8),
            'Q' => Ok(0x10_u8),
            'R' => Ok(0x11_u8),
            'S' => Ok(0x12_u8),
            'T' => Ok(0x13_u8),
            'U' => Ok(0x14_u8),
            'V' => Ok(0x15_u8),
            'W' => Ok(0x16_u8),
            'X' => Ok(0x17_u8),
            'Y' => Ok(0x18_u8),
            'Z' => Ok(0x19_u8),
            'a' => Ok(0x1A_u8),
            'b' => Ok(0x1B_u8),
            'c' => Ok(0x1C_u8),
            'd' => Ok(0x1D_u8),
            'e' => Ok(0x1E_u8),
            'f' => Ok(0x1F_u8),
            'g' => Ok(0x20_u8),
            'h' => Ok(0x21_u8),
            'i' => Ok(0x22_u8),
            'j' => Ok(0x23_u8),
            'k' => Ok(0x24_u8),
            'l' => Ok(0x25_u8),
            'm' => Ok(0x26_u8),
            'n' => Ok(0x27_u8),
            'o' => Ok(0x28_u8),
            'p' => Ok(0x29_u8),
            'q' => Ok(0x2A_u8),
            'r' => Ok(0x2B_u8),
            's' => Ok(0x2C_u8),
            't' => Ok(0x2D_u8),
            'u' => Ok(0x2E_u8),
            'v' => Ok(0x2F_u8),
            'w' => Ok(0x30_u8),
            'x' => Ok(0x31_u8),
            'y' => Ok(0x32_u8),
            'z' => Ok(0x33_u8),
            '0' => Ok(0x34_u8),
            '1' => Ok(0x35_u8),
            '2' => Ok(0x36_u8),
            '3' => Ok(0x37_u8),
            '4' => Ok(0x38_u8),
            '5' => Ok(0x39_u8),
            '6' => Ok(0x3A_u8),
            '7' => Ok(0x3B_u8),
            '8' => Ok(0x3C_u8),
            '9' => Ok(0x3D_u8),
            '+' => Ok(0x3E_u8),
            '/' => Ok(0x3F_u8),
            _   => Err(types::ErrorCode::InternalError)
        }
    }

    fn u8_to_base64(u: u8) -> Result<char, types::ErrorCode> {
        return match u {
            0x00_u8 => Ok('A'),
            0x01_u8 => Ok('B'),
            0x02_u8 => Ok('C'),
            0x03_u8 => Ok('D'),
            0x04_u8 => Ok('E'),
            0x05_u8 => Ok('F'),
            0x06_u8 => Ok('G'),
            0x07_u8 => Ok('H'),
            0x08_u8 => Ok('I'),
            0x09_u8 => Ok('J'),
            0x0A_u8 => Ok('K'),
            0x0B_u8 => Ok('L'),
            0x0C_u8 => Ok('M'),
            0x0D_u8 => Ok('N'),
            0x0E_u8 => Ok('O'),
            0x0F_u8 => Ok('P'),
            0x10_u8 => Ok('Q'),
            0x11_u8 => Ok('R'),
            0x12_u8 => Ok('S'),
            0x13_u8 => Ok('T'),
            0x14_u8 => Ok('U'),
            0x15_u8 => Ok('V'),
            0x16_u8 => Ok('W'),
            0x17_u8 => Ok('X'),
            0x18_u8 => Ok('Y'),
            0x19_u8 => Ok('Z'),
            0x1A_u8 => Ok('a'),
            0x1B_u8 => Ok('b'),
            0x1C_u8 => Ok('c'),
            0x1D_u8 => Ok('d'),
            0x1E_u8 => Ok('e'),
            0x1F_u8 => Ok('f'),
            0x20_u8 => Ok('g'),
            0x21_u8 => Ok('h'),
            0x22_u8 => Ok('i'),
            0x23_u8 => Ok('j'),
            0x24_u8 => Ok('k'),
            0x25_u8 => Ok('l'),
            0x26_u8 => Ok('m'),
            0x27_u8 => Ok('n'),
            0x28_u8 => Ok('o'),
            0x29_u8 => Ok('p'),
            0x2A_u8 => Ok('q'),
            0x2B_u8 => Ok('r'),
            0x2C_u8 => Ok('s'),
            0x2D_u8 => Ok('t'),
            0x2E_u8 => Ok('u'),
            0x2F_u8 => Ok('v'),
            0x30_u8 => Ok('w'),
            0x31_u8 => Ok('x'),
            0x32_u8 => Ok('y'),
            0x33_u8 => Ok('z'),
            0x34_u8 => Ok('0'),
            0x35_u8 => Ok('1'),
            0x36_u8 => Ok('2'),
            0x37_u8 => Ok('3'),
            0x38_u8 => Ok('4'),
            0x39_u8 => Ok('5'),
            0x3A_u8 => Ok('6'),
            0x3B_u8 => Ok('7'),
            0x3C_u8 => Ok('8'),
            0x3D_u8 => Ok('9'),
            0x3E_u8 => Ok('+'),
            0x3F_u8 => Ok('/'),
            _       => Err(types::ErrorCode::InternalError)
        }
    }

    fn decode_base64(data: &String) -> Result<Vec<u8>, types::ErrorCode> {
        let mut decoded_data: Vec<u8> = vec![];
        let mut current8 = 0_u8;
        let mut bits_to_save: i32 = 6;
        let mut bits_count: i32 = 0;

        decoded_data.reserve(data.len() * 3 / 4 + 1);

        for c in data.chars() {
            let current6 = Self::base64_to_u8(c)?;
            current8 <<= bits_to_save;
            current8 |= current6 >> (6 - bits_to_save);
            bits_count += bits_to_save;
            if bits_count == 8 {
                decoded_data.push(current8);
                current8 = current6 & (0x3F >> bits_to_save);
                bits_count = 6 - bits_to_save;
            }
            bits_to_save = std::cmp::min(8 - bits_count, 6);
        }
        if current8 != 0 {
            return Err(types::ErrorCode::InternalError);
        }

        Ok(decoded_data)
    }

    fn encode_base64(data: &Vec<u8>) -> Result<String, types::ErrorCode> {
        let mut encoded_str: String = String::with_capacity(data.len() * 4 / 3);

        let mut remaining: u8 = 0_u8;
        let mut count: i32 = 6;

        for v in data {
            let current = remaining | (v >> (8 - count));
            encoded_str.push(Self::u8_to_base64(current)?);
            count -= 2;
            remaining = (v << count) & 0x3F;
            if count == 0 {
                encoded_str.push(Self::u8_to_base64(remaining)?);
                remaining = 0;
                count = 6;
            }
        }
        if count != 6 {
            encoded_str.push(Self::u8_to_base64(remaining)?);
        }

        Ok(encoded_str)
    }

    fn join_u8(p1: u8, p2: u8, p3: u8, p4: u8, p5: u8, p6: u8, p7: u8, p8: u8) -> u64 {
        ((p1 as u64) << 56) | ((p2 as u64) << 48) | ((p3 as u64) << 40) | ((p4 as u64) << 32) | ((p5 as u64) << 24) | ((p6 as u64) << 16) | ((p7 as u64) << 8) | (p8 as u64)
    }
}

#[cfg(test)]
mod test {
    use crate::mtg::menus::UniversalCallback;

    #[test]
    fn base64_encode_test() {
        let data : Vec<u8> = vec![77, 97, 110, 121, 32, 104, 97, 110, 100, 115, 32, 109, 97, 107, 101, 32, 108, 105, 103, 104, 116, 32, 119, 111, 114, 107, 46];
        let expected_str : String = "TWFueSBoYW5kcyBtYWtlIGxpZ2h0IHdvcmsu".to_string();

        let encoded_str = UniversalCallback::encode_base64(&data).expect("Error encoding data!");
        assert_eq!(encoded_str, expected_str);
    }

    #[test]
    fn base64_decode_test() {
        let encoded_str : String = "TWFueSBoYW5kcyBtYWtlIGxpZ2h0IHdvcmsu".to_string();
        let expected_data : String = "Many hands make light work.".to_string();

        let decoded_data = UniversalCallback::decode_base64(&encoded_str).expect("Error decoding data!");

        assert_eq!(decoded_data.len(), expected_data.len());
        for (i, c) in expected_data.chars().enumerate() {
            assert_eq!(decoded_data[i], c as u8);
        }
    }

    #[test]
    fn universal_callback_encode_decode_test() {
        let callback1 = UniversalCallback{
            var1: 0x1234567890abcdef,
            var2: 0xba,
            var3: 0xec,
            var4: 0,
        };
        let str1 = callback1.to_base64().expect("Error encoding callback!");
        let callback1_decoded = UniversalCallback::from_base64(&str1).expect("Error decoding callback!");

        assert_eq!(callback1.var1, callback1_decoded.var1);
        assert_eq!(callback1.var2, callback1_decoded.var2);
        assert_eq!(callback1.var3, callback1_decoded.var3);
        assert_eq!(callback1.var4, callback1_decoded.var4);

        let callback2 = UniversalCallback{
            var1: 0x1234567890abcdef,
            var2: 0xba,
            var3: 0xec,
            var4: 0xfedcba0987654321,
        };
        let str2 = callback2.to_base64().expect("Error encoding callback!");
        let callback2_decoded = UniversalCallback::from_base64(&str2).expect("Error decoding callback!");

        assert_eq!(callback2.var1, callback2_decoded.var1);
        assert_eq!(callback2.var2, callback2_decoded.var2);
        assert_eq!(callback2.var3, callback2_decoded.var3);
        assert_eq!(callback2.var4, callback2_decoded.var4);
    }

    #[test]
    fn experiments() {
        let test_value = -535_i64;
        let container = test_value as u64;
        let extracted = container as i64;
        assert_eq!(test_value, extracted);
    }
}
