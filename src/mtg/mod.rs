pub mod types;
pub mod menus;

use std::{cmp::max, collections::HashMap, str::FromStr, env};
use chrono::{Datelike, DateTime, Days, FixedOffset, Local, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use rand::seq::SliceRandom;
use teloxide::types::{ChatId, MessageId, UserId, PollAnswer};
use rusqlite::{Connection, Error, OptionalExtension, Row};

use self::types::GameFormat;

fn get_group_table_counts(player_count: usize) -> (usize, usize, usize) {
    let mut tables_5 = 0_usize;
    let mut tables_4 = 0_usize;
    let mut tables_3 = 0_usize;

    if player_count % 4 == 0 {
        tables_4 = player_count / 4;
    } else if player_count % 3 == 0 {
        tables_3 = player_count / 3;
    } else if player_count % 4 == 3 {
        tables_4 = player_count / 4;
        tables_3 = 1;
    } else if player_count % 3 == 1 {
        tables_3 = player_count / 3 - 1;
        tables_4 = 1;
    } else if player_count % 5 == 0 || player_count % 5 == 4 {
        tables_5 = player_count / 5;
        if player_count % 5 == 4 {
            tables_4 = 1;
        }
    } else {
        tables_3 = player_count / 3 - 1;
        tables_5 = 1;
    }

    (tables_3, tables_4, tables_5)
}

fn get_player_in_sitting(registration: &types::GameRegistration) -> String {
    format!("{}{}",
        match &registration.display_name {
            Some(display_name) => display_name,
            None => "ERR:NAME"
        },
        match &registration.telegram_tag {
            Some(tag) => format!(" ({})", tag),
            None => "".to_string()
        }
    )
}

pub fn create_sittings_groups(game_registrations: Vec<types::GameRegistration>, game_id: i64) -> Result<String, String> {
    let player_count = game_registrations.len();

    if player_count < 3 {
        return Err("На игру записано слишком мало игроков!".to_string());
    }

    let mut random_order: Vec<usize> = (0..player_count).collect();
    random_order.shuffle(&mut rand::thread_rng());

    let mut res = format!("Рассадка для игры № {}:\n", game_id);
    let mut table_counter = 1_usize;
    let mut player_counter = 0_usize;

    let (t3, t4, t5) = get_group_table_counts(player_count);
    for _ in 0..t3 {
        res += format!("=== Стол {} ===\n{}\n{}\n{}\n\n",
            table_counter,
            get_player_in_sitting(&game_registrations[random_order[player_counter + 0]]),
            get_player_in_sitting(&game_registrations[random_order[player_counter + 1]]),
            get_player_in_sitting(&game_registrations[random_order[player_counter + 2]]),
        ).as_str();
        table_counter += 1;
        player_counter += 3;
    }

    for _ in 0..t4 {
        res += format!("=== Стол {} ===\n{}\n{}\n{}\n{}\n\n",
           table_counter,
           get_player_in_sitting(&game_registrations[random_order[player_counter + 0]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 1]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 2]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 3]]),
        ).as_str();
        table_counter += 1;
        player_counter += 4;
    }

    for _ in 0..t5 {
        res += format!("=== Стол {} ===\n{}\n{}\n{}\n{}\n{}\n\n",
           table_counter,
           get_player_in_sitting(&game_registrations[random_order[player_counter + 0]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 1]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 2]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 3]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 4]]),
        ).as_str();
        table_counter += 1;
        player_counter += 5;
    }

    Ok(res)
}

pub fn create_sittings_pairs(game_registrations: Vec<types::GameRegistration>, game_id: i64) -> Result<String, String> {
    let player_count = game_registrations.len();

    if player_count < 3 {
        return Err("На игру записано слишком мало игроков!".to_string());
    }

    if player_count % 2 != 0 {
        return Err("На игру записано нечётное число игроков!".to_string());
    }

    let mut random_order: Vec<usize> = (0..player_count).collect();
    random_order.shuffle(&mut rand::thread_rng());

    let mut res = format!("Рассадка для игры № {}:\n", game_id);
    let mut table_counter = 1_usize;
    let mut player_counter = 0_usize;

    for _ in 0..(player_count / 2) {
        res += format!("=== Стол {} ===\n{}\n{}\n\n",
           table_counter,
           get_player_in_sitting(&game_registrations[random_order[player_counter + 0]]),
           get_player_in_sitting(&game_registrations[random_order[player_counter + 1]]),
        ).as_str();
        table_counter += 1;
        player_counter += 2;
    }

    Ok(res)
}

pub struct Manager {
    chat_id: ChatId,
    admin_id: UserId,
    default_place: String,
    offset: Option<FixedOffset>,
    db: Connection
}

impl Manager {
    fn update_parameters_cache(&mut self) -> Result<(), types::ErrorCode> {
        let res: (UserId, ChatId, String, i32) = self.db.query_row(
            "SELECT * FROM parameters WHERE id=1",
            (),
            |row: &Row| Ok((UserId(row.get_unwrap::<usize, i64>(1) as u64), ChatId(row.get_unwrap(2)), row.get_unwrap(3), row.get_unwrap(4)))
        ).expect("Failed querying parameters database");

        self.admin_id = res.0;
        self.chat_id = res.1;
        self.default_place = res.2;

        match FixedOffset::east_opt(res.3) {
            Some(offset) => self.offset = Some(offset),
            None => return Err(types::ErrorCode::InternalError),
        }

        if self.admin_id.0 == 0 || self.chat_id.0 == 0 || self.default_place.is_empty() {
            Err(types::ErrorCode::Uninitialized)
        } else {
            Ok(())
        }
    }

    pub fn get_chat_id(&mut self) -> Result<ChatId, types::ErrorCode> {
        if self.chat_id.0 == 0 {
            let res = self.update_parameters_cache();
            if res.is_err() && self.chat_id.0 == 0 {
                return Err(res.err().unwrap());
            }
        }
        Ok(self.chat_id)
    }

    pub fn set_chat_id(&mut self, chat_id: ChatId) {
        self.db.execute(
            "UPDATE parameters SET chat_id=?1 WHERE id=1",
            (chat_id.0,)
        ).expect("Failed to update parameters database");
        self.chat_id = chat_id;
    }

    pub fn get_admin_id(&mut self) -> Result<UserId, types::ErrorCode> {
        if self.admin_id.0 == 0 {
            let res = self.update_parameters_cache();
            if res.is_err() && self.admin_id.0 == 0 {
                return Err(res.err().unwrap());
            }
        }
        Ok(self.admin_id)
    }

    pub fn set_default_place(&mut self, default_place: String) {
        self.db.execute(
            "UPDATE parameters SET default_place=?1 WHERE id=1",
            (default_place.clone(),)
        ).expect("");
        self.default_place = default_place;
    }

    pub fn get_default_place(&mut self) -> Result<String, types::ErrorCode> {
        if self.default_place.is_empty() {
            let res = self.update_parameters_cache();
            if res.is_err() && self.default_place.is_empty() {
                return Err(res.err().unwrap());
            }
        }
        Ok(self.default_place.clone())
    }

    pub fn get_offset(&mut self) -> FixedOffset {
        if self.offset.is_none() {
            let res = self.update_parameters_cache();
            if res.is_err() && self.offset.is_none() {
                log::error!("Failed retrieving offset from database!");
                return *Local::now().offset();
            }
        }
        self.offset.unwrap()
    }

    pub fn get_links(&mut self) -> Vec<(String, String)> {
        let mut res : Vec<(String, String)> = vec![];

        let mut links_statement = self.db.prepare(
            "SELECT text, link FROM links"
        ).expect("Failed preparing links statement!");
        let link_infos = links_statement.query_map([], |row: &Row| Ok((row.get_unwrap::<usize, String>(0), row.get_unwrap::<usize, String>(1)))).expect("Failed querying links table!");

        for link_info_res in link_infos {
            let link_info = link_info_res.unwrap();
            res.push(link_info);
        }

        res
    }

    pub fn edit_player_name(&self, player_id: i64, new_name: String) -> bool {
        self.db.execute(
            "UPDATE players SET display_name=?2 WHERE id=?1",
            (player_id, new_name)
        ).expect("Failed updating player name!") != 0
    }

    pub fn edit_player_max_games_in_week(&self, player_id: i64, new_max_games: u8) -> bool {
        self.db.execute(
            "UPDATE players SET max_games_in_week=?2 WHERE id=?1",
            (player_id, new_max_games)
        ).expect("Failed updating player name!") != 0
    }

    pub fn add_player(&self, telegram_id: UserId, display_name: &str) -> Result<i64, types::ErrorCode> {
        let player_exists = self.db.query_row(
            "SELECT * FROM players WHERE telegram_id=?1",
            (telegram_id.0,),
            |_| Ok(())
        ).optional().expect("Failed to check if player is already in database!").is_some();
        if player_exists {
            return Err(types::ErrorCode::AlreadyExists);
        }

        self.db.execute(
            "INSERT INTO players (display_name, telegram_id, max_games_in_week, used_badge, is_admin) VALUES (?1, ?2, ?3, ?4, 0)",
            (display_name, telegram_id.0, 1, "".to_string())
        ).expect("Failed to add player to database!");
        Ok(self.db.last_insert_rowid())
    }

    pub fn delete_player(&self, player_id: i64) -> bool {
        // Clear achievement claims
        let mut achievement_claims_statement = self.db.prepare(
            "SELECT id FROM achievement_claims WHERE player_id=?1"
        ).expect("Failed preparing achievement claims statement");

        let claim_ids = achievement_claims_statement.query_map(
            (player_id,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).expect("Failed querying achievement claims database!");

        for claim_id_res in claim_ids {
            let claim_id = claim_id_res.unwrap();
            self.db.execute(
                "DELETE FROM achievement_claim_votes WHERE claim_id=?1",
                (claim_id,)
            ).expect("Failed deleting achievement claim from database!");
        }

        self.db.execute(
            "DELETE FROM achievement_claims WHERE plater_id=?1",
            (player_id,)
        ).expect("Failed clearing player achievement claims!");

        // Clear achievements
        self.db.execute(
            "DELETE FROM player_achievements WHERE player_id=?1",
            (player_id,)
        ).expect("Failed clearing player achievements!");

        // Clear game registrations
        self.db.execute(
            "DELETE FROM registrations WHERE player_id=?1",
            (player_id,)
        ).expect("Failed clearing player registrations!");

        // Clear created games
        let mut created_games_statement = self.db.prepare(
            "SELECT id FROM games WHERE creator_id=?1"
        ).expect("Failed preparing created games statement!");

        let created_game_ids = created_games_statement.query_map(
            (player_id,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).expect("Failed querying games database!");

        for game_id_res in created_game_ids {
            let game_id = game_id_res.unwrap();
            self.delete_game(game_id);
        }

        // Clear created polls
        self.db.execute(
            "DELETE FROM polls WHERE creator_id=?1",
            (player_id,)
        ).expect("Failed clearing player created polls!");

        // Clear subscriptions
        self.db.execute(
            "DELETE FROM subscriptions WHERE player_id=?1",
            (player_id,)
        ).expect("Failed clearing player subscriptions!");

        match self.db.execute(
            "DELETE FROM players WHERE id=?1",
            (player_id,)
        ) {
            Ok(modified_rows) => modified_rows != 0,
            Err(err) => {
                log::error!("Failed deleting player with id {} from database with error {}!", player_id, err);
                false
            }
        }
    }

    pub fn new() -> Manager {
        log::info!("Initializing bot manager...");

        if !std::path::Path::new("./data").exists() {
            std::fs::create_dir("./data").expect("Failed to create data directory!");
        }

        log::info!("Establishing database connection...");
        let db = Connection::open("./data/bot.db").expect("Failed to open bot database connection!");
        log::info!("Database connection established");

        log::info!("Verifying database...");
        let res = db.query_row("SELECT name FROM sqlite_master WHERE type='table' AND name='players'", (), |_| Ok(())).optional().expect("Failed to check players database!");
        if res.is_none() {
            log::info!("Database does not contain required table. Initializing...");
            log::info!("Initializing players table...");
            db.execute(
                "CREATE TABLE players (
                        id                INTEGER PRIMARY KEY,
                        display_name      TEXT NOT NULL,
                        telegram_id       INTEGER NOT NULL,
                        max_games_in_week INTEGER NOT NULL,
                        used_badge        TEXT NOT NULL,
                        is_admin          INTEGER NOT NULL
                )",
                (),
            ).expect("Failed to initialize players table!");
            log::info!("Players table initialized");

            log::info!("Initializing achievements table...");
            db.execute(
                "CREATE TABLE achievements (
                        id          INTEGER PRIMARY KEY,
                        badge       TEXT NOT NULL,
                        name        TEXT NOT NULL,
                        description TEXT NOT NULL,
                        achievable  INTEGER NOT NULL
                )",
                (),
            ).expect("Failed to initialize achievements table!");
            log::info!("Achievements table initialized");

            log::info!("Initializing player achievements table...");
            db.execute(
                "CREATE TABLE player_achievements (
                        id             INTEGER PRIMARY KEY,
                        player_id      INTEGER NOT NULL,
                        achievement_id INTEGER NOT NULL,
                        CONSTRAINT FK_player_id FOREIGN KEY(player_id) REFERENCES players(id),
                        CONSTRAINT FK_achievement_id FOREIGN KEY(achievement_id) REFERENCES achievements(id)
                )",
                (),
            ).expect("Failed to initialize player achievements table!");
            log::info!("Player achievements table initialized");

            log::info!("Initializing achievement claims table...");
            db.execute(
                "CREATE TABLE achievement_claims (
                        id             INTEGER PRIMARY KEY,
                        player_id      INTEGER NOT NULL,
                        achievement_id INTEGER NOT NULL,
                        message_id     INTEGER NOT NULL,
                        CONSTRAINT FK_player_id FOREIGN KEY(player_id) REFERENCES players(id),
                        CONSTRAINT FK_achievement_id FOREIGN KEY(achievement_id) REFERENCES achievements(id)
                )",
                (),
            ).expect("Failed to initialize achievement claims table!");
            log::info!("Achievement claims initialized");

            log::info!("Initializing achievement claim votes table...");
            db.execute(
                "CREATE TABLE achievement_claim_votes (
                        id          INTEGER PRIMARY KEY,
                        claim_id    INTEGER NOT NULL,
                        telegram_id INTEGER NOT NULL,
                        vote        INTEGER NOT NULL,
                        CONSTRAINT FK_claim_id FOREIGN KEY(claim_id) REFERENCES achievement_claims(id)
                )",
                (),
            ).expect("Failed to initialize achievement claim votes table!");
            log::info!("Achievement claim votes initialized");

            log::info!("Initializing games table...");
            db.execute(
                "CREATE TABLE games (
                        id                    INTEGER PRIMARY KEY,
                        message_id            INTEGER NOT NULL,
                        creator_id            INTEGER NOT NULL,
                        place                 TEXT NOT NULL,
                        description           TEXT NOT NULL,
                        date_time             DATETIME NOT NULL,
                        required_player_count INTEGER NOT NULL,
                        format                INTEGER NOT NULL,
                        participation_fee     INTEGER NOT NULL,
                        mandatory_fee         INTEGER NOT NULL,
                        CONSTRAINT FK_creator_id FOREIGN KEY(creator_id) REFERENCES players(id)
                )",
                (),
            ).expect("Failed to initialize games table!");
            log::info!("Games database initialized");

            log::info!("Initializing registration table...");
            db.execute(
                "CREATE TABLE registrations (
                        id        INTEGER PRIMARY KEY,
                        game_id   INTEGER NOT NULL,
                        player_id INTEGER NOT NULL,
                        is_buying INTEGER NOT NULL,
                        CONSTRAINT FK_game_id FOREIGN KEY(game_id) REFERENCES games(id),
                        CONSTRAINT FK_player_id FOREIGN KEY(player_id) REFERENCES players(id)
                )",
                (),
            ).expect("Failed to initialize registration table!");
            log::info!("Registration table initialized");

            log::info!("Initializing anonymous registrations table...");
            db.execute(
                "CREATE TABLE anonymous_registrations (
                        id           INTEGER PRIMARY KEY,
                        game_id      INTEGER NOT NULL,
                        telegram_id  INTEGER NOT NULL,
                        is_buying    INTEGER NOT NULL,
                        CONSTRAINT FK_game_id FOREIGN KEY(game_id) REFERENCES games(id)
                )",
                (),
            ).expect("Failed to initialize anonymous registration table!");
            log::info!("Anonymous registration table initialized");

            log::info!("Initializing super anonymous registrations table...");
            db.execute(
                "CREATE TABLE super_anonymous_registrations (
                        id           INTEGER PRIMARY KEY,
                        game_id      INTEGER NOT NULL,
                        display_name TEXT NOT NULL,
                        CONSTRAINT FK_game_id FOREIGN KEY(game_id) REFERENCES games(id)
                )",
                (),
            ).expect("Failed to initialize anonymous registration table!");
            log::info!("Super anonymous registration table initialized");

            log::info!("Initializing polls table...");
            db.execute(
                "CREATE TABLE polls (
                        id          INTEGER PRIMARY KEY,
                        creator_id  INTEGER NOT NULL,
                        question    TEXT NOT NULL,
                        description TEXT NOT NULL,
                        place       TEXT NOT NULL,
                        game_format INTEGER NOT NULL,
                        game_time   TIME NOT NULL,
                        start_time  TIME NOT NULL,
                        start_day   INTEGER NOT NULL,
                        end_time    TIME NOT NULL,
                        end_day     INTEGER NOT NULL,
                        message_id  INTEGER NOT NULL,
                        poll_id     TEXT NOT NULL,
                        next_monday DATE DEFAULT NULL,
                        CONSTRAINT FK_creator_id FOREIGN KEY(creator_id) REFERENCES players(id)
                )",
                (),
            ).expect("Failed to initialize polls table!");
            log::info!("Polls table initialized");

            log::info!("Initializing poll results table...");
            db.execute(
                "CREATE TABLE poll_results (
                        id          INTEGER PRIMARY KEY,
                        poll_id     INTEGER NOT NULL,
                        telegram_id INTEGER NOT NULL,
                        vote        TEXT NOT NULL,
                        CONSTRAINT FK_poll_id FOREIGN KEY(poll_id) REFERENCES polls(id)
                )",
                (),
            ).expect("Failed to initialize poll results table!");
            log::info!("Poll results table initialized");

            log::info!("Initializing subscriptions table...");
            db.execute(
                "CREATE TABLE subscriptions (
                        id         INTEGER PRIMARY KEY,
                        player_id  INTEGER NOT NULL UNIQUE,
                        event_mask INTEGER NOT NULL,
                        CONSTRAINT FK_player_id FOREIGN KEY(player_id) REFERENCES players(id)
                )",
                ()
            ).expect("Failed to initialize subscriptions table!");
            log::info!("Subscriptions table initialized");

            log::info!("Initializing pending notifications table...");
            db.execute(
                "CREATE TABLE pending_notifications (
                        id          INTEGER PRIMARY KEY,
                        telegram_id INTEGER NOT NULL,
                        text        TEXT NOT NULL
                )",
                ()
            ).expect("Failed to initialize pending_notifications table!");
            log::info!("Pending notifications table initialized");

            log::info!("Initializing links table...");
            db.execute(
                "CREATE TABLE links (
                        id   INTEGER PRIMARY KEY,
                        text TEXT NOT NULL,
                        link TEXT NOT NULL
                )",
                (),
            ).expect("Failed to initialize links table!");
            log::info!("Links table initialized");

            log::info!("Initializing parameters table...");
            db.execute(
                "CREATE TABLE parameters (
                        id                INTEGER PRIMARY KEY,
                        admin_telegram_id INTEGER NOT NULL,
                        chat_id           INTEGER NOT NULL,
                        default_place     TEXT NOT NULL,
                        utc_offset_sec    INTEGER NOT NULL,
                )",
                (),
            ).expect("Failed to initialize parameters table!");

            let utc_offset = Local::now().offset().local_minus_utc();
            log::info!("Local UTC offset: {}", utc_offset);

            let admin_tg = match env::var("MTG_BOT_INIT_ADMIN") {
                Ok(val) => val,
                Err(_) => "".to_string(),
            };

            db.execute(
                "INSERT INTO parameters (admin_telegram_id, chat_id, default_place, utc_offset_sec, stats_table_link) VALUES(?1, 0, '', ?2, '')",
                (admin_tg, utc_offset)
            ).expect("Failed to set default parameters");
            log::info!("Parameters table initialized");
        } else {
            log::info!("Database verified");
        }

        log::info!("Bot manager initialized");
        let mut manager = Manager {
            chat_id: ChatId(0),
            admin_id: UserId(0),
            default_place: "".to_string(),
            offset: None,
            db
        };

        let _ = manager.update_parameters_cache();
        manager
    }

    fn row_to_player(row: &Row) -> Result<types::Player, Error> {
        let player = types::Player {
            id: row.get(0).unwrap(),
            display_name: row.get(1).unwrap(),
            telegram_id: UserId(row.get(2).unwrap()),
            preferences: types::Preferences {
                max_games_in_week: row.get(3).unwrap(),
                used_badge: row.get(4).unwrap()
            },
            achievements: vec![],
            subscription: types::Subscription {
                new_std: false,
                new_edh: false,
                new_std_draft: false,
                new_edh_draft: false,
            },
            is_admin: row.get::<usize, i64>(5).unwrap() == 1
        };
        Ok(player)
    }

    pub fn get_achievement(&self, achievement_id: i64, user_id: i64) -> types::Achievement {
        self.db.query_row(
            "SELECT achievements.id, achievements.badge, achievements.name, achievements.description, ((CASE WHEN (SELECT COUNT(1) FROM player_achievements WHERE player_achievements.player_id=?2 AND player_achievements.achievement_id=?1) == 0 THEN 1 ELSE 0 END) AND achievements.achievable) as achievable FROM achievements WHERE achievements.id=?1",
            (achievement_id, user_id),
            |row: &Row| {
                Ok(types::Achievement{
                    id: row.get_unwrap(0),
                    badge: row.get_unwrap(1),
                    name: row.get_unwrap(2),
                    description: row.get_unwrap(3),
                    achievable: row.get_unwrap::<usize, i64>(4) == 1,
                })
            }
        ).expect("Failed querying achievement database!")
    }

    pub fn get_achievement_badge(&self, achievement_id: i64) -> String {
        self.db.query_row(
            "SELECT badge FROM achievements WHERE id=?1",
            (achievement_id,),
            |row: &Row| Ok(row.get_unwrap::<usize, String>(0))
        ).expect("Failed getting achievement badge")
    }

    pub fn set_player_badge(&self, player_id: i64, badge: String) {
        self.db.execute(
            "UPDATE players SET used_badge=?2 WHERE id=?1",
            (player_id, badge)
        ).expect("Failed updating player badge!");
    }

    fn get_player_achievements(&self, player: &mut types::Player) {
        let mut statement = self.db.prepare(
            format!("SELECT achievement_id FROM player_achievements WHERE player_id={}", player.id).as_str(),
        ).expect("Failed to prepare statement!");
        let achievement_ids = statement.query_map((), |row| Ok(row.get::<usize, i64>(0).unwrap())).expect("Failed to execute statement!");
        for achievement_id in achievement_ids {
            player.achievements.push(achievement_id.unwrap());
        }
    }

    fn get_player_subscriptions(&self, player: &mut types::Player) {
        let res = self.db.query_row(
            "SELECT event_mask FROM subscriptions WHERE player_id=?1",
            (player.id,),
            |row: &Row| Ok(types::Subscription::from_mask(row.get_unwrap::<usize, u64>(0)))
        ).optional().expect("Failed to get player subscriptions");
        match res {
            Some(sub) => {
                player.subscription = sub;
            }
            None => {
                // Nothing to update
            }
        }
    }

    pub fn is_admin(&self, player_id: i64) -> bool {
        self.db.query_row(
            "SELECT is_admin FROM players WHERE id=?1",
            (player_id,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0) == 1)
        ).expect("Failed querying players database!")
    }

    pub fn get_player(&self, player_id: i64, get_achievements: bool, get_subscriptions: bool) -> Result<types::Player, types::ErrorCode> {
        let res = self.db.query_row(
            format!("SELECT * FROM players WHERE id={}", player_id).as_str(),
            (),
            Manager::row_to_player
        ).optional().expect("Failed to query bot database!");
        if res.is_none() {
            return Err(types::ErrorCode::NotFound);
        }
        let mut player = res.unwrap();
        if get_achievements {
            self.get_player_achievements(&mut player);
        }
        if get_subscriptions {
            self.get_player_subscriptions(&mut player);
        }
        Ok(player)
    }

    pub fn get_player_id(&self, telegram_id: UserId) -> Result<i64, types::ErrorCode> {
        let res = self.db.query_row(
            "SELECT id FROM players WHERE telegram_id=?1",
            (telegram_id.0,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).optional().expect("Failed to query bot database!");
        match res {
            Some(player_id) => Ok(player_id),
            _ => Err(types::ErrorCode::NotFound)
        }
    }

    pub fn is_player_joined(&self, player_id: i64, game_id: i64) -> bool {
        let res = self.db.query_row(
            "SELECT id FROM registrations WHERE game_id=?1 AND player_id=?2",
            (game_id, player_id),
            |_row: &Row| Ok(())
        ).optional().expect("Failed to query bot database!");
        match res {
            Some(()) => true,
            _ => false
        }
    }

    pub fn is_anon_player_joined(&self, telegram_id: UserId, game_id: i64) -> bool {
        let res = self.db.query_row(
            "SELECT id FROM anonymous_registrations WHERE game_id=?1 AND telegram_id=?2",
            (game_id, telegram_id.0),
            |_row: &Row| Ok(())
        ).optional().expect("Failed to query bot database!");
        match res {
            Some(()) => true,
            _ => false
        }
    }

    pub fn join_game(&self, game_id: i64, telegram_id: UserId) -> bool {
        match self.get_player_id(telegram_id) {
            Ok(player_id) => {
                if self.is_player_joined(player_id, game_id) {
                    false
                } else {
                    let reg_id = self.db.query_row(
                        "SELECT id FROM anonymous_registrations WHERE telegram_id=?1 AND game_id=?2",
                        (telegram_id.0, game_id),
                        |row: &Row| Ok(row.get::<usize, i64>(0).unwrap())
                    ).optional().expect("Failed querying registrations database!");

                    match reg_id {
                        Some(reg_id) => {
                            self.db.execute(
                                "DELETE FROM anonymous_registrations WHERE id=?1",
                                (reg_id,)
                            ).expect("Failed removing old anonymous registration!");
                        }
                        _ => {
                            // Nothing to do
                        }
                    }

                    self.db.execute(
                        "INSERT INTO registrations (game_id, player_id, is_buying) VALUES (?1, ?2, 0)",
                        (game_id, player_id)
                    ).expect("Failed to add game registration!");
                    true
                }
            }
            Err(_) => {
                if self.is_anon_player_joined(telegram_id, game_id) {
                    false
                } else {
                    self.db.execute(
                        "INSERT INTO anonymous_registrations (game_id, telegram_id, is_buying) VALUES (?1, ?2, 0)",
                        (game_id, telegram_id.0)
                    ).expect("Failed to add game registration!");
                    true
                }
            }
        }
    }

    pub fn toggle_buyout(&self, game_id: i64, telegram_id: UserId) -> bool {
        match self.get_player_id(telegram_id) {
            Ok(player_id) => {
                if self.is_player_joined(player_id, game_id) {
                    self.db.execute(
                        "UPDATE registrations SET is_buying = CASE is_buying WHEN 0 THEN 1 WHEN 1 THEN 0 END WHERE game_id=?1 AND player_id=?2",
                        (game_id, player_id)
                    ).expect("Failed to remove game registration!");
                    true
                } else {
                    false
                }
            }
            Err(_) => {
                if self.is_anon_player_joined(telegram_id, game_id) {
                    self.db.execute(
                        "UPDATE anonymous_registrations SET is_buying = CASE is_buying WHEN 0 THEN 1 WHEN 1 THEN 0 END WHERE game_id=?1 AND telegram_id=?2",
                        (game_id, telegram_id.0)
                    ).expect("Failed to remove game registration!");
                    true
                } else {
                    false
                }
            }
        }
    }

    pub fn quit_game(&self, game_id: i64, telegram_id: UserId) -> bool {
        match self.get_player_id(telegram_id) {
            Ok(player_id) => {
                if self.is_player_joined(player_id, game_id) {
                    self.db.execute(
                        "DELETE FROM registrations WHERE game_id=?1 AND player_id=?2",
                        (game_id, player_id)
                    ).expect("Failed to remove game registration!");
                    true
                } else {
                    false
                }
            }
            Err(_) => {
                if self.is_anon_player_joined(telegram_id, game_id) {
                    self.db.execute(
                        "DELETE FROM anonymous_registrations WHERE game_id=?1 AND telegram_id=?2",
                        (game_id, telegram_id.0)
                    ).expect("Failed to remove game registration!");
                    true
                } else {
                    false
                }
            }
        }
    }

    pub fn add_game(
        &self,
        game: types::Game,
    ) -> Result<types::Game, types::ErrorCode> {
        let event_type = match game.format {
            types::GameFormat::Standard => types::EventType::NewStd,
            types::GameFormat::Commander => types::EventType::NewEdh,
            types::GameFormat::StandardDraft => types::EventType::NewStdDraft,
            types::GameFormat::CommanderDraft => types::EventType::NewEdhDraft,
        };
        let msg = format!("Создана новая игра {} от {}\n{}",
            match game.format {
                types::GameFormat::Commander => "EDH",
                types::GameFormat::Standard => "стандарт",
                types::GameFormat::CommanderDraft => "EDH Драфт",
                types::GameFormat::StandardDraft => "Драфт"
            },
            game.date_time.format("%d.%m.%Y, %H:%M"),
            game.description,
        );
        self.defer_event_notification(types::Event { event_type, desc: msg });

        self.db.execute(
            "INSERT INTO games (message_id, creator_id, place, description, date_time, required_player_count, format, participation_fee, mandatory_fee) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)",
            (game.message_id.0, game.creator_id, &game.place, &game.description, game.date_time, game.required_player_count, game.format, game.participation_fee, if game.mandatory_fee {1} else {0})
        ).expect("Failed to add game to database!");
        let mut game_with_id = game;
        game_with_id.id = self.db.last_insert_rowid();

        Ok(game_with_id)
    }

    pub fn game_exists(&self, game_id: i64) -> bool {
        let res = self.db.query_row(
            "SELECT id FROM games WHERE id=?1",
            (game_id,),
            |_row: &Row| Ok(())
        ).optional().expect("Failed querying bot database!");
        match res {
            Some(()) => true,
            None => false
        }
    }

    pub fn get_game_registrations(&self, game_id: i64) -> Result<Vec<types::GameRegistration>, types::ErrorCode> {
        let mut res : Vec<types::GameRegistration> = vec![];

        let mut players_query = self.db.prepare("SELECT id, player_id, is_buying FROM registrations WHERE game_id=?1").expect("Failed preparing bot database query!");
        let mut anon_players_query = self.db.prepare("SELECT id, telegram_id, is_buying FROM anonymous_registrations WHERE game_id=?1").expect("Failed preparing bot database query!");
        let mut super_anon_players_query = self.db.prepare("SELECT id, display_name FROM super_anonymous_registrations WHERE game_id=?1").expect("Failed preparing bot database query!");

        let player_infos = players_query.query_map((game_id,), |row: &Row| Ok((row.get::<usize, i64>(0).unwrap(), row.get::<usize, i64>(1).unwrap(), row.get::<usize, i64>(2).unwrap() == 1))).expect("Failed querying bot database!");
        for player_info in player_infos {
            let info = player_info.unwrap();
            let player = self.get_player(info.1, false, false)?;
            let badge = if player.preferences.used_badge.is_empty() {
                "".to_string()
            } else {
                format!("[{}] ", player.preferences.used_badge)
            };
            res.push(types::GameRegistration{
                reg_id: info.0,
                reg_type: types::RegistrationType::Normal,
                display_name: Some(format!("{}{}", badge, player.display_name)),
                telegram_id: Some(player.telegram_id),
                telegram_tag: None,
                is_buying: info.2,
            });
        }

        let anon_player_infos = anon_players_query.query_map((game_id,), |row: &Row| Ok((row.get::<usize, i64>(0).unwrap(), UserId(row.get::<usize, u64>(1).unwrap()), row.get::<usize, i64>(2).unwrap() == 1))).expect("Failed querying bot database!");
        for anon_player_info in anon_player_infos {
            let info = anon_player_info.unwrap();
            res.push(types::GameRegistration{
                reg_id: info.0,
                reg_type: types::RegistrationType::TelegramId,
                display_name: None,
                telegram_id: Some(info.1),
                telegram_tag: None,
                is_buying: info.2,
            });
        }

        let super_anon_player_infos = super_anon_players_query.query_map((game_id,), |row: &Row| Ok((row.get::<usize, i64>(0).unwrap(), row.get::<usize, String>(1).unwrap()))).expect("Failed querying bot database!");
        for super_anon_player_info in super_anon_player_infos {
            let info = super_anon_player_info.unwrap();
            res.push(types::GameRegistration{
                reg_id: info.0,
                reg_type: types::RegistrationType::NameOnly,
                display_name: Some(info.1),
                telegram_id: None,
                telegram_tag: None,
                is_buying: false,
            });
        }

        Ok(res)
    }

    pub fn get_game(&self, game_id: i64) -> Result<types::Game, types::ErrorCode>{
        let game_registrations = self.get_game_registrations(game_id)?;

        let res = self.db.query_row(
            "SELECT * FROM games WHERE id=?1",
            (game_id,),
            |row: &Row| Ok(types::Game{
                id: row.get::<usize, i64>(0).unwrap(),
                message_id: MessageId(row.get::<usize, i32>(1).unwrap()),
                creator_id: row.get::<usize, i64>(2).unwrap(),
                place: row.get::<usize, String>(3).unwrap(),
                description: row.get::<usize, String>(4).unwrap(),
                date_time: row.get::<usize, DateTime<FixedOffset>>(5).unwrap(),
                required_player_count: row.get::<usize, u32>(6).unwrap(),
                format: row.get::<usize, types::GameFormat>(7).unwrap(),
                participation_fee: row.get::<usize, u32>(8).unwrap(),
                mandatory_fee: row.get::<usize, i64>(9).unwrap() == 1,
                registrations: game_registrations,
            })
        ).optional().expect("Failed to query bot database!");
        match res {
            Some(game) => {
                Ok(game)
            }
            _ => {
                Err(types::ErrorCode::NotFound)
            }
        }
    }

    pub fn edit_game_place(&self, game_id: i64, new_place: &str) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET place=?1 WHERE id=?2",
            (new_place, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn edit_game_desc(&self, game_id: i64, new_desc: &str) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET description=?1 WHERE id=?2",
            (new_desc, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn edit_game_date_time(&self, game_id: i64, new_date_time: DateTime<FixedOffset>) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET date_time=?1 WHERE id=?2",
            (new_date_time, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn edit_game_required_player_count(&self, game_id: i64, new_required_player_count: u32) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET required_player_count=?1 WHERE id=?2",
            (new_required_player_count, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn edit_game_participation_fee(&self, game_id: i64, new_participation_fee: u32) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET participation_fee=?1 WHERE id=?2",
            (new_participation_fee, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn edit_game_mandatory_fee(&self, game_id: i64, new_mandatory_fee: bool) -> bool {
        let modified_rows = self.db.execute(
            "UPDATE games SET mandatory_fee=?1 WHERE id=?2",
            (if new_mandatory_fee {1} else {0}, game_id)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn add_super_anon_player(&self, game_id: i64, display_name: &str) -> bool {
        let modified_rows = self.db.execute(
            "INSERT INTO super_anonymous_registrations (game_id, display_name) VALUES (?1, ?2)",
            (game_id, display_name)
        ).expect("Failed updating games database!");
        modified_rows != 0
    }

    pub fn remove_player(&self, game_id: i64, reg: types::GameRegistration) -> bool {
        match reg.reg_type {
            types::RegistrationType::Normal => {
                self.db.execute(
                    "DELETE FROM registrations WHERE id=?1 AND game_id=?2",
                    (reg.reg_id, game_id)
                ).expect("") != 0
            },
            types::RegistrationType::TelegramId => {
                self.db.execute(
                    "DELETE FROM anonymous_registrations WHERE id=?1 AND game_id=?2",
                    (reg.reg_id, game_id)
                ).expect("") != 0
            },
            types::RegistrationType::NameOnly => {
                self.db.execute(
                    "DELETE FROM super_anonymous_registrations WHERE id=?1 AND game_id=?2",
                    (reg.reg_id, game_id)
                ).expect("") != 0
            },
        }
    }

    pub fn get_player_achievement_infos(&self, user_id: i64, page: u64) -> Vec<(i64, String)> {
        let mut res: Vec<(i64, String)> = vec![];

        let offset = page * 10;
        let mut achievement_ids_statement = self.db.prepare(
            "SELECT achievement_id FROM player_achievements WHERE player_id=?1 ORDER BY id ASC LIMIT 10 OFFSET ?2"
        ).expect("Failed preparing player achievements database statement!");
        let achievement_ids_res = achievement_ids_statement.query_map(
            (user_id, offset),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).expect("Failed querying player achievements database!");
        for achievement_id_res in achievement_ids_res {
            let achievement_id = achievement_id_res.unwrap();
            res.push((
                achievement_id,
                self.db.query_row(
                    "SELECT badge, name FROM achievements WHERE id=?1",
                    (achievement_id, ),
                    |row: &Row| Ok(format!("{}| {}", row.get_unwrap::<usize, String>(0), row.get_unwrap::<usize, String>(1)))
                ).expect("Failed querying achievements database"))
            );
        }

        res
    }

    pub fn get_achievement_infos(&self, page: u64) -> Vec<(i64, String)> {
        let mut res: Vec<(i64, String)> = vec![];

        let offset = page * 10;
        let mut achievements_statement = self.db.prepare(
            "SELECT id, badge, name FROM achievements ORDER BY id ASC LIMIT 10 OFFSET ?1"
        ).expect("Failed preparing achievements database statement!");
        let achievements_res = achievements_statement.query_map(
            (offset,),
            |row: &Row| Ok((row.get_unwrap::<usize, i64>(0), format!("{}| {}", row.get_unwrap::<usize, String>(1), row.get_unwrap::<usize, String>(2))))
        ).expect("Failed querying achievements database!");

        for achievement_res in achievements_res {
            res.push(achievement_res.unwrap());
        }

        res
    }

    pub fn get_outdated_games(&mut self) -> Vec<i64> {
        let mut res: Vec<i64> = vec![];

        let today = Utc::now().with_timezone(&self.get_offset());

        let mut statement = self.db.prepare(
            "SELECT id FROM games WHERE julianday(?1) - julianday(date_time) > 2"
        ).expect("Failed preparing games database statement!");

        let query_res = statement.query_map(
            (today,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).expect("Failed querying games database!");

        for game_info in query_res {
            res.push(game_info.unwrap());
        }

        res
    }

    pub fn delete_game(&self, game_id: i64) -> (bool, MessageId) {
        let res = self.db.query_row(
            "SELECT message_id FROM games WHERE id=?1",
            (game_id,),
            |row: &Row| Ok(MessageId(row.get::<usize, i32>(0).unwrap()))
        ).optional().expect("Failed querying games database!");
        match res {
            Some(msg_id) => {
                self.db.execute(
                    "DELETE FROM registrations WHERE game_id=?1",
                    (game_id,)
                ).expect("Failed deleting game registrations from database!");
                self.db.execute(
                    "DELETE FROM anonymous_registrations WHERE game_id=?1",
                    (game_id,)
                ).expect("Failed deleting game registrations from database!");
                self.db.execute(
                    "DELETE FROM super_anonymous_registrations WHERE game_id=?1",
                    (game_id,)
                ).expect("Failed deleting game registrations from database!");
                let modified_rows = self.db.execute(
                    "DELETE FROM games WHERE id=?1",
                    (game_id,)
                ).expect("Failed deleting game from bot database");
                (modified_rows != 0, msg_id)
            },
            None => {
                (false, MessageId(0))
            }
        }
    }

    pub fn get_games_infos(&self, creator_id: i64) -> Vec<(i64, String)> {
        let mut res : Vec<(i64, String)> = vec![];
        if creator_id == 0 {
            let mut query = self.db.prepare(
                "SELECT id, date_time, format FROM games"
            ).expect("Failed preparing bot database query!");
            let game_infos = query.query_map(
                (),
                |row: &Row| Ok((row.get::<usize, i64>(0).unwrap(), row.get::<usize, DateTime<FixedOffset>>(1).unwrap(), row.get::<usize, types::GameFormat>(2).unwrap()))
            ).expect("Failed querying bot database!");
            for game_info in game_infos {
                let info = game_info.unwrap();
                let day_str = match info.1.weekday().number_from_monday() {
                    1 => " (Пн)".to_string(),
                    2 => " (Вт)".to_string(),
                    3 => " (Ср)".to_string(),
                    4 => " (Чт)".to_string(),
                    5 => " (Пт)".to_string(),
                    6 => " (Сб)".to_string(),
                    7 => " (Вс)".to_string(),
                    _ => "".to_string(),
                };
                res.push((info.0, format!("[{}] Игра {} от {}{}", info.0,
                    match info.2 {
                        types::GameFormat::Commander => "EDH",
                        types::GameFormat::Standard => "стандарт",
                        types::GameFormat::CommanderDraft => "EDH Драфт",
                        types::GameFormat::StandardDraft => "Драфт"
                    },
                    info.1.format("%d.%m.%Y, %H:%M"),
                    day_str))
                );
            }
        } else {
            let mut query = self.db.prepare(
                "SELECT id, date_time, format FROM games WHERE creator_id=?1"
            ).expect("Failed preparing bot database query!");
            let game_infos = query.query_map(
                (creator_id,),
                |row: &Row| Ok((row.get::<usize, i64>(0).unwrap(), row.get::<usize, DateTime<FixedOffset>>(1).unwrap(), row.get::<usize, types::GameFormat>(2).unwrap()))
            ).expect("Failed querying bot database!");
            for game_info in game_infos {
                let info = game_info.unwrap();
                let day_str = match info.1.weekday().number_from_monday() {
                    1 => " (Пн)".to_string(),
                    2 => " (Вт)".to_string(),
                    3 => " (Ср)".to_string(),
                    4 => " (Чт)".to_string(),
                    5 => " (Пт)".to_string(),
                    6 => " (Сб)".to_string(),
                    7 => " (Вс)".to_string(),
                    _ => "".to_string(),
                };
                res.push((info.0, format!("[{}] Игра {} от {}{}", info.0,
                    match info.2 {
                        types::GameFormat::Commander => "EDH",
                        types::GameFormat::Standard => "стандарт",
                        types::GameFormat::CommanderDraft => "EDH Драфт",
                        types::GameFormat::StandardDraft => "Драфт"
                    },
                    info.1.format("%d.%m.%Y, %H:%M"),
                    day_str))
                );
            }
        };
        res
    }

    pub fn schedule_poll(
        &self,
        user_id: i64,
        question: String,
        description: String,
        place: String,
        game_format: types::GameFormat,
        game_time: NaiveTime,
        start_time: NaiveTime,
        start_day: u8,
        end_time: NaiveTime,
        end_day: u8,
    ) {
        self.db.execute(
            "INSERT INTO polls (creator_id, question, description, place, game_format, game_time, start_time, start_day, end_time, end_day, message_id, poll_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, 0, '')",
            (user_id, question, description, place, game_format, game_time, start_time, start_day, end_time, end_day)
        ).expect("Failed adding poll to database!");
    }

    pub fn remove_poll(&self, poll_id: i64) {
        self.db.execute(
            "DELETE FROM polls WHERE id=?1",
            (poll_id,)
        ).expect("Failed removing poll from database!");
    }

    pub fn get_polls_to_send(&mut self) -> Vec<i64> {
        let mut res: Vec<i64> = vec![];

        let today = Utc::now().with_timezone(&self.get_offset());
        let week_day_num = today.weekday().num_days_from_monday() + 1;

        // 0.125 day = 3 hours
        let mut statement = self.db.prepare(
            "SELECT id FROM polls WHERE (message_id=0 AND poll_id='') AND ((start_day=?1 AND julianday(?2) - julianday(start_time) >= 0 AND julianday(?2) - julianday(start_time) <= 0.125) OR (start_day=(?1 + 5) % 7 + 1 AND julianday(?2, '+001 days') - julianday(start_time) <= 0.125))"
        ).expect("Failed to prepare statement!");

        let results = statement.query_map(
            (week_day_num, today.time()),
            |row: &Row| Ok(row.get::<usize, i64>(0).unwrap())
        ).expect("Failed to get polls to send from database!");

        for result in results {
            res.push(result.unwrap());
        }

        res
    }

    pub fn get_poll_infos(&self) -> Vec<(i64, String)> {
        let mut res: Vec<(i64, String)> = vec![];

        let mut statement = self.db.prepare(
            "SELECT id, game_format, start_time, start_day, end_time, end_day FROM polls"
        ).expect("Failed to prepate statement to extract polls information");

        let results = statement.query_map(
            [],
            |row: &Row| Ok((
                row.get_unwrap::<usize, i64>(0),
                row.get_unwrap::<usize, types::GameFormat>(1),
                row.get_unwrap::<usize, NaiveTime>(2),
                row.get_unwrap::<usize, u8>(3),
                row.get_unwrap::<usize, NaiveTime>(4),
                row.get_unwrap::<usize, u8>(5),
            ))
        ).expect("Failed to get polls information from database");

        let day_to_str = |day: u8| {
            match day {
                1 => "Пн",
                2 => "Вт",
                3 => "Ср",
                4 => "Чт",
                5 => "Пт",
                6 => "Сб",
                7 => "Вс",
                _ => "Err"
            }
        };

        for result in results {
            let tmp = result.unwrap();
            res.push((tmp.0, format!("{} {} ({}) -> {} ({})",
                match tmp.1 {
                    types::GameFormat::Commander => "EDH",
                    types::GameFormat::Standard => "Standard",
                    types::GameFormat::CommanderDraft => "EDH Draft",
                    types::GameFormat::StandardDraft => "Standard Draft",
                },
                day_to_str(tmp.3),
                tmp.2.format("%H:%M"),
                day_to_str(tmp.5),
                tmp.4.format("%H:%M"),
            )))
        }

        res
    }

    pub fn get_poll(&self, poll_id: i64) -> types::Poll {
        self.db.query_row(
            "SELECT * FROM polls WHERE id=?1",
            (poll_id,),
            |row: &Row| Ok(types::Poll{
                id: row.get_unwrap::<usize, i64>(0),
                creator_id: row.get_unwrap::<usize, i64>(1),
                question: row.get_unwrap::<usize, String>(2),
                description: row.get_unwrap::<usize, String>(3),
                place: row.get_unwrap::<usize, String>(4),
                game_format: row.get_unwrap::<usize, GameFormat>(5),
                game_time: row.get_unwrap::<usize, NaiveTime>(6),
                start_time: row.get_unwrap::<usize, NaiveTime>(7),
                start_day: row.get_unwrap::<usize, u8>(8),
                end_time: row.get_unwrap::<usize, NaiveTime>(9),
                end_day: row.get_unwrap::<usize, u8>(10),
            })
        ).expect("Failed extracting poll info")
    }

    pub fn get_poll_question(&self, poll_id: i64) -> String {
        self.db.query_row(
            "SELECT question FROM polls WHERE id=?1",
            (poll_id,),
            |row: &Row| Ok(row.get::<usize, String>(0).unwrap())
        ).expect("Failed querying polls database!")
    }

    pub fn edit_poll_question(&mut self, poll_id: i64, question: String) {
        self.db.execute(
            "UPDATE polls SET question=?2 WHERE id=?1",
            (poll_id, question)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_description(&mut self, poll_id: i64, description: String) {
        self.db.execute(
            "UPDATE polls SET description=?2 WHERE id=?1",
            (poll_id, description)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_place(&mut self, poll_id: i64, place: String) {
        self.db.execute(
            "UPDATE polls SET place=?2 WHERE id=?1",
            (poll_id, place)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_game_time(&mut self, poll_id: i64, game_time: NaiveTime) {
        self.db.execute(
            "UPDATE polls SET game_time=?2 WHERE id=?1",
            (poll_id, game_time)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_start_time(&mut self, poll_id: i64, start_time: NaiveTime) {
        self.db.execute(
            "UPDATE polls SET start_time=?2 WHERE id=?1",
            (poll_id, start_time)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_start_day(&mut self, poll_id: i64, start_day: u8) {
        self.db.execute(
            "UPDATE polls SET start_day=?2 WHERE id=?1",
            (poll_id, start_day)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_end_time(&mut self, poll_id: i64, end_time: NaiveTime) {
        self.db.execute(
            "UPDATE polls SET end_time=?2 WHERE id=?1",
            (poll_id, end_time)
        ).expect("Failed updating poll information");
    }

    pub fn edit_poll_end_day(&mut self, poll_id: i64, end_day: u8) {
        self.db.execute(
            "UPDATE polls SET end_day=?2 WHERE id=?1",
            (poll_id, end_day)
        ).expect("Failed updating poll information");
    }

    pub fn get_poll_options(&mut self, poll_id: i64) -> Vec<String> {
        let mut res: Vec<String> = vec![];

        let today = Utc::now().with_timezone(&self.get_offset());
        let week_day_num = today.weekday().num_days_from_monday();
        let next_monday = today.checked_add_days(Days::new(7 - week_day_num as u64)).unwrap().date_naive();

        res.push(format!("Понедельник ({})", next_monday.format("%d.%m")));
        res.push(format!("Вторник ({})", next_monday.checked_add_days(Days::new(1)).unwrap().format("%d.%m")));
        res.push(format!("Среда ({})", next_monday.checked_add_days(Days::new(2)).unwrap().format("%d.%m")));
        res.push(format!("Четверг ({})", next_monday.checked_add_days(Days::new(3)).unwrap().format("%d.%m")));
        res.push(format!("Пятница ({})", next_monday.checked_add_days(Days::new(4)).unwrap().format("%d.%m")));
        res.push("Проверяю обстановку 👀".to_string());
        res.push("Не приду 🙅".to_string());

        self.db.execute(
            "UPDATE polls SET next_monday=?2 WHERE id=?1",
            (poll_id, next_monday)
        ).expect("Failed recording next monday!");

        res
    }

    pub fn attach_poll_and_msg(&self, poll_id: i64, message_id: MessageId, tg_poll_id: &String) {
        self.db.execute(
            "UPDATE polls SET message_id=?2, poll_id=?3 WHERE id=?1",
            (poll_id, message_id.0, tg_poll_id)
        ).expect("Failed attaching poll and message!");
    }

    pub fn add_poll_result(&self, poll_answer: PollAnswer) {
        self.db.execute(
            "DELETE FROM poll_results WHERE telegram_id=?1",
            (poll_answer.user.id.0,)
        ).expect("Failed deleting old poll results");

        let poll_id = self.db.query_row(
            "SELECT id FROM polls WHERE poll_id=?1",
            (poll_answer.poll_id,),
            |row: &Row| Ok(row.get::<usize, i64>(0).unwrap())
        ).expect("Could not find requested poll!");

        let mut vote = String::new();
        for option_id in poll_answer.option_ids {
            vote += format!("{},", option_id).as_str();
        }
        if vote.ends_with(',') {
            vote.remove(vote.len() - 1);
        }

        self.db.execute(
            "INSERT INTO poll_results (poll_id, telegram_id, vote) VALUES (?1, ?2, ?3)",
            (poll_id, poll_answer.user.id.0, vote)
        ).expect("Failed adding new vote");
    }

    pub fn get_polls_to_process(&mut self) -> Vec<i64> {
        let mut res: Vec<i64> = vec![];

        let today = Utc::now().with_timezone(&self.get_offset());
        let week_day_num = today.weekday().num_days_from_monday() + 1;

        // 0.125 day = 3 hours
        let mut statement = self.db.prepare(
            "SELECT id FROM polls WHERE (message_id!=0 AND poll_id!='') AND ((end_day=?1 AND julianday(?2) - julianday(end_time) >= 0 AND julianday(?2) - julianday(end_time) <= 0.125) OR (end_day=(?1 + 5) % 7 + 1 AND julianday(?2, '+001 days') - julianday(end_time) <= 0.125))"
        ).expect("Failed to prepare statement!");

        let results = statement.query_map(
            (week_day_num, today.time()),
            |row: &Row| Ok(row.get::<usize, i64>(0).unwrap())
        ).expect("Failed to get polls to process from database!");

        for result in results {
            res.push(result.unwrap());
        }

        res
    }

    pub fn get_poll_message_id(&self, poll_id: i64) -> MessageId {
        self.db.query_row(
            "SELECT message_id FROM polls WHERE id=?1",
            (poll_id,),
            |row: &Row| Ok(MessageId(row.get::<usize, i32>(0).unwrap()))
        ).expect("Failed querying polls database!")
    }

    pub fn get_poll_id(&mut self, message_id: MessageId) -> Option<i64> {
        self.db.query_row(
            "SELECT id FROM polls WHERE message_id=?1",
            (message_id.0,),
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).optional().expect("Failed querying polls database!")
    }

    pub fn process_poll_results(&mut self, poll_id: i64, dry_run: bool) -> (Vec<(types::Game, Vec<UserId>)>, String, Vec<UserId>, Vec<UserId>) {
        let mut res: Vec<(types::Game, Vec<UserId>)> = vec![];

        log::debug!("Start processing poll results");

        let mut statement = self.db.prepare(
            "SELECT telegram_id, vote FROM poll_results WHERE poll_id=?1"
        ).expect("Failed preparing poll results database query!");

        let query_map = statement.query_map(
            (poll_id,),
            |row: &Row| Ok((UserId(row.get::<usize, u64>(0).unwrap()), row.get::<usize, String>(1).unwrap()))
        ).expect("Failed querying poll results database!");

        let mut max_games_statement = self.db.prepare(
            "SELECT max_games_in_week FROM players WHERE telegram_id=?1"
        ).expect("Failed preparing players database statement");

        //                                       Monday       Tuesday      Wednesday    Thursday     Friday
        let mut votes: [(u8, Vec<UserId>); 5] = [(0, vec![]), (1, vec![]), (2, vec![]), (3, vec![]), (4, vec![])];
        let mut checkers : Vec<UserId> = vec![];

        let mut player_availability = HashMap::new();
        for answer_res in query_map {
            let answer = answer_res.unwrap();
            log::debug!("Handling player {} vote...", answer.0.0);
            let max_games = max_games_statement.query_row(
                (answer.0.0,),
                |row: &Row| Ok(row.get_unwrap::<usize, u8>(0))
            ).optional().expect("Failed querying players database").unwrap_or(1);
            log::debug!("- Player is available for {} games", max_games);
            player_availability.insert(
                answer.0,
                max_games,
            );
            if !answer.1.is_empty() {
                for vote in answer.1.split(",").map(|block| i32::from_str(block).unwrap()).collect::<Vec<i32>>() {
                    match vote {
                        0..=4 => {
                            log::debug!("- Adding vote for day {}", vote);
                            votes[vote as usize].1.push(answer.0);
                        },
                        5 => {
                            log::debug!("- Adding player to checkers");
                            checkers.push(answer.0);
                        },
                        6 => {
                            log::debug!("- Player decided not to come");
                        },
                        _ => {
                            log::error!("Unexpected vote option for user {}, poll id {}", answer.0, poll_id);
                        }
                    }
                }
            } else {
                log::debug!("- Player changed their mind about voting");
            }
        }
        drop(statement);
        drop(max_games_statement);

        let max_votes = max(max(max(max(votes[0].1.len(), votes[1].1.len()), votes[2].1.len()), votes[3].1.len()), votes[4].1.len());
        log::debug!("Maximum votes in a day: {}", max_votes);
        if max_votes < 3 {
            if !dry_run {
                self.db.execute(
                    "UPDATE polls SET message_id=0, poll_id='', next_monday=null WHERE id=?1",
                    (poll_id,)
                ).expect("Failed clearing poll id and message!");
                self.db.execute(
                    "DELETE FROM poll_results WHERE poll_id=?1",
                    (poll_id,)
                ).expect("Failed clearing poll results");
            }
            return (res, "".to_string(), vec![], vec![]);
        }

        let mut wanting_players: Vec<UserId> = vec![];
        let offset = self.get_offset();
        let (creator_id, description, place, game_format, game_time, next_monday) = self.db.query_row(
            "SELECT creator_id, description, place, game_format, game_time, next_monday FROM polls WHERE id=?1",
            (poll_id,),
            |row: &Row| {
                Ok((
                    row.get_unwrap::<usize, i64>(0),
                    row.get_unwrap::<usize, String>(1),
                    row.get_unwrap::<usize, String>(2),
                    row.get_unwrap::<usize, types::GameFormat>(3),
                    row.get_unwrap::<usize, NaiveTime>(4),
                    row.get_unwrap::<usize, NaiveDate>(5),
                ))
            }
        ).expect("Failed querying polls database!");

        if !dry_run {
            self.db.execute(
                "UPDATE polls SET message_id=0, poll_id='', next_monday=null WHERE id=?1",
                (poll_id,)
            ).expect("Failed clearing poll id and message!");
            self.db.execute(
                "DELETE FROM poll_results WHERE poll_id=?1",
                (poll_id,)
            ).expect("Failed clearing poll results");
        }

        let mut winning_days = String::new();
        votes.sort_by(|day1, day2| day2.1.len().cmp(&day1.1.len()));
        for day in &mut votes {
            log::debug!("Processing day {} with {} votes", day.0, day.1.len());
            if day.1.len() >= 3 {
                log::debug!("- More than 3 players voted. Considering creating a game...");
                let mut pending_players : Vec<UserId> = vec![];
                for player in &day.1 {
                    let games_left = player_availability[&player];
                    if games_left >= 1 {
                        player_availability.insert(*player, games_left - 1);
                        pending_players.push(*player);
                    }
                }
                if pending_players.len() < 3 {
                    log::debug!("- Found only {} players available, resetting their availiable days", pending_players.len());
                    for player in &pending_players {
                        let games_left = player_availability[&player];
                        player_availability.insert(*player, games_left + 1);
                    }
                    wanting_players.append(&mut pending_players);
                } else {
                    log::debug!("- Found {} players available, preparing game info", pending_players.len());
                    let date_time : DateTime<FixedOffset> = DateTime::from_naive_utc_and_offset(
                        NaiveDateTime::new(
                            next_monday,
                            game_time
                        ).checked_sub_offset(offset).unwrap(),
                        offset
                    ).checked_add_days(Days::new(day.0 as u64)).unwrap();

                    winning_days += match day.0 {
                        0 => date_time.format("Пн (%d.%m) "),
                        1 => date_time.format("Вт (%d.%m) "),
                        2 => date_time.format("Ср (%d.%m) "),
                        3 => date_time.format("Чт (%d.%m) "),
                        4 => date_time.format("Пт (%d.%m) "),
                        5 => date_time.format("Сб (%d.%m) "),
                        6 => date_time.format("Вс (%d.%m) "),
                        _ => date_time.format("(%d.%m) "),
                    }.to_string().as_str();

                    res.push((types::Game{
                        id: 0,
                        message_id: MessageId(0),
                        creator_id,
                        place: place.clone(),
                        description: description.clone(),
                        date_time,
                        required_player_count: 0,
                        format: game_format,
                        participation_fee: 0,
                        mandatory_fee: false,
                        registrations: vec![],
                    }, pending_players));
                }
            } else {
                wanting_players.append(&mut day.1);
            }
        }

        log::debug!("Notifying players that are only checking");
        // Cleanup players that got to play
        wanting_players.sort(); // Need to sort before de-duping
        wanting_players.dedup();
        for (_, playing_players) in &res {
            for player in playing_players {
                let res = wanting_players.iter().position(|val| val.0 == player.0);
                match res {
                    Some(index) => { wanting_players.remove(index); },
                    None => { /*Nothing to do*/ },
                }
            }
        }

        log::debug!("Poll results succesfully processed!");

        (res, winning_days, wanting_players, checkers)
    }

    pub fn does_achievement_claim_exist(&self, user_id: i64, achievement_id: i64) -> bool {
        self.db.query_row(
            "SELECT COUNT(1) FROM achievement_claims WHERE player_id=?1 AND achievement_id=?2",
            [user_id, achievement_id],
            |row: &Row| Ok(row.get_unwrap::<usize, i64>(0))
        ).expect("Failed checking achievement claim!") != 0
    }

    pub fn add_achievement_claim(&self, user_id: i64, achievement_id: i64, message_id: MessageId) -> i64 {
        self.db.execute(
            "INSERT INTO achievement_claims (player_id, achievement_id, message_id) VALUES (?1, ?2, ?3)",
            (user_id, achievement_id, message_id.0)
        ).expect("Failed adding achievement claim!");
        self.db.last_insert_rowid()
    }

    pub fn grant_player_achievement(&self, user_id: i64, achievement_id: i64) {
        self.db.execute(
            "INSERT INTO player_achievements (player_id, achievement_id) VALUES (?1, ?2)",
            (user_id, achievement_id)
        ).expect("Failed granting player achievement!");
    }

    pub fn add_achievement_claim_vote(&self, telegram_id: UserId, claim_id: i64, vote: u8) -> Option<(bool, MessageId, i64)> {
        let has_voted = self.db.query_row(
            "SELECT id FROM achievement_claim_votes WHERE claim_id=?1 AND telegram_id=?2",
            (claim_id, telegram_id.0),
            |_row: &Row| Ok(())
        ).optional().expect("Failed querying achievement claim votes database!").is_some();

        if has_voted {
            self.db.execute(
                "UPDATE achievement_claim_votes SET vote=?3 WHERE claim_id=?1 AND telegram_id=?2",
                (claim_id, telegram_id.0, vote)
            ).expect("Failed to add vote");
        } else {
            self.db.execute(
                "INSERT INTO achievement_claim_votes (claim_id, telegram_id, vote) VALUES (?1, ?2, ?3)",
                (claim_id, telegram_id.0, vote)
            ).expect("Failed to add vote");
        }

        let mut claim_statement = self.db.prepare(
            "SELECT vote FROM achievement_claim_votes WHERE claim_id=?1",
        ).expect("Failed preparing achievement claim votes database statement!");

        let claim_res = claim_statement.query_map(
            (claim_id,),
            |row: &Row| Ok(row.get_unwrap::<usize, u8>(0) == 1)
        ).expect("Failed querying achievement claim votes database!");

        let mut votes_for = 0;
        let mut votes_against = 0;
        for res in claim_res {
            let current_vote = res.unwrap();
            if current_vote {
                votes_for += 1;
            } else {
                votes_against += 1;
            }
        }

        if votes_for >= 3 || votes_against >=3 {
            self.db.execute(
                "DELETE FROM achievement_claim_votes WHERE claim_id=?1",
                (claim_id,)
            ).expect("Failed clearing achievement claim votes!");
            let (message_id, achievement_id) = self.db.query_row(
                "SELECT message_id, achievement_id FROM achievement_claims WHERE id=?1",
                (claim_id,),
                |row: &Row| Ok((MessageId(row.get_unwrap::<usize, i32>(0)), row.get_unwrap::<usize, i64>(1)))
            ).expect("Failed getting achievement claims message id!");
            self.db.execute(
                "DELETE FROM achievement_claims WHERE id=?1",
                (claim_id,)
            ).expect("Failed clearing achievement claim!");
            if votes_for >= 3 {
                Some((true, message_id, achievement_id))
            } else {
                Some((false, message_id, achievement_id))
            }
        } else {
            None
        }
    }

    pub fn get_pending_notifications(&mut self) -> Vec<types::Notification> {
        let mut statement = self.db.prepare(
            "DELETE FROM pending_notifications RETURNING telegram_id, text"
        ).expect("Failed preparing pending notifications statement!");
        let query_res_opt = statement.query_map((), |row: &Row| Ok(types::Notification {
            telegram_id: UserId(row.get_unwrap::<usize, i64>(0) as u64),
            text: row.get_unwrap(1),
        })).optional().expect("Failed querying pending notifications from database!");

        let mut res: Vec<types::Notification> = vec![];
        match query_res_opt {
            Some(query_res) => {
                for event_res in query_res {
                    res.push(event_res.unwrap());
                }
            },
            None => {
                // Nothing to do
            }
        }
        return res;
    }

    pub fn defer_event_notification(&self, event: types::Event) {
        let mut s_statement = self.db.prepare(
            "SELECT telegram_id FROM players INNER JOIN subscriptions ON players.id == subscriptions.player_id WHERE subscriptions.event_mask & ?1 != 0"
        ).expect("Failed preparing subscribers statement!");
        let query_res_opt = s_statement.query_map(
            (event.event_type,),
            |row: &Row| Ok(UserId(row.get_unwrap(0)))
        ).optional().expect("Failed querying subscribers from database!");
        let mut subscribers: Vec<UserId> = vec![];
        match query_res_opt {
            Some(query_res) => {
                for user_res in query_res {
                    subscribers.push(user_res.unwrap());
                }
            }
            None => {
                // Nothing to do
            }
        }

        if !subscribers.is_empty() {
            let mut pn_statement = self.db.prepare(
                "INSERT INTO pending_notifications (telegram_id, text) VALUES (?1, ?2)"
            ).expect("Failed preparing pending notifications statement");

            for subscriber in subscribers {
                pn_statement.execute((subscriber.0, event.desc.clone())).expect("Failed posting notification for player");
            }
        }

    }

    pub fn update_user_subscriptions(&mut self, user_id: i64, subscription: &types::Subscription) {
        self.db.execute(
            "INSERT INTO subscriptions (player_id, event_mask) VALUES (?1, ?2) ON CONFLICT(player_id) DO UPDATE SET event_mask=?2 WHERE player_id=?1",
            (user_id, subscription.to_mask())
        ).expect("Failed updating user preferences in database!");
    }
}

#[cfg(test)]
mod test {
    use crate::mtg::get_group_table_counts;

    #[test]
    fn group_tables_count_test() {
        let test_cases = vec![
            (03, (1, 0, 0)), // 3 players, only 1 table of 3
            (04, (0, 1, 0)), // 4 players, only 1 table of 4
            (05, (0, 0, 1)), // 5 players, only 1 table of 5

            (06, (2, 0, 0)), //  6 players, 2 tables of 3
            (07, (1, 1, 0)), //  7 players, 1 table of 3 and 1 table of 4
            (08, (0, 2, 0)), //  8 players, 2 tables of 4
            (09, (3, 0, 0)), //  9 players, 3 tables of 3
            (10, (2, 1, 0)), // 10 players, 2 tables of 3 and 1 table of 4
            (11, (1, 2, 0)), // 11 players, 1 table of 3 and 2 tables of 4

            // Edge case, both 3 and 4 are good. We prioritize 4s
            (12, (0, 3, 0)), // 12 players, 3 tables of 4

            (13, (3, 1, 0)), // 13 players, 3 tables of 3 and 1 table of 4

            // Bad number. We prioritize getting equal size tables
            (14, (0, 1, 2)), // 14 players, 1 table of 4 and 2 tables of 5

            (15, (5, 0, 0)), // 15 players, 5 tables of 3

            // We rarely get 4 players, so... Testing further is kinda pointless
        ];

        for (player_count, expected_layout) in test_cases {
            assert_eq!(expected_layout, get_group_table_counts(player_count));
        }
    }
}
