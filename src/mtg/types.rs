use std::fmt::{Display, Formatter};
use chrono::{DateTime, FixedOffset, NaiveTime};
use rusqlite::types::{ToSql, FromSql, FromSqlError, FromSqlResult, ToSqlOutput, ValueRef};
use teloxide::{prelude::UserId, types::MessageId};

pub struct Subscription {
    pub new_std: bool,
    pub new_edh: bool,
    pub new_std_draft: bool,
    pub new_edh_draft: bool,
}

pub enum EventType {
    NewStd,
    NewEdh,
    NewStdDraft,
    NewEdhDraft,
}

pub struct Event {
    pub event_type: EventType,
    pub desc: String,
}

pub struct Notification {
    pub telegram_id: UserId,
    pub text: String,
}

pub struct Achievement {
    pub id: i64,
    pub badge: String,
    pub name: String,
    pub description: String,
    pub achievable: bool,
}

pub struct Preferences {
    pub max_games_in_week: u8,
    pub used_badge: String
}

pub struct Player {
    pub id: i64,
    pub display_name: String,
    pub telegram_id: UserId,
    pub preferences: Preferences,
    pub achievements: Vec<i64>,
    pub subscription: Subscription,
    pub is_admin: bool,
}

pub enum RegistrationType {
    Normal,     // 0
    TelegramId, // 1
    NameOnly,   // 2
}

pub struct GameRegistration {
    pub reg_id: i64,
    pub reg_type: RegistrationType,
    pub display_name: Option<String>,
    pub telegram_id: Option<UserId>,
    pub telegram_tag: Option<String>,
    pub is_buying: bool,
}

#[derive(Copy, Clone)]
pub enum GameFormat {
    Commander,
    Standard,
    CommanderDraft,
    StandardDraft,
}

pub struct Game {
    pub id: i64,
    pub message_id: MessageId,
    pub creator_id: i64,
    pub place: String,
    pub description: String,
    pub date_time: DateTime<FixedOffset>,
    pub required_player_count: u32,
    pub format: GameFormat,
    pub participation_fee: u32,
    pub mandatory_fee: bool,
    pub registrations: Vec<GameRegistration>,
}

pub struct Poll {
    pub id: i64,
    pub creator_id: i64,
    pub question: String,
    pub description: String,
    pub place: String,
    pub game_format: GameFormat,
    pub game_time: NaiveTime,
    pub start_time: NaiveTime,
    pub start_day: u8,
    pub end_time: NaiveTime,
    pub end_day: u8,
}

#[derive(Debug)]
pub enum ErrorCode {
    InternalError,
    AlreadyExists,
    NotFound,
    Uninitialized,
}

impl Display for ErrorCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let str_repr = match self {
            ErrorCode::InternalError => "Internal error",
            ErrorCode::AlreadyExists => "Already exists",
            ErrorCode::NotFound => "Not found",
            ErrorCode::Uninitialized => "Uninitialized",
        };
        write!(f, "{}", str_repr)
    }
}

impl std::error::Error for ErrorCode {}

impl ToSql for GameFormat {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        return match self {
            GameFormat::Commander => Ok(ToSqlOutput::from(0)),
            GameFormat::Standard => Ok(ToSqlOutput::from(1)),
            GameFormat::CommanderDraft => Ok(ToSqlOutput::from(2)),
            GameFormat::StandardDraft => Ok(ToSqlOutput::from(3)),
        }
    }
}

impl FromSql for GameFormat {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let val = value.as_i64().unwrap_or(-1);
        return match val {
            0 => Ok(GameFormat::Commander),
            1 => Ok(GameFormat::Standard),
            2 => Ok(GameFormat::CommanderDraft),
            3 => Ok(GameFormat::StandardDraft),
            _ => Err(FromSqlError::OutOfRange(val)),
        }
    }
}

impl ToSql for EventType {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        return match self {
            EventType::NewStd => Ok(ToSqlOutput::from(1)),
            EventType::NewEdh => Ok(ToSqlOutput::from(2)),
            EventType::NewStdDraft => Ok(ToSqlOutput::from(4)),
            EventType::NewEdhDraft => Ok(ToSqlOutput::from(8)),
        }
    }
}

impl FromSql for EventType {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let val = value.as_i64().unwrap_or(-1);
        return match val {
            1 => Ok(EventType::NewStd),
            2 => Ok(EventType::NewEdh),
            4 => Ok(EventType::NewStdDraft),
            8 => Ok(EventType::NewEdhDraft),
            _ => Err(FromSqlError::OutOfRange(val)),
        }
    }
}

impl GameRegistration {
    pub fn simple_form(reg_id: i64, reg_type: RegistrationType) -> GameRegistration {
        GameRegistration {
            reg_id,
            reg_type,
            display_name: None,
            telegram_id: None,
            telegram_tag: None,
            is_buying: false,
        }
    }
}

impl Subscription {
    pub fn to_mask(&self) -> u64 {
        return (if self.new_std { 1_u64 } else { 0_u64 }) |
               (if self.new_edh { 1_u64 << 1 } else { 0_u64 }) |
               (if self.new_std_draft { 1_u64 << 2 } else { 0_u64 }) |
               (if self.new_edh_draft { 1_u64 << 3 } else { 0_u64 });
    }

    pub fn from_mask(mask: u64) -> Subscription {
        Subscription {
            new_std: (mask & 1_u64 << 0) != 0,
            new_edh: (mask & 1_u64 << 1) != 0,
            new_std_draft: (mask & 1_u64 << 2) != 0,
            new_edh_draft: (mask & 1_u64 << 3) != 0,
        }
    }
}
